$(function(){
//    修改头像
    
    $("#photo").click(function(){  
        layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'],
            //宽高
            title: false,
            //不显示标题
            content: '<div class="layer_box">' + '<div class="login_bg login_top">修改头像</div>'          
            + '<div class="layer_li" id="picbox">'
            +'<div class="pbox"><div class="boximg"><div class="imgpc"><img src="../resources/images/shang.png"></div><p>请选择一张图片</p></div>'
            + '<div class="posi" style=""><input type="file" id="img" name="img"  style="width: 100%;height:100%;"> </div>' 
            +'</div>'        
            +'<div class="clear"></div>'
            + '</div>' + 
            '<div id="qr_php" class="qr_btn">确认修改</div>' + '<div class="login_bg login_bto" style="height:20px"></div>' + '</div>',
          
            success: function() {
            	
                $('#qr_php').click(function() {                	
                	if($('input[name="img"]').val()==''){
    	    	    	layer.msg('请选择图片');
    	    	        return false;
    	    	    }else if($('input[name="img"]').val()!=''){
    	    	    	if(!(/^[a-zA-Z]:(\\.+)(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$/.test($('input[name=img]').val()))){		
    	    				layer.msg("请选择正确的图片格式,支持以下.JPEG .jpeg .JPG .jpg .GIF .gif .BMP .bmp .PNG .png");
    	    				return false;
    	    			} 
    	    	    } 
//                	上传图片
                	$.ajaxFileUpload({  
        	    	      url : basePath+"/update/img.htm",  
        	    	      secureuri : false,  
        	    	      fileElementId :'img',  
        	    	      dataType : 'json',
        	    	      data: {
                            username: $('#layer_nike').val()                           
                          },
                	      success:function(data){
                	    	 console.log(data);
                	    	  if(data.flag == 10000){
                	    		  layer.msg('上传成功');	
                                  setTimeout(function() {
                                	  $.ajax({
         				                 url: basePath + 'loginout.htm',
         				                 type: 'GET',
         				                 cache: false,
         				                 success: function(res) {
         				                    if (res == '10000') {
         				                        location.reload();
         				                    }
         				                }
         				            })
                                  }, 3000)
                              
                	    	  }else{
                	    		  layer.msg('上传失败');	
                	    	  }
                	    
                	      }
        	    	  }); 
                  	
                	
                })
            }
        })

    	
    })
})