//getCommonApi(code, startH, startM, endH, endM, endS, inter, endDateIsToday)
			/*bjpk10*/
			getCommonApi('bjpk10', 9, 02, 23, 59, 59, 5, true);

			/*cqssc*/
			var cqsscSecondTime = {
				startH: 22,
				startM: 0,
				endH: 2,
				endM: 0,
				endS: 0,
				inter: 5,
				endDateIsToday: false
			}
			getCommonApi('cqssc', 9, 50, 22, 0, 0, 10, true, cqsscSecondTime);

			/*jsk3*/
			getCommonApi('jsk3', 8, 30, 22, 10, 0, 10, true);

			/*gdklsf*/
			getCommonApi('gdklsf', 9, 0, 23, 0, 0, 10, true);

			/*cqklsf*/
			getCommonApi('cqklsf', 9, 53, 2, 0, 0, 10, false);

			/*xjssc*/
			getCommonApi('xjssc', 10, 0, 2, 0, 0, 10, false);

			/*bjkl8*/
			getCommonApi('bjkl8', 9, 00, 23, 59, 59, 5, true);

			/*hk6*/
			$.ajax({
				type: 'get',
				dataType:'json',
				url: 'api.php?code=hk6',
				success:function(data) {
					var item = data.data[0];
					$('#hk6-expect').text(item.expect);
					var arrImg = formatNumber(item.opencode, 'hk6');
					$('#expecthk6').html(arrImg);
					$('.hk6-lottery-time').text('晚上9:34:40');

					var time = getBJTime(item.opentime),
						nowTime = getTime(getBJTime(), 'all'),
						fixTimeStart = '21:33:00',
						fixTimeEnd = '21:35:00';
					if (nowTime > fixTimeStart && nowTime < fixTimeEnd) {
						var t = setInterval(function() {
							nowTime = getTime(getBJTime(), 'all')
							if (nowTime > fixTimeEnd) {
								clearInterval(t);
								return;
							}
							getApi('hk6');

						}, 3000);
					}
				}
			});

			/*fc3d*/
			$.ajax({
				type: 'get',
				dataType:'json',
				url: 'api.php?code=fc3d',
				success:function(data) {
					var item = data.data[0];
					$('#fc3d-expect').text(item.expect);
					var arrImg = formatNumber(item.opencode, 'fc3d');
					$('#expectfc3d').html(arrImg);
					$('.fc3d-lottery-time').text('晚上9:15:00');

					var time = getBJTime(item.opentime),
						nowTime = getTime(getBJTime(), 'all'),
						fixTimeStart = '09:13:00',
						fixTimeEnd = '09:16:00';
					if (nowTime > fixTimeStart && nowTime < fixTimeEnd) {
						var t = setInterval(function() {
							nowTime = getTime(getBJTime(), 'all')
							if (nowTime > fixTimeEnd) {
								clearInterval(t);
								return;
							}
							getApi('fc3d');

						}, 3000);
					}
				}
			});

			/*pl3*/
			$.ajax({
				type: 'get',
				dataType:'json',
				url: 'api.php?code=pl3',
				success:function(data) {
					var item = data.data[0];
					$('#pl3-expect').text(item.expect);
					var arrImg = formatNumber(item.opencode, 'pl3');
					$('#expectpl3').html(arrImg);
					$('.pl3-lottery-time').text('08:30:00');

					var time = getBJTime(item.opentime),
						nowTime = getTime(getBJTime(), 'all'),
						fixTimeStart = '08:29:00',
						fixTimeEnd = '08:31:00';
					if (nowTime > fixTimeStart && nowTime < fixTimeEnd) {
						var t = setInterval(function() {
							nowTime = getTime(getBJTime(), 'all')
							if (nowTime > fixTimeEnd) {
								clearInterval(t);
								return;
							}
							getApi('pl3');

						}, 3000);
					}
				}
			});

			/*common method*/
			function formatNumber(strs, type){
				strimg='';
				strs=strs.split(',');
				for(i=0;i<strs.length;i++) 
				{
					if(strs[i].indexOf("+")>0){
						strjs=strs[i].split('+');
						strimg=strimg + '<span class="' + type + '_no' + parseInt(strjs[0]) + '">'+ parseInt(strjs[0]) +'</span>';
						strimg=strimg + '<span class="' + type + '_no+">+</span>';
						strimg=strimg + '<span class="' + type + '_no' + parseInt(strs[i]) + '">'+ parseInt(strjs[1]) +'</span>';
					}
					else{
						strimg=strimg + '<span class="' + type + '_no' + parseInt(strs[i]) + '">'+ parseInt(strs[i]) +'</span>';
					}
				}
				return strimg;
			}

			function getApi(code) {
				$.ajax({
					type: 'get',
					dataType:'json',
					url: 'api.php?code=' + code,
					success:function(data) {
						var item = data.data[0];
						$('#' + code + '-expect').text(item.expect);
						var arrImg = formatNumber(item.opencode, 'code');
						$('#expect' + code).html(arrImg);
						$('.' + code + '-lottery-time').text(item.opentime);
					}
				});
			}

			function getTime(time, type) {
				var h = time.getHours() < 10 ? '0' + time.getHours() : time.getHours(),
					m = time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes(),
					s = time.getSeconds() < 10 ? '0' + time.getSeconds() : time.getSeconds();
				if (type == h) {
					return h;
				} else if (type == m) {
					return m;
				} else if (type == s) {
					return s;
				} else {
					return h + ':' + m + ':' + s;
				}
			}

            function calTimeDiff(start, end, type) {
                var date1=getBJTime(start);  //开始时间
                var date2=getBJTime(end);    //结束时间
                var date3=date2.getTime()-date1.getTime();  //时间差的毫秒数
                 
                //计算出相差天数
                var days=Math.floor(date3/(24*3600*1000));
                 
                //计算出小时数
                var leave1=date3%(24*3600*1000);    //计算天数后剩余的毫秒数
                var hours=Math.floor(leave1/(3600*1000));

                //计算相差分钟数
                var leave2=leave1%(3600*1000);        //计算小时数后剩余的毫秒数
                var minutes=Math.floor(leave2/(60*1000));
                 
                //计算相差秒数
                var leave3=leave2%(60*1000);      //计算分钟数后剩余的毫秒数
                var seconds=Math.round(leave3/1000);

                if (type == 'h') {
                	return hours;
                }  else if (type == 'm') {
                	return  minutes;
                } else if (type == 's') {
                	return seconds;
                } else if (type == 'allS') {
                	return leave1;
                }
            }
			
			function getLatelyPoint(startDate, endDate, interval) {
				var arrDate = [],
					startDate = getBJTime(startDate),
					endDate = getBJTime(endDate),
					latelyDate,
					start = startDate;

				while(start < endDate) {
					var date1 = start.setMinutes(start.getMinutes() + interval);
					var date2 = getBJTime(date1);
					var date3 = date2.setSeconds(date2.getSeconds() + 42);

					if (getBJTime(date3) < endDate) {
						arrDate.push(date3);
					}
				}

				$.each(arrDate, function(idx, item) {
					var resDate = getBJTime(item),
						nowDate = getBJTime();
					var diffs = calTimeDiff(nowDate, resDate, 'allS');
					if (diffs > 0 && diffs <= (interval * 60 * 1000 + 42)) {
						latelyDate = resDate;
					}
				});

				return latelyDate;
			}

			function currentDateSetHour(h, m, s, isToday) {
				var nowDate = getBJTime();

				if (!isToday) {
					nowDate = getBJTime(nowDate.setDate(nowDate.getDate() + 1));
				}

				nowDate.setHours(h, m, s);

				return getBJTime(nowDate);
			}
			function getCommonApi(code, startH, startM, endH, endM, endS, inter, endDateIsToday, secondTime) {
				$.ajax({
					type: 'get',
					dataType:'json',
					url: 'api.php?code=' + code,
					success:function(data) {
						var item = data.data[0];
						var lastExpect = $('#' + code + '-expect').text();
						if (lastExpect == item.expect) {
							$('.' + code + '-lottery-time').show();
							$('.' + code + '-countdown').hide();
							$('.' + code + '-lottery-time').text('开奖中...');
							var timeout = setTimeout(function() {
								getCommonApi(code, startH, startM, endH, endM, endS, inter, endDateIsToday);
								clearTimeout(timeout);
							}, 3000);
						} else {
							$('.' + code + '-lottery-time').hide();
							$('.' + code + '-countdown').show();

							$('#' + code + '-expect').text(item.expect);
							var arrData = formatNumber(item.opencode, code);
							$('#expect' + code).html(arrData);
							var fixTimeStart = currentDateSetHour(startH, startM, 0, true),
								fixTimeEnd = currentDateSetHour(endH, endM, endS, endDateIsToday),
								latelyDate = getLatelyPoint(fixTimeStart, fixTimeEnd, inter),
								nowDate = getBJTime();

							if (latelyDate && nowDate > fixTimeStart && nowDate < fixTimeEnd) {
								var m = calTimeDiff(nowDate, latelyDate, 'm'), s = calTimeDiff(nowDate, latelyDate, 's');
								if (s == 0 && m > 0) {
									m--;
									s=3;
								} else if (s == 0 && m == 0) {
									s = 1;
								}
								var i=0;
								var t1 = setInterval(function() {
									s--;
									if(s==0&&m==0) {
										playSound('baoma/ring.wav');
										getCommonApi(code, startH, startM, endH, endM, endS, inter, endDateIsToday);
										clearInterval(t1);
									}
									$('#kj' + code + '-next-m').text(m);
									$('#kj' + code + '-next-s').text(s);
									if(s == 0) {
										m--;
										s = 60;
									}
								}, 1000);
							} else {
								if (secondTime) {
									var SecondfixTimeStart = currentDateSetHour(secondTime.startH, secondTime.startM, 0, true);
									var SecondfixTimeEnd = currentDateSetHour(secondTime.endH, secondTime.endM, secondTime.endS, secondTime.endDateIsToday);
								}

								if (secondTime && nowDate > SecondfixTimeStart && nowDate < SecondfixTimeEnd) {
									fixTimeStart = SecondfixTimeStart;
									fixTimeEnd = SecondfixTimeEnd;
									latelyDate = getLatelyPoint(fixTimeStart, fixTimeEnd, secondTime.inter);
								}
								if (secondTime && (nowDate > fixTimeStart) && (nowDate < fixTimeEnd) && latelyDate) {
									var m = calTimeDiff(nowDate, latelyDate, 'm'), s = calTimeDiff(nowDate, latelyDate, 's');
									if (s == 0 && m > 0) {
										m--;
										s=60;
									} else if (s == 0 && m == 0) {
										s = 1;
									}
									var i=0;
									var t1 = setInterval(function() {
										s--;
										if(s == 0 && m == 0) {
											playSound('ring.wav');
											$('.' + code + '-lottery-time').show();
											$('.' + code + '-countdown').hide();
											$('.' + code + '-lottery-time').text('开奖中...');
											getCommonApi(code, secondTime.startH, secondTime.startM, secondTime.endH, secondTime.endM, secondTime.endS, secondTime.inter, secondTime.endDateIsToday);
											clearInterval(t1);
										}
										$('#kj' + code + '-next-m').text(m);
										$('#kj' + code + '-next-s').text(s);
										if(s == 0) {
											m--;
											s = 60;
										}
									}, 1000);
								} else {
									var startDate = currentDateSetHour(startH, startM, 0, true);
									var nowTime = getTime(nowDate, 'all');
									var startTime = getTime(fixTimeStart, 'all');
									var endTime = getTime(fixTimeEnd, 'all');

									if (nowTime > startTime ) {
										startDate = currentDateSetHour(startH, startM, 0, false);
									}
									var h = calTimeDiff(nowDate, startDate, 'h');
									var m = calTimeDiff(nowDate, startDate, 'm');
									var s = calTimeDiff(nowDate, startDate, 's');
									if (s == 0 && m > 0) {
										m--;
										if (m == 0 && h > 0) {
											h--;
											m = 59;
										}
										s = 60;
									} else if (s == 0 && m == 0 && h > 0) {
										h--;
										m = 59;
										s = 60;
									} else if (s == 0 && m == 0 && h == 0) {
										s = 1;
									}
									var i=0;
									var t2 = setInterval(function() {
										s--;
										if(s == 0 && m == 0 && h == 0) {
											playSound('ring.wav');
											clearInterval(t2);
										}
										if (h > 0) {
											$('#kj' + code + '-next-h').show();
											$('#kj' + code + '-next-h').text(h + '时');
										}
										
										$('#kj' + code + '-next-m').text(m);
										$('#kj' + code + '-next-s').text(s);
										if (s == 0 && m > 0) {
											m--;
											if (m == 0 && h > 0) {
												h--;
												m = 59;
											}
											s = 60;
										} else if (s == 0 && m == 0 && h > 0) {
											h--;
											m = 59;
											s = 60;
										}
									}, 1000);
								}
							}
						}
					}
				});	
			}

			function playSound(file){
			    document.getElementById('MsgSound').innerHTML='<audio controls="controls" autoplay="autoplay"><source src="'+file+'" type="audio/mpeg">你的浏览器还不支持哦</audio>';
	
			}

			function getBJTime (time) {
				var date, utc, newDate;
				if (time) {
					date = new Date(time);
				} else {
					date = new Date();
				}
				utc = date.getTime() + (date.getTimezoneOffset() * 60000);
				newDate = new Date(utc + (3600000 * +8));
				return newDate;
			}