/*
  初始化加载
*/
$(function(){
	//全屏设置
	$('#quanping').click(function(){
		quanping(this);
	})
	/*获取皮肤*/
	getSkinByCookie();
	/*获取菜单icon随机色*/
	getMathColor();
	/*菜单json*/
	var menuList=$.parseJSON($('#menu').attr('menu'));
	console.log(menuList.list);
	menuList=menuList.list;
	console.log(menuList);
	var menu=[];
	var tmp={};
	var tmpc={};
	var tmpcc={};
	for(var i=0;i<menuList.length;i++){
		tmp.id=menuList[i].id;
		tmp.name=menuList[i].name;
		tmp.parentId=menuList[i].parentId?menuList[i].parentId:'0';
		tmp.icon=menuList[i].icon?menuList[i].icon:'';
		tmp.order='1';
		tmp.isHeader='1';
		if((menuList[i].url?menuList[i].url:'').trim()==''){
			tmp.url='';
		}else{
			tmp.url=menuList[i].url.trim();
		}
		if(menuList[i].children.length!=0){
			tmp.childMenus=[];
			for(var l=0;l<menuList[i].children.length;l++){
				tmpc.id=menuList[i].children[l].id;
				tmpc.name=menuList[i].children[l].name;
				tmpc.parentId=menuList[i].id;
				tmpc.icon=menuList[i].children[l].icon?menuList[i].children[l].icon:'';
				tmpc.order='1';
				tmpc.isHeader='0';
				if((menuList[i].children[l].url?menuList[i].children[l].url:'').trim()==''){
					tmpc.url='';
				}else{
					tmpc.url=ctx+menuList[i].children[l].url.trim();
				}
				if(menuList[i].children[l].children){
					tmpc.childMenus=[];
					for(var s=0;s<res[i].children[l].children.length;s++){
						tmpcc.id=menuList[i].children[l].children[s].id;
						tmpcc.name=menuList[i].children[l].children[s].name;
						tmpcc.parentId=menuList[i].children[l].id;
						tmpcc.icon=menuList[i].children[l].children[s].icon?menuList[i].children[l].children[s].icon:'';
						tmpcc.order='1';
						tmpcc.isHeader='0';
						/*tmpcc.url=menuList[i].children[l].children[s].url.trim();*/
						if(menuList[i].children[l].children[s].children.length==0){
							tmpcc.childMenus='';
						}
						tmpc.childMenus.push(tmpcc);
						tmpcc={};
					 }
				}else{
					tmpc.childMenus='';
				}
				tmp.childMenus.push(tmpc);
				tmpc={};
			 }
		}else{
			tmp.childMenus='';
		}
		menu.push(tmp);
		tmp={};
	 }
	console.log(menu);
	 initMenu(menu,$(".side-menu"));
	$(".side-menu > li").addClass("menu-item");	
	//修改密码
	$('#updatePass').click(function(){
		var that=this;
		var v = localStorage.sccluiSkin;
		var bgcolor='#33AECC';
		if(v=='blue'){
			bgcolor='#1E9FFF';
		}else if(v=='molv'){
			bgcolor='#19a97b';
		}else if(v=='qinxin'){
			bgcolor='#33AECC';
		}
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '修改密码',
            content: '<div class="layer_box"><div class="layer_li"><span>原密码</span><input type="text" id="layer_oldpass" placeholder="请输入原密码" value=""></div><div class="layer_li"><span>新密码</span><input type="password" id="layer_newpass" value=""></div><div class="layer_li"><span>确认密码</span><input type="password" id="layer_newpass2" value=""></div><div id="qr_login" class="qr_btn" istrue="false">确认修改</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('#qr_login').css('background',bgcolor);
    			 $('#qr_login').attr('istrue','true');
 /*                $('#layer_oldpass').blur(function(){
                	 var oldpass=$('#layer_oldpass').val().trim();
                	 if(oldpass==''){
                		 layer.msg('请输入原密码');
                		 return;
                	 }
                	 if($('#qr_login').attr('istrue')=='true'){
                		 return;
                	 }
                	 $.ajax({
                    	 url:ctx+'/index/checkPassword',
                    	 type:'POST',
                    	 data:{salt:$(that).attr('salt'),oldpassword:$('#layer_oldpass').val().trim(),id:$('body').attr('thisid')},
                    	 success:function(res){
                    		 console.log(res);
                    		 if(res=='false'){
                    			 layer.msg('原密码错误');
                    		 }else{
                    			 $('#qr_login').css('background',bgcolor);
                    			 $('#qr_login').attr('istrue','true')
                    		 }
                    	 }
                     })
                 })*/
    			 var qrFlag=true;
                 $('#qr_login').click(function(){
                	 if(!qrFlag){
                		 return;
                	 }
                	 qrFlag=false;
                	 var oldpass=$('#layer_oldpass').val().trim();
                	 var newpass=$('#layer_newpass').val().trim();
                	 var newpass2=$('#layer_newpass2').val().trim();
                	 if(newpass==''){
                		 layer.msg('请输入新密码');
                		 return;
                	 }
                	 if(newpass.length<6){
                		 layer.msg('新密码长度不能小于7');
                		 return;
                	 }
                	 if(newpass==oldpass){
                		 layer.msg('原密码和新密码相同,不需修改');
                		 return;
                	 }
                	 if(newpass2==''){
                		 layer.msg('请确认新密码');
                		 return;
                	 }
                	 if(newpass!=newpass2){
                		 layer.msg('两次密码输入不一致');
                		 return;
                	 }
                	 $.ajax({
                    	 url:ctx+'/admin/update/pass.htm',
                    	 type:'POST',
                    	 data:{username:$('body').attr('thidname'),newPassword:newpass,passWord:oldpass},
                    	 success:function(res){
                    		 qrFlag=true;
                    		 var res=$.parseJSON(res);
                    		 console.log(res);
                    		 if(res.flag=='10000'){
                    			 layer.msg(res.msg);
                    			 setTimeout(function(){
                    				 layer.closeAll();
                    			 },500)
                    		 }else{
                    			 layer.msg(res.msg);
                    		 }
                    	 }
                     })
                     
                     
                     
                 })
                 
            }
		})
	})
})