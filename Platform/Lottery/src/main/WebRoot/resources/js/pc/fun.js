
// 左侧列表的显示隐藏
$(document).ready(function() {
    $('.js-main-meta-toggle').click(function() {
        var $this = $(this);
        var _mLeft = parseInt($("#UI_Left").css("margin-left"));
        if( _mLeft < 0) {
            $this.find("i").removeClass("fa-angle-double-right");
            $("#UI_Left").css("margin-left","2px");
            $('#UI_Central').width($('#UI_Central').width() - 212);
            $this.attr({"title": "点击隐藏左侧列表"});
        } else {
            $this.find("i").addClass("fa-angle-double-right");
            $("#UI_Left").css("margin-left","-212px");
            $('#UI_Central').width($('#UI_Central').width() + 212);
            $this.attr({"title": "点击显示左侧列表"});
        }
    });
})
function _toRight(){
    var _mLeft = parseInt($("#UI_Left").css("margin-left"));
    if( _mLeft < 0){
        $("#UI_Left").animate({"margin-left":"2px"});
        $("#UI_Central").animate({"margin-left":"198px"});
    }else{
        $("#UI_Left").animate({"margin-left":"-192px"});
        $("#UI_Central").animate({"margin-left":"3px"});
    }

}

// 根据可视窗口大小进行调整
function OnResize(){
    var cw=$(window).width();
    var _mLeft = parseInt($("#UI_Left").css("margin-left"));
    if( _mLeft < 0) {
        $('#UI_Central').width(cw - $('#UI_Right').width() - 15);
    } else {
        $('#UI_Central').width(cw - $('#UI_Right').width() - $('#UI_Left2').width() - 15);
    }

    var cH=$(window).height()-10;
    $('#UI_MainBox').height(cH);
    $('#MsgBox1').height($('#MsgBox1').height()+cH-$('#UI_Central').height()-$('#UI_Head').height());

    $('#OnLineUser').height($('#OnLineUser').height()+$('#UI_Central').height()-$('#UI_Left2').height());
    $('#UI_Left2').height(cH);
    $('#UI_Left1').height(cH);

    $('#UI_Right').css("height",cH - 50);
    $('#ppiframe').height($('#UI_Right').height()-235);
    $('#OnLine_MV').css("height",cH-$('#UI_Head').height()-100 - $('.NoticeList').height());
    $('.open_items').css("height",cH-$('#UI_Head').height()-100 - $('.NoticeList').height());
    // 中间内容
    $("#OnLineUser_OhterList").height($('#OnLineUser').height());
    $("#OnLineUser_FindList").height($('#OnLineUser').height());
    $("#notice-scrollbox").width($('#UI_Central').width()-83);
    $('.navTab .sub').width($('#UI_Central').width()); // 排行榜的宽度
}
//设置cookies
function setCookie(name,value){
    var Days = 30;
    var exp = new Date();
    exp.setTime(exp.getTime() + Days*24*60*60*1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();

}

//读取cookies
function getCookie(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
 
    if(arr=document.cookie.match(reg))
 
        return (arr[2]);
    else
        return null;
}

// 背景图片切换
function changeSkin(e){
    $(document.body).css({'background': 'url(' + e.rel + ') #663366','background-size': '100% 100%'});
    setCookie("bg_img",e.rel);

}
// 点击图片放大
var msgBlock='';
function MsgShow(str,type){
    $('#MsgBox1').append(str);
    if($('#MsgBox1').find(".msg").length>200){
        $('#MsgBox1').find(".msg:first").remove();
    }
    $(".layim_chatsay img").unbind();
    $(".layim_chatsay img").bind("click",function(){
        if($(this).width()>=300||$(this).height()>=300)open_img($(this).attr('src'),1300,800)
    });
    $(".layim_chatsay img").on("mouseover",function(){
        if($(this).width()>=300||$(this).height()>=300 ){
            $(this).attr("title","点击看大图");$(this).addClass("chat-pic");
        }
    });
    //$(".layim_chatsay  img").attr("title","点击看大图");
    MsgAutoScroll();
}
function MsgAutoScroll(){
    if(toggleScroll == true){
        $('#MsgBox1').scrollTop($('#MsgBox1')[0].scrollHeight);
        //$('#MsgBox1').animate({scrollTop:$('#MsgBox1')[0].scrollHeight}, 1000);

    }
}
// 聊天框
function dragMsgWinx(o){
    //o.firstChild.onmousedown=function(){return false;};
    o.onmousedown=function(a){
        var d=document;if(!a)a=window.event;
        var dy=a.clientY-getId('MsgBox1').offsetHeight;
        var x=getId('MsgBox2').offsetHeight+getId('MsgBox1').offsetHeight;
        if(o.setCapture)
            o.setCapture();
        else if(window.captureEvents)
            window.captureEvents(Event.MOUSEMOVE|Event.MOUSEUP);

        d.onmousemove=function(a){
            if(!a)a=window.event;
            var t=Math.min(Math.max(120,a.clientY-dy),x-40);
            getId('MsgBox1').style.height=t+'px';
            getId('MsgBox2').style.height=x-t+'px';
        };

        d.onmouseup=function(){
            if(o.releaseCapture)
                o.releaseCapture();
            else if(window.captureEvents)
                window.captureEvents(Event.MOUSEMOVE|Event.MOUSEUP);
            d.onmousemove=null;
            d.onmouseup=null;
        };
    };
}

//初始化表情包和彩条
function sendCaitiao(tag){
	var ct=[];
	ct['dyg']='<img src="/room/face/colorbar/dyg.gif">';
	ct['zyg']='<img src="/room/face/colorbar/zyg.gif">';
	ct['gl']='<img src="/room/face/pic/s1.gif"><img src="/room/face/pic/s1.gif"><img src="/room/face/pic/s6.gif"><img src="/room/face/pic/s6.gif"><img src="/room/face/pic/geili_thumb.gif"><img src="/room/face/pic/geili_thumb.gif"><img src="/room/face/pic/s0.gif"><img src="/room/face/pic/s0.gif">';
	ct['zs']='<img src="/room/face/colorbar/zs.gif">';
	ct['xh']='<img src="/room/face/colorbar/xh.gif">';
	app_sendmsg(ct[tag]);
        $('#caitiao').hide();
}
function showFacePanel(e,toinput){
           $.get("room/face/pic/face.html",function(data){
		$('#face').html(data);
		$('#facenav li').on('click',function(){
			var rel = $(this).attr('rel');
			$('#face dl').hide();
			$('#f_'+rel).show();
			$(this).siblings().removeClass('f_cur');
			$(this).addClass('f_cur');

		});	
	}).success(function(){
		$(document).bind('mouseup',function(e){
		if($(e.target).attr('isface')!='1' && $(e.target).attr('isface')!='2')
		{
			$('#face').hide();
			$(document).unbind('mouseup');
		}
		else if($(e.target).attr('isface')=='1')
		{
			var toinput =$('#face').attr("toinput");
                        if($(e.target).attr('src')!=undefined){
			$(toinput).append('<img src="'+$(e.target).attr('src')+'" onresizestart="return false" contenteditable="false">');}
		}
	});


	});
	var offset = $(e).offset();

	var t = offset.top;
	var l = offset.left;
	$('#face').css( {"top" : t-$('#face').outerHeight(), "left":l});
	$('#face').show();
	$('#face').attr("toinput" , toinput);
}
function app_sendmsg(msg){
	var msgid=randStr()+randStr();
	//var str='SendMsg=M=ALL|false|color:#000|'+msgid+'_+_'+encodeURIComponent(msg.str_replace());
          var str = '{"type":"SendMsg","ToChatId":"ALL","IsPersonal":"false","Style":"appsend","tanmu":"false","device":"computer","Txt":"'+msgid+'_+_'+encodeURIComponent(msg.str_replace())+'"}';
         ws.send(str);
         if(check_auth('hd_add')){
         if(msg.indexOf('#app_1')>-1){SysSend.command('handanvideo','handan.mp3');}}
	PutMessage(My.rid,My.chatid,'ALL',My.nick,'ALL','false','appsend',msg.str_replace(),msgid)
}
function  initFaceColobar(){
	 $.get("room/face/colorbar/colorbar.html",function(data){
		$('#caitiao').html(data);
		//彩条
		$('#bt_caitiao').on('click',function(){
			var offset = $('#bt_caitiao').offset();
			var t = offset.top;
			var l = offset.left;
			$('#caitiao').css( {"top" : t-$('#caitiao').outerHeight(), "left":l});
	
			$('#caitiao').show();
			
		});
		$('#caitiaonav li').on('click',function(){

			var rel = $(this).attr('rel');
			$('#caitiao dl').hide();
			$('#c_'+rel).show();
			$(this).siblings().removeClass('f_cur');
			$(this).addClass('f_cur');
		});	
		
	});
	}
