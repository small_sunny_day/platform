//设置一个全局的变量，便于保存验证码
    var code;
    function createCode(id){ //ang是显示验证码input的id
        //首先默认code为空字符串
        code = '';
        //设置长度，这里看需求，我这里设置了4
        var codeLength = 4;
		var id=id?id:'code';
		if(id==''){
			console.log('请传入显示验证码input的id');
			return;
		}
        var codeV = document.getElementById(id);
        //设置随机字符
        var random = new Array(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R', 'S','T','U','V','W','X','Y','Z');
        //循环codeLength 我设置的4就是循环4次
        for(var i = 0; i < codeLength; i++){
            //设置随机数范围,这设置为0 ~ 36
             var index = Math.floor(Math.random()*36);
             //字符串拼接 将每次随机的字符 进行拼接
             code += random[index]; 
        }
        //将拼接好的字符串赋值给展示的Value
        codeV.value = code;
    }

    //下面就是判断是否== 的代码，无需解释
    function yzmvalidate(ang){
		var id=ang.id?ang.id:'input';
		var success=ang.success?ang.success:'';
		var codeid=ang.id?ang.id:'code';
		if(id==''){
			console.log('请传入验证码输入框的id');
			return;
		}
        var oValue = document.getElementById(id).value.toUpperCase();
        if(oValue ==0){
			if(typeof layer!='undefined'){
                layer.msg('请输入验证码');
            }else{
                alert('请输入验证码');
            }
        }else if(oValue != code){
			if(typeof layer!='undefined'){
                layer.msg('验证码不正确，请重新输入');
            }else{
                alert('验证码不正确，请重新输入');
            }
            oValue = ' ';
            createCode(codeid);
        }else{
            if(typeof success=='function'){
				success();
			}else{
				return true;
			}
        }
    }
