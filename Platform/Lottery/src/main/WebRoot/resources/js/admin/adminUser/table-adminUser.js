$(function(){
	//按钮颜色--根据皮肤切换
	var v = localStorage.sccluiSkin;
	var bgcolor='#33AECC';
	if(v=='blue'){
		bgcolor='#1E9FFF';
	}else if(v=='molv'){
		bgcolor='#19a97b';
	}else if(v=='qinxin'){
		bgcolor='#33AECC';
	}
	$('.conb-add').css('background',bgcolor);
	//表格中所展示的字段的字段名
	var zdm=['username','saveTime','edit'];
	var successFn=function(ang){
		if(typeof ang=='string'){
			var ang=$.parseJSON(ang);
		}
		if(ang.flag=='10000'){
			$('#table tbody').children().remove();
			if(ang.list.length==0){
				$('#table table').hide();
				$('#table .zwsj').hide();
				$('<div class="zwsj">').css({'textAlign':'center','lineHeight':'300px','fontSize':'30px'}).text('暂无数据').prependTo('#table').show();
			}else{
				$('#table .zwsj').hide();
				$('#table table').show();
				ghtable.tbody({
					parent:'#table tbody',
					data:ang.list,
					page:ang.current,
					titlezi:zdm
				})
				ghtable.pages({
					parent:'#table',
					pages:ang.page,
					url:ctx+'/alluser/adminuser',
					callback:successFn,
					currPage:ang.current,
				});
				$('#table tbody td[name=edit]').each(function(index){//<span class="table_edit bianji">编辑</span> | 
	    			$(this).html('<span class="table_del">删除</span> | <span class="table_edit updatapass">修改密码</span>')    			
	    		})
			}
		}
	}
	//table中的编辑
	$('#table tbody').delegate('.bianji','click',function(){
		var thisid=$(this).parents('tr').attr('name');
		var that=this;
		var hyType='';
		console.log($(this).parent().parent().find('td[name=userType]').text());
		if($(this).parent().parent().find('td[name=userType]').text()=='计划员'){
			layer.msg('计划员不可编辑');
			return;
		}else if($(this).parent().parent().find('td[name=userType]').text()=='至尊'){
			hyType+='<select id="layer_usertype"><option value="2" selected="selected">至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='皇冠'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3" selected="selected">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='铂金'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4" selected="selected">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='黄金'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5" selected="selected">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='vip'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6" selected="selected">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='普通会员'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7" selected="selected">会员</option></select>';
		}
		console.log(hyType);
		//回显状态
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '编辑',
           /* offset: ['25%','35%'],*/
            offset: '25%',
            content: '<div class="layer_box">'+
            /*'<div class="layer_li"><span>姓名</span><input type="text" id="layer_name" value="'+relaname+'"></div>'+
            '<div class="layer_li"><span>机构</span>'+deptStr+'</div>'+*/
            '<div class="layer_li"><span>会员类型</span>'+hyType+'</div>'+
            '<div style="clear:both;"></div>'+
            '<div id="qr_bianji" class="qr_btn" istrue="false">确认修改</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('.layui-layer-content').css({'height': 'auto'});
                 $('#qr_bianji').css('background',bgcolor);
                 $('#qr_bianji').click(function(){
                		 var setData={};
                    	 setData.userType=$('#layer_usertype').find('option:selected').val();
                    	 var userrole='';
                    	 console.log(setData);
                    	 $.ajax({
                 	    	url:ctx+'/alluser/adminuser/edit',
                 	    	type:'POST',
                 	    	cache: false,
                 	    	data:setData,
                 	    	success:function(res){
                 	    		if(typeof res=='string'){
	                 	   			var res=$.parseJSON(res);
	                 	   		}
                 	    		console.log(res.flag)
                 	    		if(res.flag==10000||res.flag==10000){
                 	    			layer.msg(res.msg);
        		 	    		    setTimeout(function(){
        		 	    		    	layer.closeAll();
        		 	    		    	getDataFn();
        		 	    		    },500)
                 	    		}else{
                 	    			layer.msg(res.msg);
                 	    		}
                 	    	}
                 	    })
                 })
                 
            }
		})
	})
	//新增
	$('#table').delegate('.conb-add','click',function(){
		//回显状态
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '新增',
           /* offset: ['25%','35%'],*/
            offset: '25%',
            content: '<div class="layer_box">'+
            '<div class="layer_li"><span>用户名</span><input type="text" id="layer_name" placeholder=""></div>'+
            '<div class="layer_li"><span>初始密码</span><input type="text" id="layer_pass" placeholder=""></div>'+
            '<div style="clear:both;"></div>'+
            '<div id="qr_add" class="qr_btn" istrue="false">确认修改</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('.layui-layer-content').css({'height': 'auto'});
                 $('#qr_add').css('background',bgcolor);
                 $('#qr_add').click(function(){
                		 var setData={};
                    	 setData.username=$('#layer_name').val().trim();
                    	 setData.password=$('#layer_pass').val().trim();
                    	 setData.state='1';
                    	 console.log(setData);
                    	 $.ajax({
                 	    	url:ctx+'/admin/edit/admin.htm',
                 	    	type:'POST',
                 	    	cache: false,
                 	    	data:setData,
                 	    	success:function(res){
                 	    		console.log(res);
                 	    		if(typeof res=='string'){
	                 	   			var res=$.parseJSON(res);
	                 	   		}
                 	    		console.log(res.flag)
                 	    		if(res.flag==10000||res.flag==10000){
                 	    			layer.msg(res.msg);
        		 	    		    setTimeout(function(){
        		 	    		    	layer.closeAll();
        		 	    		    	getDataFn();
        		 	    		    },500)
                 	    		}else{
                 	    			layer.msg(res.msg);
                 	    		}
                 	    	}
                 	    })
                 })
                 
            }
		})
	})
	//删除
	$('#table tbody').delegate('.table_del','click',function(){
		var username=$(this).parents('tr').find('td[name=username]').text();
		var id=$(this).parents('tr').attr('name');
		if(username=='admin'){
			layer.msg('admin不能删除');
			return;
		}
		layer.confirm('是否删除', {
			  btn: ['是','否'], //按钮
			  success:function(){
				  $('.layui-layer-title').hide();
	              $('.layui-layer-content').css({'text-align':'center'});
	              $('.layui-layer-btn').css({'text-align':'center'});
			  }
			}, function(){
				$.ajax({
         	    	url:ctx+'/admin/edit/admin.htm',
			    	type:'POST',
			    	cache: false,
			    	data:{username:username,state:'3'},
			    	success:function(res){
			    		console.log(res);
			    		if(typeof res=='string'){
             	   			var res=$.parseJSON(res);
             	   		}
         	    		console.log(res.flag)
         	    		if(res.flag==10000||res.flag==10000){
         	    			layer.msg(res.msg);
		 	    		    setTimeout(function(){
		 	    		    	layer.closeAll();
		 	    		    	getDataFn();
		 	    		    },500)
         	    		}else{
         	    			layer.msg(res.msg);
         	    		}
			    	}
			    })
			}, function(){
			  
			});
	})
	//重置密码
	$('#table tbody').delegate('.updatapass','click',function(){
		var username=$(this).parents('tr').find('td[name=username]').text();
		layer.confirm('是否重置密码为123456', {
			  btn: ['是','否'], //按钮
			  success:function(){
				  $('.layui-layer-title').hide();
	              $('.layui-layer-content').css({'text-align':'center'});
	              $('.layui-layer-btn').css({'text-align':'center'});
			  }
			}, function(){
				$.ajax({
					url:ctx+'/admin/edit/admin.htm',
			    	type:'POST',
			    	cache: false,
			    	data:{username:username,state:'4'},
			    	success:function(res){
			    		console.log(res);
			    		if(typeof res=='string'){
             	   			var res=$.parseJSON(res);
             	   		}
         	    		console.log(res.flag)
         	    		if(res.flag==10000||res.flag==10000){
         	    			layer.msg(res.msg);
		 	    		    setTimeout(function(){
		 	    		    	layer.closeAll();
		 	    		    	getDataFn();
		 	    		    },500)
         	    		}else{
         	    			layer.msg(res.msg);
         	    		}
			    	}
			    })
			}, function(){
			  
			})
	})
	//加载头部
	ghtable.title({
		parent:'#table thead',
		courses:['用户名','时间','操作'],
		zdm:zdm
	})
    //获取数据
    var getDataFn=function(){
    	$.ajax({
        	url:ctx+'/admin/admin/list.htm',
        	type:'POST',
        	cache: false,
        	success:function(res){
        		console.log(res);
        		//res.search=[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }]
        		successFn(res)
        	}
        })
    }
    getDataFn();
    /*ghtable.sousuo({
		parent:".sousuo",
		value:"用户名查询",
		url:ctx+'/admin/user.htm',
		callback:successFn,
		data:[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }],
		success:function(selector){
			$(selector).css('background',bgcolor);
		},
		searchkey:'account'
	})*/
})