$(function(){
	//按钮颜色--根据皮肤切换
	var v = localStorage.sccluiSkin;
	var bgcolor='#33AECC';
	if(v=='blue'){
		bgcolor='#1E9FFF';
	}else if(v=='molv'){
		bgcolor='#19a97b';
	}else if(v=='qinxin'){
		bgcolor='#33AECC';
	}
	//表格中所展示的字段的字段名
	var zdm=['title','saveTime','edit'];
	var successFn=function(ang){
		if(typeof ang=='string'){
			var ang=$.parseJSON(ang);
		}
		console.log(ang);
		if(ang.flag=='10000'){
			$('#table tbody').children().remove();
			if(ang.list.length==0){
				$('#table table').hide();
				$('#table .zwsj').hide();
				$('<div class="zwsj">').css({'textAlign':'center','lineHeight':'300px','fontSize':'30px'}).text('暂无数据').prependTo('#table').show();
			}else{
				$('#table .zwsj').hide();
				$('#table table').show();
				ghtable.tbody({
					parent:'#table tbody',
					data:ang.list,
					page:ang.current,
					titlezi:zdm
				})
				ghtable.pages({
					parent:'#table',
					pages:ang.page,
					url:ctx+'/admin/plan/list.htm',
					callback:successFn,
					currPage:Number(ang.current),
					pageNum:'pageNumber'
				});
				var lists=ang.list;
	    		$('#table tbody td[name=edit]').each(function(index){
	    			$(this).html('<span class="table_edit show">查看详情</span>');
	    			$(this).attr('text',lists[index].text)
	    		})
			}
		}
	}
	//查看详情
	$('#table tbody').delegate('.show','click',function(){
		var text=$(this).parent().attr('text');
		//回显状态
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['500px', 'auto'], //宽高
            title: '详情',
           /* offset: ['25%','35%'],*/
            offset: '25%',
            content: '<div class="layer_box">'+text+'</div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)','text-align': 'center'});
                 $('.layui-layer-content').css({'height': 'auto'});
            }
		})
	})
	//加载头部
	ghtable.title({
		parent:'#table thead',
		courses:['标题','时间','操作'],
		zdm:zdm
	})
    //获取数据
    var getDataFn=function(){
    	$.ajax({
        	url:ctx+'/admin/plan/list.htm',
        	"dataType": 'json',
        	"contentType": "application/json",
        	type:'POST',
        	cache: false,
        	success:function(res){
        		console.log(res);
        		//res.search=[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }]
        		successFn(res)
        	}
        })
    }
    getDataFn();
/*    ghtable.sousuo({
		parent:".sousuo",
		value:"用户名查询",
		url:ctx+'/admin/user.htm',
		callback:successFn,
		data:[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }],
		success:function(selector){
			$(selector).css('background',bgcolor);
		},
		searchkey:'account'
	})*/
})