$(function(){
	//按钮颜色--根据皮肤切换
	var v = localStorage.sccluiSkin;
	var bgcolor='#33AECC';
	if(v=='blue'){
		bgcolor='#1E9FFF';
	}else if(v=='molv'){
		bgcolor='#19a97b';
	}else if(v=='qinxin'){
		bgcolor='#33AECC';
	}
	//表格中所展示的字段的字段名
	var zdm=['mobile','username','userType','saveTime','edit'];
	var successFn=function(ang){
		if(typeof ang=='string'){
			var ang=$.parseJSON(ang);
		}
		if(ang.flag=='10000'){
			$('#table tbody').children().remove();
			if(ang.list.length==0){
				$('#table table').hide();
				$('#table .zwsj').hide();
				$('<div class="zwsj">').css({'textAlign':'center','lineHeight':'300px','fontSize':'30px'}).text('暂无数据').prependTo('#table').show();
			}else{
				$('#table .zwsj').hide();
				$('#table table').show();
				ghtable.tbody({
					parent:'#table tbody',
					data:ang.list,
					page:ang.current,
					titlezi:zdm
				})
				ghtable.pages({
					parent:'#table',
					pages:ang.page,
					url:ctx+'/admin/user.htm',
					callback:successFn,
					currPage:ang.current,
					pageNum:'pageNumber'
				});
	    		$('#table tbody td[name=userType]').each(function(index){	    			
	    			switch ($(this).text()) {
					case '1':
						$(this).html('计划员');
						break;
					case '2':
						$(this).html('至尊');
						break;
					case '3':
						$(this).html('皇冠');
						break;
					case '4':
						$(this).html('铂金');
						break;
					case '5':
						$(this).html('黄金');
						break;
					case '6':
						$(this).html('vip');
						break;
					case '7':
						$(this).html('普通会员');
						break;
					default:
						//$(this).text("未知");
						$(this).text("禁用");	
						break;
					}
	    		})
	    		$('#table tbody td[name=edit]').each(function(index){	    			
	    			$(this).html('<span class="table_edit bianji">编辑</span> | <span class="table_del">删除</span> | <span class="table_edit updatapass">修改密码</span> | <span class="table_edit clears">清空余额</span>')    			
	    		})
	    	
			}
		}
	}
	//table中的编辑
	$('#table tbody').delegate('.bianji','click',function(){
		var mobile=$(this).parents('tr').find('td[name=mobile]').text();
		var that=this;
		var hyType='';
		console.log($(this).parent().parent().find('td[name=userType]').text());
		if($(this).parent().parent().find('td[name=userType]').text()=='计划员'){
			layer.msg('计划员不可编辑');
			return;
		}else if($(this).parent().parent().find('td[name=userType]').text()=='至尊'){
			hyType+='<select id="layer_usertype"><option value="2" selected="selected">至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='皇冠'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3" selected="selected">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='铂金'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4" selected="selected">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='黄金'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5" selected="selected">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='vip'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6" selected="selected">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='普通会员'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7" selected="selected">会员</option></select>';
		}
		console.log(hyType);
		//回显状态
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '编辑',
           /* offset: ['25%','35%'],*/
            offset: '25%',
            content: '<div class="layer_box">'+
            /*'<div class="layer_li"><span>姓名</span><input type="text" id="layer_name" value="'+relaname+'"></div>'+
            '<div class="layer_li"><span>机构</span>'+deptStr+'</div>'+*/
            '<div class="layer_li"><span>会员类型</span>'+hyType+'</div>'+
            '<div style="clear:both;"></div>'+
            '<div id="qr_bianji" class="qr_btn" istrue="false">确认修改</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('.layui-layer-content').css({'height': 'auto'});
                 $('#qr_bianji').css('background',bgcolor);
                 $('#qr_bianji').click(function(){
                		 var setData={};
                    	 setData.userType=$('#layer_usertype').find('option:selected').val();
                    	 setData.mobile=mobile;
                    	 console.log(setData);
                    	 $.ajax({
                 	    	url:ctx+'/admin/edit/user.htm',
                 	    	type:'POST',
                 	    	cache: false,
                 	    	data:setData,
                 	    	success:function(res){
                 	    		console.log(res);
                 	    		var res=$.parseJSON(res);
                 	    		if(res.flag==10000){
                 	    			layer.msg(res.msg);
        		 	    		    setTimeout(function(){
        		 	    		    	layer.closeAll();
        		 	    		    	getDataFn();
        		 	    		    },500)
                 	    		}else{
                 	    			layer.msg(res.msg);
                 	    		}
                 	    	}
                 	    })
                 })
                 
            }
		})
	})
	//删除
	$('#table tbody').delegate('.table_del','click',function(){
		var mobile=$(this).parents('tr').find('td[name=mobile]').text();
		layer.confirm('是否删除', {
			  btn: ['是','否'], //按钮
			  success:function(){
				  $('.layui-layer-title').hide();
	              $('.layui-layer-content').css({'text-align':'center'});
	              $('.layui-layer-btn').css({'text-align':'center'});
			  }
			}, function(){
				$.ajax({
			    	url:ctx+'/admin/edit/user.htm',
			    	type:'POST',
			    	cache: false,
			    	data:{mobile:mobile,'delete':'delete'},
			    	success:function(res){
			    		console.log(res);
			    		var res=$.parseJSON(res);
			    		if(res.flag==10000){
			    			layer.msg(res.msg);
		 	    		    setTimeout(function(){
		 	    		    	layer.closeAll();
		 	    		    	getDataFn();
		 	    		    	
		 	    		    },500)
		 	    		}else{
		 	    			layer.msg(res.msg);
		 	    		}
			    	}
			    })
			}, function(){
			  
			});
	})
	//重置密码
	$('#table tbody').delegate('.updatapass','click',function(){
		var mobile=$(this).parents('tr').find('td[name=mobile]').text();
		layer.confirm('是否重置密码为123456', {
			  btn: ['是','否'], //按钮
			  success:function(){
				  $('.layui-layer-title').hide();
	              $('.layui-layer-content').css({'text-align':'center'});
	              $('.layui-layer-btn').css({'text-align':'center'});
			  }
			}, function(){
				$.ajax({
			    	url:ctx+'/admin/edit/user.htm',
			    	type:'POST',
			    	cache: false,
			    	data:{mobile:mobile,passWord:'123456'},
			    	success:function(res){
			    		console.log(res);
			    		var res=$.parseJSON(res);
			    		if(res.flag==10000){
			    			layer.msg(res.msg);
		 	    		    setTimeout(function(){
		 	    		    	layer.closeAll();
		 	    		    	getDataFn();
		 	    		    	
		 	    		    },500)
		 	    		}else{
		 	    			layer.msg(res.msg);
		 	    		}
			    	}
			    })
			}, function(){
			  
			})
	})
	//清空余额
	$('#table tbody').delegate('.clears','click',function(){
		var mobile=$(this).parents('tr').find('td[name=mobile]').text();
		layer.confirm('是否清空余额为0', {
			  btn: ['是','否'], //按钮
			  success:function(){
				  $('.layui-layer-title').hide();
	              $('.layui-layer-content').css({'text-align':'center'});
	              $('.layui-layer-btn').css({'text-align':'center'});
			  }
			}, function(){
				$.ajax({
			    	url:ctx+'/admin/edit/user.htm',
			    	type:'POST',
			    	cache: false,
			    	data:{mobile:mobile,passWord:'123456',type:'1'},
			    	success:function(res){
			    		console.log(res);
			    		var res=$.parseJSON(res);
			    		if(res.flag==10000){
			    			layer.msg(res.msg);
		 	    		    setTimeout(function(){
		 	    		    	layer.closeAll();
		 	    		    	getDataFn();
		 	    		    	
		 	    		    },500)
		 	    		}else{
		 	    			layer.msg(res.msg);
		 	    		}
			    	}
			    })
			}, function(){
			  
			})
	})
	//加载头部
	ghtable.title({
		parent:'#table thead',
		courses:['用户名','昵称','会员类型','时间','操作'],
		zdm:zdm
	})

    //获取数据
    var getDataFn=function(){
    	$.ajax({
        	url:ctx+'/admin/user.htm',
        	"dataType": 'json',
        	"contentType": "application/json",
        	type:'POST',
        	cache: false,
        	success:function(res){
        		console.log(res);
        		//res.search=[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }]
        		successFn(res)      		
        	}
        })
    }
    getDataFn();
    ghtable.sousuo({
		parent:".sousuo",
		value:"用户名查询",
		url:ctx+'/admin/user.htm',
		callback:successFn,
		searchkey:'username',
		success:function(selector){
			$(selector).css('background',bgcolor);
		},
	})
    
// 用户名查找
	$(".findicon").click(function(){	
		if( $(".findtext").val().trim()=="" ){
			layer.alert("请输入用户名进行查找");
			return;
		}
		var search ={};
		var name = $(".findtext").val().trim();
		search.username=name;
		$.ajax({
			"dataType" : 'json',
			"type" : "POST",
			"url" : ctx+'/admin/edit/user.htm',
			"cache" : false,
			"data":search,
			"success" : function(data) {
			
			}
		})
	})
    
    
    
    
    
})