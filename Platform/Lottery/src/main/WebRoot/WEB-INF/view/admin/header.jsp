<%@ page language="java" pageEncoding="UTF-8"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="reqURL" value="${pageContext.request.servletPath }" />


<!-- BEGIN HEADER -->
<header class="layout-header">
			<span class="header-logo"><img src="${ctx}/resources/images/logo.png" style="height: 30px;"></span> 
			<a class="header-menu-btn" href="javascript:;"><i class="icon-font">&#xe600;</i></a>
			<ul class="header-bar">
				<li class="header-bar-role"><a href="javascript:;">${admin.username}</a></li>
				<li class="header-bar-nav">
					<a href="javascript:;"><span class="username" thisid="${admin.id}"><i class="icon-font" style="margin-left:5px;">&#xe60c;${admin.username}</i></span></a>
					<ul class="header-dropdown-menu">
						<li><a href="javascript:;" id="quanping">全屏</a></li>
						<li><a href="javascript:;" id="updatePass">修改密码</a></li>
						<li><a href="${ctx}/admin/loginout.htm"><i class="fa fa-key"></i> 退出</a></li>
					</ul>
				</li>
				<li class="header-bar-nav"> 
					<a href="javascript:;" title="换肤"><i class="icon-font">&#xe608;</i></a>
					<ul class="header-dropdown-menu right dropdown-skin">
						<li><a href="javascript:;" data-val="qingxin" title="清新">清新</a></li>
						<li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
						<li><a href="javascript:;" data-val="molv" title="墨绿">墨绿</a></li>
					</ul>
				</li>
			</ul>
		</header>
<!-- END HEADER -->