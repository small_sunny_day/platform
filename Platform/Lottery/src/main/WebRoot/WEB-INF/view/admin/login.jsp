<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%> 
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<%=path %>/resources/css/pc/font-awesome.min.css">
     <link rel="stylesheet" href="<%=path %>/resources/css/n-login.css">
     <script src="<%=path %>/resources/js/jquery-2.1.4.js"></script>
     <script src="<%=path %>/resources/js/layer.js"></script>
     <script language="javascript" type="text/javascript">
     var basePath='<%=basePath%>';
        var code ; //在全局 定义验证码
        function createCode(){
            code = new Array();
            var codeLength = 4;//验证码的长度
            var checkCode = document.getElementById("checkCode");
            checkCode.value = "";
            var selectChar = new Array(2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');

            for(var i=0;i<codeLength;i++) {
                var charIndex = Math.floor(Math.random()*32);
                code +=selectChar[charIndex];
            }
            if(code.length != codeLength){
                createCode();
            }
            checkCode.value = code;
        }
        function validate () {
            var inputCode = document.getElementById("input1").value.toUpperCase();
        	if(inputCode.length <=0) {
                layer.msg("请输入验证码！");               
                return false;
	        }else if(inputCode != code ){
                layer.msg("验证码输入错误！");               
                createCode();
                return false;
            }else{
                return true;
            }
        }

    </script>
</head>
<body onload="createCode()">
<div class="logn-box" >
     <div class="title"><img alt="信彩" src="<%=path %>/resources/images/logo.png"></div>
     <div class="logn-con" style="height:200px;margin-top: 60px;border-radius: 5px;box-shadow: none;width:280px;">
        <div class="yh-box">
            <span class="fa fa-user"></span>
            <input type="text"  placeholder="用户名" name="username">
        </div>
        <div class="yh-box">
            <span class="fa fa-key"></span>
            <input type="password"  placeholder="登录密码" name="password">
        </div>
     		 <span>验证码：</span>
     		 <input type="text" id="input1" />
		<input type="button" id="checkCode" class="code" style="width:60px" onClick="createCode()" value=""/>
      <!--  <input id="Button1"  type="button" value="立即登录" /> -->
         <input id="Button1"  type="submit" value="立即登录" />
    </div>
</div>

<script>
	$(function(){
		$('#Button1').click(function(){
			if($('input[name=username]').val().trim()==''){
				layer.msg('请输入用户名');
				return;
			}
			if($('input[name=password]').val().trim()==''){
				layer.msg('请输入用户名');
				return;
			}
			if(validate()){
				$.ajax({
              		url:basePath+'admin/login.htm',
              		type:'POST',
              		data:{username:$('input[name=username]').val().trim(),passWord :$('input[name=password]').val().trim()},
              		success:function(data){
              			var data=$.parseJSON(data);
              			if(data.flag='10000'){
              				layer.msg(data.msg);
              				location.reload();
              			}else{
              				//location.href=basePath+'admin/index.htm';
              				
              			}
              		}
              	})
			}
		})
		$(document).keydown(function(e){
			if(e.keyCode==13){
				if($('input[name=username]').val().trim()==''){
					layer.msg('请输入用户名');
					return;
				}
				if($('input[name=password]').val().trim()==''){
					layer.msg('请输入用户名');
					return;
				}
				if(validate()){
					$.ajax({
	              		url:basePath+'admin/login.htm',
	              		type:'POST',
	              		data:{username:$('input[name=username]').val().trim(),passWord :$('input[name=password]').val().trim()},
	              		success:function(data){
	              			var data=$.parseJSON(data);
	              			console.log(data.flag)
	              			if(data.flag==10000){
	              				//location.href=basePath+'admin/index.htm';
	              				location.reload();
	              			}else{
	              				layer.msg(data.msg);
	              			}
	              		}
	              	})
				}
			}
		})
	})
</script>
</body>
</html>