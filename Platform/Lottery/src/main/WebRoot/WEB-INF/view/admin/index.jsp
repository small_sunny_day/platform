<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
 String path = request.getContextPath();

%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="js_ver" value="20150618000000" />
<c:set var="css_ver" value="20150618000000" />
<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-CN" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh-CN" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="zh-CN" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<meta name="keywords" content="信彩">
	<meta name="description" content="信彩欢迎您">
    <title>53彩票管理平台</title>
	<link rel="stylesheet" href="${ctx}/resources/css/sccl.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/resources/skin/qingxin/skin.css" id="layout-skin"/>
	<script>
		var ctx='${ctx}';
	</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="page-header-fixed page-full-width" id="body" thidname="${admin.username}">
	<%@ include file="/WEB-INF/view/admin/header.jsp"%>
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
	<%@ include file="/WEB-INF/view/admin/left.jsp"%>
	<div class="layout-admin">
		<section class="layout-main "> <!--full-page-->
			<div class="layout-main-tab">
				<button class="tab-btn btn-left"><i class="icon-font">&#xe60e;</i></button>
                <nav class="tab-nav">
                    <div class="tab-nav-content">
                        <a href="javascript:;" class="content-tab active" data-id="home.html">计划管理</a>
                    </div> 
                </nav>
                <button class="tab-btn btn-right"><i class="icon-font">&#xe60f;</i></button>
			</div>
			<div class="layout-main-body">
				<iframe class="body-iframe" name="iframe0" width="100%" height="99%" src="<%=path %>/admin/plan/view.htm" frameborder="0" data-id="home.html" seamless></iframe>
			</div>
		</section>
	</div>
	<script type="text/javascript" src="${ctx}/resources/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="${ctx}/resources/js/sccl-util.js"></script>
	</div>
	<!-- END CONTAINER -->
	<%@ include file="/WEB-INF/view/admin/footer.jsp"%>
	<script type="text/javascript" src="${ctx}/resources/js/layer.js"></script>
	<link rel="stylesheet" href="${ctx}/resources/css/mylayer.css">	
	<script type="text/javascript" src="${ctx}/resources/js/sccl.js"></script>
	<script type="text/javascript" src="${ctx}/resources/js/ghPublic.js"></script>
</body>
<!-- END BODY -->
</html>

