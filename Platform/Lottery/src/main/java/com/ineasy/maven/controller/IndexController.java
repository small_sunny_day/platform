package com.ineasy.maven.controller;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ineasy.maven.po.Message;
import com.ineasy.maven.po.Sign;
import com.ineasy.maven.po.User;
import com.ineasy.maven.service.IndexService;
import com.ineasy.maven.util.tool.TimeUtils;
import com.mysql.jdbc.TimeUtil;
import com.navict.jrobot.core.annotations.ApiLog;

import net.sf.json.JSONObject;

/**
 * 平台控制层
 * @author Jia Xinlei
 *
 */
@Controller
public class IndexController {

	@Resource
	private IndexService indexService;

	@RequestMapping(value = "/index",method=RequestMethod.GET)
	@ApiLog(name="首页")
	public ModelAndView index(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav = new ModelAndView();
		String plan = indexService.findPlan();
		String result = indexService.notice();
		request.setAttribute("plan", plan);
		request.setAttribute("notice", result);
		JSONObject json = (JSONObject)request.getSession().getAttribute("user");
		if(json != null){
			request.setAttribute("money", indexService.money(request));
		}
		mav.setViewName("jsp/chatMb");
		return mav;
	}

	@RequestMapping(value = "/chat/list",method=RequestMethod.POST)
	@ApiLog(name="聊天记录")
	@ResponseBody
	public String cahtList(HttpServletRequest request,HttpServletResponse response){
		String result = indexService.chatList();
		return result;
	}
	
	@RequestMapping(value = "/chat/headimage",method=RequestMethod.POST)
	@ApiLog(name="根据username获取头像")
	@ResponseBody
	public String cahtimgae(HttpServletRequest request,HttpServletResponse response){
		JSONObject json = (JSONObject)request.getSession().getAttribute("user");
		Date date =  new Date();
		String date1 = TimeUtils.formatTime(date);
		json.put("datatime", date1);
		return json.toString();
	}
	

	@RequestMapping(value = "/pc/index",method=RequestMethod.GET)
	@ApiLog(name="PC端首页")
	public ModelAndView pcIndex(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav = new ModelAndView();
		String result = indexService.notice();
		request.setAttribute("notice", result);
		JSONObject json = (JSONObject)request.getSession().getAttribute("user");
		if(json != null){
			request.setAttribute("money", indexService.money(request));
		}
		mav.setViewName("pc/chatPc");
		return mav;
	}

	@RequestMapping(value = "/register",method=RequestMethod.POST)
	@ApiLog(name="注册")
	@ResponseBody
	public String register(HttpServletRequest request,HttpServletResponse response,@Valid User vo){
		String result = indexService.register(vo);
		return result;
	}

	@RequestMapping(value = "/login",method=RequestMethod.POST)
	@ApiLog(name="登录")
	@ResponseBody
	public String login(HttpServletRequest request,HttpServletResponse response,@Valid User vo){
		String result = indexService.login(request,vo);
		return result;
	}

	@RequestMapping(value = "/sign",method=RequestMethod.POST)
	@ApiLog(name="签到")
	@ResponseBody
	public String sign(HttpServletRequest request,HttpServletResponse response,@Valid Sign vo){
		String result = indexService.sign(vo);
		return result;
	}

	@RequestMapping(value = "/friend/list",method=RequestMethod.POST)
	@ApiLog(name="好友列表")
	@ResponseBody
	public String friendList(HttpServletRequest request,HttpServletResponse response){
		JSONObject jsonObj = (JSONObject)request.getSession().getAttribute("user");
		String result = indexService.friendList(jsonObj);
		return result;
	}

	@RequestMapping(value = "/sign/note",method=RequestMethod.POST)
	@ApiLog(name="签到记录")
	@ResponseBody
	public String signNote(HttpServletRequest request,HttpServletResponse response,@Valid Sign vo){
		String result = indexService.signNote(vo);
		return result;
	}

	@RequestMapping(value = "/sign/list",method=RequestMethod.POST)
	@ApiLog(name="签到列表")
	@ResponseBody
	public String signList(HttpServletRequest request,HttpServletResponse response,@Valid Sign vo){
		String result = indexService.signList(vo);
		return result;
	}

	@RequestMapping(value = "/plan",method=RequestMethod.POST)
	@ApiLog(name="计划")
	@ResponseBody
	public String plan(HttpServletRequest request,HttpServletResponse response,@Valid Sign vo){
		String result = indexService.plan();
		System.out.println(result);
		return result;
	}

	@RequestMapping(value = "/benth",method=RequestMethod.GET)
	@ApiLog(name="benth")
	@ResponseBody
	public String benth(HttpServletRequest request,HttpServletResponse response,@Valid Sign vo){
		String a = indexService.benth();
		return a;
	}

	@RequestMapping(value = "/loginout", method = RequestMethod.GET)
	@ApiLog(name = "登出")
	@ResponseBody
	public String loginOut(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().removeAttribute("user");
		return "10000";
	}

	@RequestMapping(value = "/save/chat", method = RequestMethod.POST)
	@ApiLog(name = "保存聊天信息")
	@ResponseBody
	public void saveChat(HttpServletRequest request, HttpServletResponse response,@Valid Message vo) {
		indexService.saveChat(vo);
	}

	@RequestMapping(value = "/update/img", method = RequestMethod.POST)
	@ResponseBody
	public String updateImg(HttpServletRequest request, HttpServletResponse response) {
		String result = indexService.updateImg(request);
		return result;
	}

}
