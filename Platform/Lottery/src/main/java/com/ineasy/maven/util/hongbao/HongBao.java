package com.ineasy.maven.util.hongbao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ineasy.maven.util.tool.UUIDUtils;

public class HongBao {

	public List<Map<String,Object>> hb(double total,int num,double min){
		//Map<Double,Double> map = new HashMap<Double, Double>();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		String hbId = UUIDUtils.getUUID();	
		Map<String,Object> map = null;
		for(int i=1;i<num;i++){
			map = new HashMap<>();
			double safe_total=(total-(num-i)*min)/(num-i);
			double money=Math.random()*(safe_total-min)+min;
			BigDecimal money_bd=new BigDecimal(money);
			money=money_bd.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue(); 
			total=total-money;
			BigDecimal total_bd=new BigDecimal(total);
			total=total_bd.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
			System.out.println("第"+i+"个红包："+money+",余额为:"+total+"元");
			map.put("hbid", hbId);
			map.put("money", money);
			list.add(map);
		}
		map = new HashMap<>();
		map.put("hbid", hbId);
		map.put("money", total);
		list.add(map);
		System.out.println("第"+num+"个红包："+total+",余额为:0元");
		System.out.println(list.toString());
		//map.put(total, 0.0);
		//list.add(map);
		//maplist.put(hbId, list);
		//System.out.println(maplist);
		return list;
	}

	void zb(){
		for(int a=0;a<=10000;a++){
			if(a % 1000== 0) 
				System.out.println (a);
		}
	}

	public static void main(String[] args) {  
		//随机一个188.88  5个红包  
		HongBao dd = new HongBao();  
		//单位是分  
		dd.hb(188.88,5,0.2);  
	}  

}
