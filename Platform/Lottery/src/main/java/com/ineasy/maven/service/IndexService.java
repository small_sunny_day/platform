package com.ineasy.maven.service;

import javax.servlet.http.HttpServletRequest;

import com.ineasy.maven.po.Message;
import com.ineasy.maven.po.Red;
import com.ineasy.maven.po.Sign;
import com.ineasy.maven.po.User;

import net.sf.json.JSONObject;

public interface IndexService {
	
	//登录
	String login(HttpServletRequest request,User vo);
	
	//注册
	String register(User vo);
	
	//签到
	String sign(Sign vo);
	
	//签到记录
	String signNote(Sign vo);
	
	//签到列表
	String signList(Sign vo);
	
	//好友列表
	String friendList(JSONObject jsonObj);
	
	//计划
	String plan();
	
	String findPlan();
	
	String notice();
	
	String money(HttpServletRequest request);
	
	String benth();

	User queryuser(String userid);
	
	String red(Red vo);
	
	String sendred(Red vo);
	
	void saveChat(Message vo);
	
	String chatList();
	
	String updateImg(HttpServletRequest request);

	String chatheadimage(String headimage);
	
}
