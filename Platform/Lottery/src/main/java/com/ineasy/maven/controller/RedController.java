package com.ineasy.maven.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ineasy.maven.po.Red;
import com.ineasy.maven.service.IndexService;
import com.navict.jrobot.core.annotations.ApiLog;


@Controller
public class RedController {

	@Resource
	private IndexService indexService;
	
	Map<String,List<Integer>> hashmap = new HashMap<String,List<Integer>>();
	List<Map<String,List<Integer>>> listhb = new ArrayList<Map<String,List<Integer>>>();
	
	
	@RequestMapping(value = "/red",method=RequestMethod.POST)
	@ApiLog(name="红包")
	@ResponseBody
	public String red(HttpServletRequest request,HttpServletResponse response,@Valid Red vo){
		/*int money = vo.getMoney();
		int count = vo.getNumber();
		List<Integer> list =new ArrayList<Integer>(); 
		HongBaoAlgorithm dd = new HongBaoAlgorithm();  
		list = dd.splitRedPackets(money, count);
		String hbId = UUIDUtils.getUUID();
		hashmap.put(hbId,list);
		listhb.add(hashmap);*/
		String hbId = indexService.red(vo);
		return	hbId;
	}
	
	
	/*@RequestMapping(value = "/sendred",method=RequestMethod.POST)
	@ApiLog(name="获取红包")
	@ResponseBody
	public String sendred(HttpServletRequest request,HttpServletResponse response,@Valid Red vo){
		int count = 0;
		String hbid = vo.getHbid();
		List<Integer> list = new ArrayList<Integer>(); 
		for(int i=0;i<listhb.size();i++){
			for(Map.Entry<String, List<Integer>> entry : listhb.get(i).entrySet()){
				if (entry.getKey()==hbid){
					for(int j=0;j<entry.getValue().size();j++){
						list.add(entry.getValue().get(j));
						entry.getValue().remove(i);
						if(entry.getValue().equals("") || entry.getValue() == null){
							list.remove(entry.getValue());
						}
						if (!StringUtils.isNullOrEmpty(vo.getUserid())){
							User user = new User();
							user = indexService.queryuser(vo.getUserid());
							if (user.getMoney()==null){
								int moneycount = 0;
								count = moneycount+list.get(i);
							}else{
								int moneycount = user.getMoney();
								count = moneycount+list.get(i);
							}
							indexService.updateUserManey(count,vo.getUserid());
						}
					}
				}
			}
		}
		return	list.toString();
	}*/
	
	@RequestMapping(value = "/sendred",method=RequestMethod.POST)
	@ApiLog(name="获取红包")
	@ResponseBody
	public String sendred(HttpServletRequest request,HttpServletResponse response,@Valid Red vo){
		String result = indexService.sendred(vo);
		return result;
	}
	
}
