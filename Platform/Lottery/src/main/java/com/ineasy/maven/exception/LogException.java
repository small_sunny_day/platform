package com.ineasy.maven.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @desc:全局异常
 * @author: Jia xinlei
 * @date:2016年11月4日 下午5:13:36
 * @version: 1.0.0
 */
public class LogException implements HandlerExceptionResolver {

	@ResponseBody
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object obj,
			Exception ex) {
		System.out.println(ex.getMessage());
		ModelAndView mView = new ModelAndView();
		mView.addObject("");
		mView.setViewName("/error");
		return mView;
	}

}
