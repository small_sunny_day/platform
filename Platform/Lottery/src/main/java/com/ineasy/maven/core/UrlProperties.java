package com.ineasy.maven.core;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

public class UrlProperties {
	public static String getUrlProperties() {
		Properties p=new Properties();
		String url = "";
		try {
			InputStream in=PageProperties.class.getClassLoader().getResourceAsStream("url.properties");
			p.load(in);												     
			Iterator<String> it=p.stringPropertyNames().iterator();
			while(it.hasNext()){
				String key=it.next();
				//System.out.println(key+":"+p.getProperty(key));
				if(key.equals("url")){
					url= p.getProperty(key); 
				}
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return url;
	}
	
	public static long getTimeProperties() {
		Properties p=new Properties();
		long time = 0;
		try {
			InputStream in=PageProperties.class.getClassLoader().getResourceAsStream("url.properties");
			p.load(in);												     
			Iterator<String> it=p.stringPropertyNames().iterator();
			while(it.hasNext()){
				String key=it.next();
				//System.out.println(key+":"+p.getProperty(key));
				if(key.equals("time")){
					time= Long.parseLong(p.getProperty(key)); 
				}
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time;
	}
	
}
