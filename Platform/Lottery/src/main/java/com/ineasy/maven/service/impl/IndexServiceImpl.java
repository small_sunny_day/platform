package com.ineasy.maven.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ineasy.maven.core.JsonApi;
import com.ineasy.maven.core.UrlProperties;
import com.ineasy.maven.mapper.AdminMapper;
import com.ineasy.maven.mapper.UserMapper;
import com.ineasy.maven.po.Message;
import com.ineasy.maven.po.Plan;
import com.ineasy.maven.po.Red;
import com.ineasy.maven.po.Sign;
import com.ineasy.maven.po.User;
import com.ineasy.maven.service.IndexService;
import com.ineasy.maven.util.hongbao.HongBao;
import com.ineasy.maven.util.tool.TimeUtils;
import com.ineasy.maven.util.tool.UUIDUtils;
import com.mysql.jdbc.StringUtils;

import net.sf.json.JSONObject;

@Service
public class IndexServiceImpl extends FileAlterationListenerAdaptor implements IndexService{

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private AdminMapper adminMapper;

	private JsonApi jsonApi;

	//登录
	public String login(HttpServletRequest request,User vo) {
		String mobile = vo.getMobile();
		JSONObject json = (JSONObject)request.getSession().getAttribute("user");
		if(json != null){
			String usern = json.getString("mobile");
			if(usern.equals(vo.getMobile())){
				jsonApi = new JsonApi(10001, "用户已在线,不可重复登录");
			}else{
				if(!StringUtils.isNullOrEmpty(mobile)){
					User user = userMapper.findUserByMobile(mobile.trim());
					if(user != null){
						if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
							if(vo.getPassWord().equals(user.getPassWord())){
								user.setPassWord("");
								request.getSession().setAttribute("user", JSONObject.fromObject(user));
								jsonApi = new JsonApi(10000, "登录成功");
								jsonApi.SetObj("user", JSONObject.fromObject(user));
							}else{
								jsonApi = new JsonApi(10002, "用户名或密码错误");
							}
						}else{
							jsonApi = new JsonApi(10001, "密码不能为空");
						}
					}else{
						jsonApi = new JsonApi(10001, "用户不存在");
					}
				}else{
					jsonApi = new JsonApi(10001, "用户名不能为空");
				}
			}
		}else{
			if(!StringUtils.isNullOrEmpty(mobile)){
				User user = userMapper.findUserByMobile(mobile.trim());
				if(user != null){
					if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
						if(vo.getPassWord().equals(user.getPassWord())){
							user.setPassWord("");
							request.getSession().setAttribute("user", JSONObject.fromObject(user));
							jsonApi = new JsonApi(10000, "登录成功");
							jsonApi.SetObj("user", JSONObject.fromObject(user));
						}else{
							jsonApi = new JsonApi(10002, "用户名或密码错误");
						}
					}else{
						jsonApi = new JsonApi(10001, "密码不能为空");
					}
				}else{
					jsonApi = new JsonApi(10001, "用户不存在");
				}
			}else{
				jsonApi = new JsonApi(10001, "用户名不能为空");
			}
		}
		return jsonApi.ToJson();
	}

	//注册
	public String register(User vo) {
		String mobile = vo.getMobile();
		String username = vo.getUsername();
		if(!StringUtils.isNullOrEmpty(mobile) && !StringUtils.isNullOrEmpty(username)){
			List<User> user = userMapper.findUser(mobile,username);
			if(user.size() == 0){
				if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
					String userType = vo.getUserType();
					if(!StringUtils.isNullOrEmpty(userType)){
						vo.setUserType(userType);
					}else{
						vo.setUserType("7");
					}
					int res = userMapper.register(vo);
					if(res == 1){
						jsonApi = new JsonApi(10000, "注册成功");
					}else{
						jsonApi = new JsonApi(10001, "网络异常");
					}
				}else{
					jsonApi = new JsonApi(10001, "密码不能为空");
				}
			}else{
				jsonApi = new JsonApi(10001, "用户已存在");
			}
		}else{
			jsonApi = new JsonApi(10001, "不能为空");
		}
		return jsonApi.ToJson();
	}

	//签到
	@Override
	public String sign(Sign vo) {
		String userId = vo.getUserId();
		if(!StringUtils.isNullOrEmpty(userId)){
			vo.setSignDate(TimeUtils.getDate());
			String result = userMapper.findSignByUserId(vo);
			if(result == null){
				vo.setId(UUIDUtils.getUUID());
				vo.setBranch(10);
				vo.setRemarks("签到奖励10积分");
				int res = userMapper.sign(vo);
				if(res == 1){
					jsonApi = new JsonApi(10000, "签到成功");
				}else{
					jsonApi = new JsonApi(10001, "网络异常");
				}
			}else{
				jsonApi = new JsonApi(10001, "今天已签到");
			}
		}else{
			jsonApi = new JsonApi(10001, "参数不能为空");
		}
		return jsonApi.ToJson();
	}

	//签到记录
	@Override
	public String signNote(Sign vo) {
		String userId = vo.getUserId();
		if(!StringUtils.isNullOrEmpty(userId)){
			vo.setSignDate(TimeUtils.getMonth());
			List<String> list = userMapper.signNote(vo);
			jsonApi = new JsonApi(10000, "查询成功");
			jsonApi.SetObj("list",list);
		}else{
			jsonApi = new JsonApi(10001, "参数不能为空");
		}
		return jsonApi.ToJson();
	}

	//签到列表
	@Override
	public String signList(Sign vo) {
		String userId = vo.getUserId();
		if(!StringUtils.isNullOrEmpty(userId)){
			vo.setSignDate(TimeUtils.getMonth());
			List<Sign> list = userMapper.signList(vo);
			jsonApi = new JsonApi(10000, "查询成功");
			jsonApi.SetObj("list",list);
		}else{
			jsonApi = new JsonApi(10001, "参数不能为空");
		}
		return jsonApi.ToJson();
	}

	/**
	 * 好友列表
	 */
	@Override
	public String friendList(JSONObject jsonObj) {
		try {
			List<User> list = null;
			if (jsonObj != null && !"".equals(jsonObj)) {
				if (jsonObj.containsKey("id")) {
					String userId = "";
					userId = jsonObj.getString("id");
					list = userMapper.friendListById(userId);
				}else{
					list = userMapper.friendList();
				}

			}else{
				list = userMapper.friendList();
			}
			jsonApi = new JsonApi(10000, "查询成功");
			jsonApi.SetObj("list",list);
		} catch (Exception e) {
			// TODO: handle exception
			jsonApi = new JsonApi(10001,e.getMessage());
		}
		return jsonApi.ToJson();
	}

	/**
	 * 
	 */
	@SuppressWarnings("static-access")
	@Override
	public String plan() {
		try {
			long time = UrlProperties.getTimeProperties();
			System.out.println("休眠时间为："+time+"秒");
			new Thread().sleep(time);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//String url = request.getSession().getServletContext().getRealPath("");
		//url = url.substring(0,url.lastIndexOf("webapps"))+"/fileupload";
		String url = UrlProperties.getUrlProperties();
		File f = new File(url);
		if(!f.exists()){
			f.mkdirs();
		}
		String filePath = url+ "/1.txt";
		StringBuffer sbf = new StringBuffer();
		int result = 0;
		String titile = null,number = "";
		try {
			String encoding="GBK";
			File file=new File(filePath);
			if(file.isFile() && file.exists()){ //判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file),encoding);//考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				List<String> list = new ArrayList<>();
				String lineTxt = null;
				while((lineTxt = bufferedReader.readLine()) != null){
					sbf.append(lineTxt+"</br>");
					list.add(lineTxt+"");
				}
				if(list.size() > 0){
					titile = list.get(0);
					number = titile.substring(titile.lastIndexOf("开奖号码") + 5,titile.length()-1);
					titile = titile.substring(0, titile.lastIndexOf("开奖号码") - 1);
				}
				read.close();
				String res = adminMapper.selectPlan();
				if(res == null || "".equals(res)){
					if(!StringUtils.isNullOrEmpty(sbf.toString())){
						result =  adminMapper.savePlan(UUIDUtils.getUUID(),titile,number, sbf.toString());
					}
				}else{
					if(!res.equals(sbf.toString())){
						result =  adminMapper.savePlan(UUIDUtils.getUUID(),titile,number, sbf.toString());
					}
				}
			}else{
				jsonApi = new JsonApi(10001, "找不到指定文件");
				System.out.println("找不到指定文件");
				return jsonApi.ToJson();
			}
			if(result != 0){
				jsonApi = new JsonApi(10000, "成功");
				jsonApi.SetObj("obj",sbf.toString());
				jsonApi.SetObj("number",number);
				//jsonApi.SetObj("titile",titile.substring(titile.lastIndexOf("第"),titile.length()));
				jsonApi.SetObj("titile",titile);
			}else{
				jsonApi = new JsonApi(10001, "数据未发生改变");
			}
		} catch (Exception e) {
			jsonApi = new JsonApi(10001,e.getMessage());
		}
		return jsonApi.ToJson();
	}

	@Override
	public String findPlan() {
		Plan plan = null;
		try {
			plan = userMapper.findPlan();
			jsonApi = new JsonApi(10000, "成功");
			jsonApi.SetObj("plan",plan);
		} catch (Exception e) {
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	//公告列表
	@Override
	public String notice() {
		List<String> list = null;
		try {
			list = userMapper.notice();
			jsonApi = new JsonApi(10000, "成功");
			jsonApi.SetObj("list",list);
		} catch (Exception e) {
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	//公告列表
	@Override
	public String money(HttpServletRequest request) {
		JSONObject json = (JSONObject)request.getSession().getAttribute("user");
		String mobile = json.getString("mobile");
			User user = userMapper.findUserByMobile(mobile);
		return user.getMoney()+"";
	}

	@Override
	public String benth() {
		List<User> list = new ArrayList<>();
		User u = null;
		for(int i = 0 ;i <500;i++){
			u = new User();
			u.setId(UUIDUtils.getUUID());
			u.setMobile("15634011"+UUIDUtils.getRandom());
			u.setUsername("游客"+UUIDUtils.getRandom());
			list.add(u);	
		}
		int result = 0;
		try {
			result = userMapper.benth(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result+"";
	}


	/**
	 * 根据ID查询用户信息
	 */
	@Override
	public User queryuser(String userid) {
		User user = new User();
		if(!StringUtils.isNullOrEmpty(userid)){
			user = adminMapper.findUserByUserID(userid);
		}
		return user;
	}


	/*	@Override
	public String updateUserManey(Integer moneycount,String userid) {
		if(!StringUtils.isNullOrEmpty(userid)){
			User user = adminMapper.findUserByUserID(userid);
			if(user != null){
				int result = adminMapper.updateusermoney(moneycount,userid);
				if(result == 1){
					jsonApi = new JsonApi(10000, "修改成功");
				}else{
					jsonApi = new JsonApi(10001, "网络异常，请稍候重试");
				}
			}else{
				jsonApi = new JsonApi(10001, "网络异常，请稍候重试");
			}
		}else{
			jsonApi = new JsonApi(10001, "用户ID为空");
		}
		return jsonApi.ToJson();
	}
	 */
	/**
	 * 发红包
	 */
	@Override
	public String red(Red vo) {
		HongBao hb = new HongBao();
		if(vo != null && !"".equals(vo)){
			double money = vo.getMoney();
			int count = vo.getNumber();
			List<Map<String,Object>> list = hb.hb(money, count, 0.1);
			try {
				userMapper.red(list);
				return (String)list.get(0).get("hbid");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return e.getMessage();
			}
		}else{
			return "参数不能为空";
		}
	}

	/**
	 * 抢红包
	 */
	@Override
	public String sendred(Red vo) {
		Thread t = new Thread();
		try {
			t.join();
			if(vo != null && !"".equals(vo)){
				List list = userMapper.sendred(vo);
				if(list.size() == 0){
					List<Red> lis = userMapper.sendred2(vo);
					if(lis.size() > 0 ){
						Red r = lis.get(0);
						r.setUserid(vo.getUserid());
						userMapper.update(r);
						jsonApi = new JsonApi(10000,"成功");
						jsonApi.SetObj("money",r.getMoney());
						if (!StringUtils.isNullOrEmpty(vo.getUserid())){
							User user = adminMapper.findUserByUserID(vo.getUserid());
							double count = 0.0;
							if (user.getMoney()==null){
								int moneycount = 0;
								count = moneycount+r.getMoney();
							}else{
								double moneycount = user.getMoney();
								count = moneycount+r.getMoney();
							}
							adminMapper.updateusermoney(count,vo.getUserid());
						}
					}else{
						jsonApi = new JsonApi(10001, "该红包已抢完!");
					}
				}else{
					jsonApi = new JsonApi(10001, "你已抢过该红包!");
				}
			}else{
				return "参数不能为空";
			}

		} catch (Exception e) {
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	@Override
	public void saveChat(Message vo) {
		try {
			int result = userMapper.saveChat(vo);
			System.out.println(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String chatList() {
		try {
			List<Message> list = userMapper.chatList();
//			for(Message ms:list){
//				if(ms.getHeadImg()==null && ms.getHeadImg().equals("")){
//					ms.setHeadImg("1");
//				}
//			} 
			jsonApi = new JsonApi(10000, "查询成功!");
			jsonApi.SetObj("message",list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jsonApi = new JsonApi(10001,e.getMessage());
		}
		return jsonApi.ToJson();
	}

	
	@Override
	public String chatheadimage(String headimage) {
		try {
			List<User> list = userMapper.chatheadimage(headimage);
			jsonApi = new JsonApi(10000, "查询成功!");
			jsonApi.SetObj("message",list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jsonApi = new JsonApi(10001,e.getMessage());
		}
		return jsonApi.ToJson();
	}
	
	@Override
	public String updateImg(HttpServletRequest request) {
		JSONObject jsonObj = (JSONObject)request.getSession().getAttribute("user");
		if(jsonObj != null && jsonObj.containsKey("id")){
			String id = (String)jsonObj.get("id");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("img");
			String fileName = "";
			if (!file.isEmpty()) {
				try {
					// 原文件名
					fileName = file.getOriginalFilename();
					// 文件后缀
					//String suffix = fileName.substring(fileName.lastIndexOf("."));
					// 构建文件名称

					String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
					String s = file.getOriginalFilename();
					s = s.substring(s.lastIndexOf("."), s.length());   //字符串截取有问题
					fileName = uuid + s;

					String path = request.getSession().getServletContext().getRealPath("/");
					String path2  = request.getContextPath().replace("/","");
					path = path.substring(0,path.lastIndexOf(path2));
					String paths =  "resources/upload/img" + "/" + fileName;
					String filePath = path + paths;

					File saveDir = new File(filePath);
					if (!saveDir.getParentFile().exists())
						saveDir.getParentFile().mkdirs();
					// 转存文件
					file.transferTo(saveDir);
					int result = userMapper.updateImg(id, paths);
					if(result == 1){
						jsonApi = new JsonApi(10000, "操作成功!");
						jsonApi.SetObj("path",paths);
					}else{
						jsonApi = new JsonApi(10001, "网络异常，请稍候重试!");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				jsonApi = new JsonApi(10001, "文件不能为空!");
			}
		}else{
			jsonApi = new JsonApi(10001, "请登陆后重试!");
		}
		return jsonApi.ToJson();
	}

}
