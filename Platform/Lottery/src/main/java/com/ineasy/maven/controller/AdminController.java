package com.ineasy.maven.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.collections.set.SynchronizedSortedSet;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ineasy.maven.po.Admin;
import com.ineasy.maven.po.Notice;
import com.ineasy.maven.po.User;
import com.ineasy.maven.service.AdminService;
import com.navict.jrobot.core.annotations.ApiLog;

/**
 * 后台管理控制层
 * 
 * @author Jia Xinlei
 *
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends BaseController {

	@Resource
	private AdminService adminService;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	@ApiLog(name = "首页")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		boolean flag = BaseController.check(request);
		if (flag == true) {
			request.setAttribute("menuList", adminService.menuList());
			mav.setViewName("admin/index");
		} else {
			mav.setViewName("admin/login");
		}
		return mav;
	}

	@RequestMapping(value = "/login/view", method = RequestMethod.GET)
	@ApiLog(name = "登录页面")
	public ModelAndView loginView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("admin/login");
		return mav;
	}

	@RequestMapping(value = "/easy/view", method = RequestMethod.GET)
	@ApiLog(name = "快捷方式页面")
	public ModelAndView easy(HttpServletRequest request, HttpServletResponse response, InputStream in,
			String sFileName) {
		// sFileName = "www.baidu.com";
		ModelAndView mav = new ModelAndView();
		java.io.OutputStream toClient = null;
		try {
			// 清空response
			response.reset();
			// 设置response的Header
			response.setContentType("application/octet-stream");
			response.addHeader("Content-Disposition", "attachment;filename=" + new String(sFileName.getBytes("UTF-8")));
			// response.addHeader("Content-Length", "" + file.length());
			toClient = new BufferedOutputStream(response.getOutputStream());

			byte[] buffer = new byte[2 * 1024];
			int iReadLen;
			while (-1 != (iReadLen = in.read(buffer))) {
				toClient.write(buffer, 0, iReadLen);
			}
			toClient.flush();
		} catch (Exception e) {
		} finally {
			IOUtils.closeQuietly(toClient);
		}
		mav.setViewName("pc/easy");
		return mav;
	}

	@RequestMapping(value = "/loginout", method = RequestMethod.GET)
	@ApiLog(name = "登出")
	public ModelAndView loginOut(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		request.getSession().removeAttribute("admin");
		mav.setViewName("admin/login");
		return mav;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ApiLog(name = "登录")
	@ResponseBody
	public String login(HttpServletRequest request, HttpServletResponse response, @Valid User vo) {
		String result = adminService.login(request, vo);
		return result;
	}

	@RequestMapping(value = "/user/view", method = RequestMethod.GET)
	@ApiLog(name = "用户列表页面")
	@ResponseBody
	public ModelAndView userView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		boolean flag = BaseController.check(request);
		if (flag == true) {
			mav.setViewName("admin/user/list");
		} else {
			mav.setViewName("admin/login");
		}
		return mav;
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	@ApiLog(name = "用户列表")
	@ResponseBody
	public String userList(HttpServletRequest request, HttpServletResponse response, @Valid User vo) {
		String result = adminService.userList(vo);
		return result;
	}

	@RequestMapping(value = "/update/pass", method = RequestMethod.POST)
	@ApiLog(name = "修改密码")
	@ResponseBody
	public String updatePass(HttpServletRequest request, HttpServletResponse response, @Valid User vo) {
		String result = adminService.updatePass(vo);
		return result;
	}

	@RequestMapping(value = "/edit/user", method = RequestMethod.POST)
	@ApiLog(name = "编辑用户")
	@ResponseBody
	public String editUser(HttpServletRequest request, HttpServletResponse response, @Valid User vo) {
		String result = adminService.editUser(request,vo);
		return result;
	}

	@RequestMapping(value = "/plan/view", method = RequestMethod.GET)
	@ApiLog(name = "计划页面")
	public ModelAndView plaView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		boolean flag = BaseController.check(request);
		if (flag == true) {
			mav.setViewName("admin/plan/list");
		} else {
			mav.setViewName("admin/login");
		}
		return mav;
	}

	@RequestMapping(value = "/plan/list", method = RequestMethod.POST)
	@ApiLog(name = "计划信息")
	@ResponseBody
	public String planList(HttpServletRequest request, HttpServletResponse response) {
		String pageSize = request.getParameter("pageSize");
		String pageNumber = request.getParameter("pageNumber");
		String result = adminService.planList(pageSize, pageNumber);
		return result;
	}

	@RequestMapping(value = "/admin/view", method = RequestMethod.GET)
	@ApiLog(name = "管理员页面")
	public ModelAndView adminView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		boolean flag = BaseController.check(request);
		if (flag == true) {
			mav.setViewName("admin/adminUser/list");
		} else {
			mav.setViewName("admin/login");
		}
		return mav;
	}

	@RequestMapping(value = "/admin/list", method = RequestMethod.POST)
	@ApiLog(name = "管理员列表")
	@ResponseBody
	public String adminList(HttpServletRequest request, HttpServletResponse response) {
		String pageSize = request.getParameter("pageSize");
		String pageNumber = request.getParameter("pageNumber");
		String result = adminService.adminList(pageSize, pageNumber);
		return result;
	}
	
	@RequestMapping(value = "/edit/admin", method = RequestMethod.POST)
	@ApiLog(name = "编辑管理员")
	@ResponseBody
	public String editAdmin(HttpServletRequest request, HttpServletResponse response,@Valid Admin vo) {
		String result = adminService.editAdmin(vo);
		return result;
	}
	
	@RequestMapping(value = "/notice/view", method = RequestMethod.GET)
	@ApiLog(name = "公告页面")
	public ModelAndView noticeView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		boolean flag = BaseController.check(request);
		if (flag == true) {
			mav.setViewName("admin/gonggao/list");
		} else {
			mav.setViewName("admin/login");
		}
		return mav;
	}

	@RequestMapping(value = "/notice/list", method = RequestMethod.POST)
	@ApiLog(name = "公告列表数据")
	@ResponseBody
	public String noticeList(HttpServletRequest request, HttpServletResponse response) {
		String pageSize = request.getParameter("pageSize");
		String pageNumber = request.getParameter("pageNumber");
		String result = adminService.noticeList(pageSize, pageNumber);
		return result;
	}
	
	@RequestMapping(value = "/edit/votice", method = RequestMethod.POST)
	@ApiLog(name = "编辑公告")
	@ResponseBody
	public String editNotice(HttpServletRequest request, HttpServletResponse response,@Valid Notice vo) {
		String result = adminService.editNotice(vo);
		return result;
	}
	   public static void main(String[] args) {
		   System.out.println("请输入一个字符串");
		   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		   System.out.println(br);
		   
	   }
	}// MyLinkedList end～
	
	
