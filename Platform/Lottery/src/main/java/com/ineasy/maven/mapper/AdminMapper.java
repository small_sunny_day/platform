package com.ineasy.maven.mapper;

import java.util.List;
import java.util.Map;

import com.ineasy.maven.po.Admin;
import com.ineasy.maven.po.Menu;
import com.ineasy.maven.po.Notice;
import com.ineasy.maven.po.Plan;
import com.ineasy.maven.po.User;

public interface AdminMapper {
	
	List<Menu> menuList();
	
	User findUserByUserID(String userid);
	
	int updateusermoney(Double moneycount,String userid);
	
	User findUserByUserName(String username);
	
	List<User> userList(Map<String,Object> map);
	
	List<Plan> planList(Map<String,Object> map);
	
	List<Admin> adminList(Map<String,Object> map);
	
	int updatePass(User vo);
	
	String selectPlan();
	
	int savePlan(String id,String title,String number,String text);
	
	int deleteUser(String mobile);
	
	int editUser(String userType,String mobile);
	
	int resetPass(String mobile);
	
	int resetMoney(String mobile);
	
	int saveAdmin(Admin vo);
	
	int updateUsername(String username,String mobile);
	
	int resetAdmin(String username);
	
	int updateAdmin(String username,String id);
	
	int deleteAdmin(String username);
	
	List<Notice> noticeList(Map<String,Object> map);
	
	int insertNotice(Notice vo);
	
	int updateNotice(Notice vo);
	
	int deleteNotice(String id);

}
