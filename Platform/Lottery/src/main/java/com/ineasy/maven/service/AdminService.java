package com.ineasy.maven.service;

import javax.servlet.http.HttpServletRequest;

import com.ineasy.maven.po.Admin;
import com.ineasy.maven.po.Notice;
import com.ineasy.maven.po.User;

public interface AdminService {
	
	String menuList();
	
	String login(HttpServletRequest request,User vo);
	
	String userList(User vo);
	
	String updatePass(User vo);
	
	String editUser(HttpServletRequest request,User vo);
	
	String planList(String pageSize,String pageNumber);
	
	String adminList(String pageSize,String pageNumber);
	
	String noticeList(String pageSize,String pageNumber);
	
	String editAdmin(Admin vo);
	
	String editNotice(Notice vo);

}
