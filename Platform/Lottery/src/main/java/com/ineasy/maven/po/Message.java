package com.ineasy.maven.po;

public class Message {
	
	private int id ;
	
	private String content;
	
	private String from;
	
	private String time;
	
	private String username;
	
	private String type;
	
	private String fsrname;
	
	private String redbagid;
	
	private String headImg;
	

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFsrname() {
		return fsrname;
	}

	public void setFsrname(String fsrname) {
		this.fsrname = fsrname;
	}

	public String getRedbagid() {
		return redbagid;
	}

	public void setRedbagid(String redbagid) {
		this.redbagid = redbagid;
	}

}
