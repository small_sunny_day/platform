package com.ineasy.maven.core;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

/**
 *  JSON操作提示类
 * @author Jia Xinlei
 * @version 1.0.0
 */
public class JsonApi implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * @param flag 操作标识，大于零表示操作成功，否则表示操作失败
	 */
	public JsonApi(int flag) {
		this.map.put("flag", flag);
	}

	/**
	 * @param flag 操作标识
	 * @param msg 提示语或描述信息
	 */
	public JsonApi(int flag, String msg) {
		this(flag);
		this.map.put("msg", msg);
	}

	/**
	 * @param flag 操作标识
	 * @param msg 提示语或描述信息
	 * @param error 错误信息
	 */
	public JsonApi(int flag, String msg, String error) {
		this(flag, msg);
		this.map.put("error", error);
	}
	
	/**
	 * @param flag 操作标识
	 * @param msg 提示语或描述信息
	 * @param error 错误信息
	 */
	public JsonApi(int flag, String msg, Object obj) {
		this(flag, msg);
		this.map.put("obj", obj);
	}
	
	
	/**
	 * 提示对象容器
	 */
	private Map<String, Object> map = new HashMap<String, Object>();

	/**
	 * @param key 扩展键名
	 * @param obj 扩展对象
	 * @return 当前对象
	 */
	public JsonApi SetObj(String key, Object obj) {
		this.map.put(key, obj);
		return this;
	}
	
	/**
	 * 〈按键名获取容器中存在的对象〉
	 * 〈功能详细描述〉
	 * @param @param key
	 * @param @return   
	 * @return Object  
	 * @throws
	 * @author Wxb
	 * @date 2016-1-10
	 */
	public Object GetObj(String key) {
		return this.map.get(key);
	}
	
	/**
	 * @param obj
	 * @return
	 */
	public JsonApi SetFlag(Object obj) {
		this.map.put("flag", obj);
		return this;
	}
	
	/**
	 * @param obj
	 * @return
	 */
	public JsonApi SetMsg(Object obj) {
		this.map.put("msg", obj);
		return this;
	}
	
	/**
	 * @param obj
	 * @return
	 */
	public JsonApi SetError(Object obj) {
		this.map.put("error", obj);
		return this;
	}
	
	/**
	 * @return 返回当前对象的 JSON 格式
	 */
	public String ToJson() {
		return new Gson().toJson(map);
	}
	/**
	 * 无参构造方法,默认设置flag为0
	 */
	public JsonApi() {
		this.map.put("flag", 0);
	}
	public JsonApi SetObj(Object obj) {
		this.map.put("list", obj);
		return this;
	}
	
}
