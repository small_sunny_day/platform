package com.ineasy.maven.util.tool;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

/**
 * 〈Ajax操作提示类〉 〈所有的Ajax操作均要求返回该类〉
 *  可根据需要用 SetObj 方法进行扩展
 * @author [WXB]
 * @version [1.0, 2016-1-4]
 * @since [1.0]
 */
public class AjaxUtils implements Serializable {

	/**
	 * @param flag 操作标识，大于零表示操作成功，否则表示操作失败
	 */
	public AjaxUtils(int flag) {
		this.map.put("flag", flag);
	}

	/**
	 * @param flag 操作标识
	 * @param msg 提示语或描述信息
	 */
	public AjaxUtils(int flag, String msg) {
		this(flag);
		this.map.put("msg", msg);
	}

	/**
	 * @param flag 操作标识
	 * @param msg 提示语或描述信息
	 * @param error 错误信息
	 */
	public AjaxUtils(int flag, String msg, String error) {
		this(flag, msg);
		this.map.put("error", error);
	}
	public AjaxUtils(int flag, String msg, Object obj,String code,String phone) {
		this(flag, msg);
		this.map.put("userinfo", obj);
		this.map.put("code", code);
		this.map.put("phone", phone);
	}
	
	
	/**
	 * @param flag 操作标识
	 * @param msg 提示语或描述信息
	 * @param error 错误信息
	 */
	public AjaxUtils(int flag, String msg, Object obj) {
		this(flag, msg);
		this.map.put("userinfo", obj);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 提示对象容器
	 */
	private Map<String, Object> map = new HashMap<String, Object>();

	/**
	 * @param key 扩展键名
	 * @param obj 扩展对象
	 * @return 当前对象
	 */
	public AjaxUtils SetObj(String key, Object obj) {
		this.map.put(key, obj);
		return this;
	}
	
	public AjaxUtils SetObj2(String key, Object obj) {
		this.map.put(key, obj);
		return this;
	}
	
	/**
	 * 〈按键名获取容器中存在的对象〉
	 * 〈功能详细描述〉
	 * @param @param key
	 * @param @return   
	 * @return Object  
	 * @throws
	 * @author Wxb
	 * @date 2016-1-10
	 */
	public Object GetObj(String key) {
		return this.map.get(key);
	}
	
	/**
	 * @param obj
	 * @return
	 */
	public AjaxUtils SetFlag(Object obj) {
		this.map.put("flag", obj);
		return this;
	}
	
	/**
	 * @param obj
	 * @return
	 */
	public AjaxUtils SetMsg(Object obj) {
		this.map.put("msg", obj);
		return this;
	}
	
	/**
	 * @param obj
	 * @return
	 */
	public AjaxUtils SetError(Object obj) {
		this.map.put("error", obj);
		return this;
	}
	
	/**
	 * @return 返回当前对象的 JSON 格式
	 */
	public String ToJson() {
		return new Gson().toJson(map);
	}
	/**
	 * 无参构造方法,默认设置flag为0
	 */
	public AjaxUtils() {
		this.map.put("flag", 0);
	}
	
	
}
