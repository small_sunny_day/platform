package com.ineasy.maven.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.ineasy.maven.core.JsonApi;
import com.ineasy.maven.mapper.AdminMapper;
import com.ineasy.maven.po.Admin;
import com.ineasy.maven.po.Menu;
import com.ineasy.maven.po.Notice;
import com.ineasy.maven.po.Plan;
import com.ineasy.maven.po.User;
import com.ineasy.maven.service.AdminService;
import com.ineasy.maven.util.tool.PageUtils;
import com.ineasy.maven.util.tool.TimeUtils;
import com.ineasy.maven.util.tool.UUIDUtils;
import com.mysql.jdbc.StringUtils;

import net.sf.json.JSONObject;

@Service
public class AdminServiceImpl implements AdminService {

	@Resource
	private AdminMapper adminMapper;

	private JsonApi jsonApi;

	/**
	 * 菜单列表
	 */
	@Override
	public String menuList() {
		List<Menu> jsonList = new ArrayList<Menu>();
		List<Menu> list = null;
		try {
			list = adminMapper.menuList();
			for(int i=0;i<list.size();i++){
				List<Menu> child = new ArrayList<>();
				if(StringUtils.isNullOrEmpty(list.get(i).getParentId())){
					for(int j =0;j<list.size();j++){
						if(!StringUtils.isNullOrEmpty(list.get(j).getParentId())){
							if(list.get(j).getParentId().equals(list.get(i).getId())){
								child.add(list.get(j));
								list.get(i).setChildren(child);
							}
						}
					}
					jsonList.add(list.get(i));
				}
			}
			jsonApi = new JsonApi(10000, "查询成功");
			jsonApi.SetObj("list",jsonList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	/**
	 * 登录
	 */
	@Override
	public String login(HttpServletRequest request,User vo) {
		String username = vo.getUsername();
		if(!StringUtils.isNullOrEmpty(username)){
			User user = adminMapper.findUserByUserName(username.trim());
			if(user != null){
				if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
					if(vo.getPassWord().equals(user.getPassWord())){
						user.setPassWord("");
						request.getSession().setAttribute("admin", JSONObject.fromObject(user));
						jsonApi = new JsonApi(10000, "登录成功");
						jsonApi.SetObj("user", JSONObject.fromObject(user));
					}else{
						jsonApi = new JsonApi(10001, "用户名或密码错误");
					}
				}else{
					jsonApi = new JsonApi(10001, "密码不能为空");
				}
			}else{
				jsonApi = new JsonApi(10001, "用户不存在");
			}
		}else{
			jsonApi = new JsonApi(10001, "用户名不能为空");
		}
		return jsonApi.ToJson();
	}

	/**
	 * 用户列表
	 */
	@Override
	public String userList(User vo) {
		Map<String,Object> map = (Map<String,Object>)PageUtils.getPage(vo.getPageSize(), vo.getPageNumber());
		List<User> list = null;
		try {
			list = adminMapper.userList(map);
			jsonApi = new JsonApi(10000, "查询成功");
			if(list.size() > 0 ){
				int count = (list.get(0).getCount()+(int)map.get("pageSize")-1)/(int)map.get("pageSize");      //总条数
				if(!StringUtils.isNullOrEmpty(vo.getPageNumber())){
					jsonApi.SetObj("current", vo.getPageNumber());
				}else{
					jsonApi.SetObj("current",1);
				}
				jsonApi.SetObj("page", count);
			}
			jsonApi.SetObj("list",list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	/**
	 * 修改密码
	 */
	@Override
	public String updatePass(User vo) {
		String username = vo.getUsername();
		if(!StringUtils.isNullOrEmpty(username)){
			User user = adminMapper.findUserByUserName(username.trim());
			if(user != null){
				if(vo.getPassWord().equals(user.getPassWord())){
					int result = adminMapper.updatePass(vo);
					if(result == 1){
						jsonApi = new JsonApi(10000, "修改成功");
					}else{
						jsonApi = new JsonApi(10001, "网络异常，请稍候重试");
					}
				}else{
					jsonApi = new JsonApi(10001, "原密码错误");
				}
			}else{
				jsonApi = new JsonApi(10001, "网络异常，请稍候重试");
			}
		}else{
			jsonApi = new JsonApi(10001, "用户名不能为空");
		}
		return jsonApi.ToJson();
	}

	/**
	 * 编辑用户
	 */
	@Override
	public String editUser(User vo) {
		int result = 0;
		String mobile = vo.getMobile();
		if(!StringUtils.isNullOrEmpty(mobile)){
			if(!StringUtils.isNullOrEmpty(vo.getPassWord())){  //重置密码为：123456
				result = adminMapper.resetPass(mobile);
			}else if(!StringUtils.isNullOrEmpty(vo.getUserType())){  //修改用户类型
				String userType = vo.getUserType().trim();
				result = adminMapper.editUser(userType, mobile);   
			}else if(!StringUtils.isNullOrEmpty(vo.getDelete())){   //删除用户
				result = adminMapper.deleteUser(mobile);
			}
			if(result == 1){
				jsonApi = new JsonApi(10000, "操作成功");
			}else{
				jsonApi = new JsonApi(10001, "操作失败");
			}
		}else{
			jsonApi = new JsonApi(10001, "用户名不能为空");
		}

		return jsonApi.ToJson();
	}

	/**
	 * 计划信息
	 */
	@Override
	public String planList(String pageSize,String pageNumber) {
		Map<String,Object> map = (Map<String,Object>)PageUtils.getPage(pageSize,pageNumber);
		List<Plan> list = null;
		try {
			list = adminMapper.planList(map);
			jsonApi = new JsonApi(10000, "查询成功");
			if(list.size() > 0 ){
				int count = (list.get(0).getCount()+(int)map.get("pageSize")-1)/(int)map.get("pageSize");      //总条数
				if(!StringUtils.isNullOrEmpty(pageNumber)){
					jsonApi.SetObj("current", pageNumber);
				}else{
					jsonApi.SetObj("current",1);
				}
				jsonApi.SetObj("page", count);
			}
			jsonApi.SetObj("list",list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	/**
	 * 管理员信息
	 */
	@Override
	public String adminList(String pageSize, String pageNumber) {
		Map<String,Object> map = (Map<String,Object>)PageUtils.getPage(pageSize,pageNumber);
		List<Admin> list = null;
		try {
			list = adminMapper.adminList(map);
			jsonApi = new JsonApi(10000, "查询成功");
			if(list.size() > 0 ){
				int count = (list.get(0).getCount()+(int)map.get("pageSize")-1)/(int)map.get("pageSize");      //总条数
				if(!StringUtils.isNullOrEmpty(pageNumber)){
					jsonApi.SetObj("current", pageNumber);
				}else{
					jsonApi.SetObj("current",1);
				}
				jsonApi.SetObj("page", count);
			}
			jsonApi.SetObj("list",list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	/**
	 * 编辑用户
	 */
	@Override
	public String editAdmin(Admin vo) {
		int result = 0;
		if("1".equals(vo.getState())){ //新增用户
			vo.setId(UUIDUtils.getUUID());
			result = adminMapper.saveAdmin(vo);
		}else if("2".equals(vo.getState())){  //修改用户
			result = adminMapper.updateAdmin(vo.getUsername(),vo.getId());
		}else if("3".equals(vo.getState())){   //删除用户
			if(!"admin".equals(vo.getUsername())){
				result = adminMapper.deleteAdmin(vo.getUsername());
			}
		}else if("4".equals(vo.getState())){   //重置密码（123456）
			result = adminMapper.resetAdmin(vo.getUsername());
		}else{
			jsonApi = new JsonApi(10001,"参数不能为空");
		}
		if(result == 1){
			jsonApi = new JsonApi(10000, "操作成功");
		}else{
			jsonApi = new JsonApi(10001, "操作失败");
		}
		return jsonApi.ToJson();
	}

	/**
	 * 广告列表
	 */
	@Override
	public String noticeList(String pageSize, String pageNumber) {
		Map<String,Object> map = (Map<String,Object>)PageUtils.getPage(pageSize,pageNumber);
		List<Notice> list = null;
		try {
			list = adminMapper.noticeList(map);
			jsonApi = new JsonApi(10000, "查询成功");
			if(list.size() > 0 ){
				int count = (list.get(0).getCount()+(int)map.get("pageSize")-1)/(int)map.get("pageSize");      //总条数
				if(!StringUtils.isNullOrEmpty(pageNumber)){
					jsonApi.SetObj("current", pageNumber);
				}else{
					jsonApi.SetObj("current",1);
				}
				jsonApi.SetObj("page", count);
			}
			jsonApi.SetObj("list",list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	/**
	 * 编辑广告
	 */
	@Override
	public String editNotice(Notice vo) {
		String type = vo.getType();
		int result = 0;
		if(!StringUtils.isNullOrEmpty(type)){
		 if("1".equals(type)){   		//新增
			 vo.setId(UUIDUtils.getUUID());
			 result = adminMapper.insertNotice(vo);
		 }else if ("2".equals(type)){   //修改
			 vo.setSaveTime(TimeUtils.getDate());
			 result = adminMapper.updateNotice(vo);
		 }else if("3".equals(type)){    //删除
			 result = adminMapper.deleteNotice(vo.getId());
		 }else{
			 jsonApi = new JsonApi(10001,"网络异常，请稍候重试");
		 }
		 if(result == 1){
			 jsonApi = new JsonApi(10000,"操作成功");
		 }else{
			 jsonApi = new JsonApi(10001,"操作失败");
		 }
		}else{
			jsonApi = new JsonApi(10001,"参数格式不正确");
		}
		return jsonApi.ToJson();
	}

}
