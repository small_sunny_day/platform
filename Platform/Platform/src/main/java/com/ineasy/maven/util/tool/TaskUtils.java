package com.ineasy.maven.util.tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ineasy.maven.mapper.AdminMapper;

/**
 * 
 *@Title: TimerUtil
 *@Desc: 定时器util类
 *@Author: Jia Xilei
 *@Version: 1.0.0
 *@Date: 2016年11月11日 下午3:53:32
 */
@Component
public class TaskUtils {
	
	@Resource
	private AdminMapper adminMapper;
	
	//private final Logger log = LoggerFactory.getLogger(TaskUtils.class);  
	int i = 0;

	//常用定时格式数据如下
	//	0 0 12 * * ?  每天中午12点触发 
	//	0 15 10 ? * *  每天上午10:15触发 
	//	0 15 10 * * ?  每天上午10:15触发 
	//	0 15 10 * * ? *  每天上午10:15触发 
	//	0 15 10 * * ? 2005  2005年的每天上午10:15触发 
	//	0 * 14 * * ?  在每天下午2点到下午2:59期间的每1分钟触发 
	//	0 0/5 14 * * ?  在每天下午2点到下午2:55期间的每5分钟触发 
	//	0 0/5 14,18 * * ?  在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发 
	//	0 0-5 14 * * ?  在每天下午2点到下午2:05期间的每1分钟触发 
	//	0 10,44 14 ? 3 WED  每年三月的星期三的下午2:10和2:44触发 
	//	0 15 10 ? * MON-FRI  周一至周五的上午10:15触发 
	//	0 15 10 15 * ?  每月15日上午10:15触发 
	//	0 15 10 L * ?  每月最后一日的上午10:15触发 
	//	0 15 10 ? * 6L  每月的最后一个星期五上午10:15触发 
	//	0 15 10 ? * 6L 2002-2005  2002年至2005年的每月的最后一个星期五上午10:15触发 
	//	0 15 10 ? * 6#3  每月的第三个星期五上午10:15触发 
	//	0 6 * * *       每天早上6点 
	//	0 */2 * * *    	每两个小时 
	//	0 23-7/2，8 * * *  晚上11点到早上8点之间每两个小时，早上八点 
	//	0 11 4 * 1-3       每个月的4号和每个礼拜的礼拜一到礼拜三的早上11点 
	//	0 4 1 1 *     1月1日早上4点 
	//  0/10 * * * * ?  每隔十秒执行一次

	/**
	 * 
	 *@Title: testTimer
	 *@Desc: 测试方法
	 *@Author: Jia Xilei
	 *@ReturnType void
	 *@Version: 1.0.0
	 *@Date: 2016年11月14日 下午2:47:01
	 */
	//@Scheduled(cron= "0/10 * * * * ?") 
	@SuppressWarnings({ "static-access", "resource", "unused" })
	public void testTimer() {
		try {
			System.out.println("定时任务开启了！！");
			new Thread().sleep(60 * 1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		StringBuffer sbf = new StringBuffer();
		int result = 0;
		try {
			String encoding="GBK";
			File file=new File("C:\\1.txt");
			if(file.isFile() && file.exists()){ //判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file),encoding);//考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				List<String> list = new ArrayList<>();
				String lineTxt = null;
				while((lineTxt = bufferedReader.readLine()) != null){
					sbf.append(lineTxt+"</br>");
					list.add(lineTxt+"");
				}
				String titile= "";
				if(list.size() > 0){
					titile = list.get(list.size() -1);
					titile = titile.substring(0, titile.lastIndexOf("开奖号码") -1);
				}
				String res = adminMapper.selectPlan();
				if(res == null || "".equals(res)){
					//result =  adminMapper.savePlan(UUIDUtils.getUUID(),titile, sbf.toString());
				}else{
					if(!res.equals(sbf.toString())){
						//result =  adminMapper.savePlan(UUIDUtils.getUUID(),titile, sbf.toString());
					}
				}
			}else{
				System.out.println("找不到指定文件");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
