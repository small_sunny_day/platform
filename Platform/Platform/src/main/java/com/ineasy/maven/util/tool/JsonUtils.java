package com.ineasy.maven.util.tool;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @Title:JsonUtils
 * @Description:json解析成字符串
 * @author : Jia xinlei
 * @version: 1.0
 * @date 2016年10月20日 上午10:18:29
 */
public class JsonUtils {

	/**
	 * 
	 * @Title: parseMsgData
	 * @Description:实名认证接口返回json数据
	 * @return
	 * @throws JSONException
	 * @author : Jia xinlei
	 * @return Map<String,Object>
	 * @date 2016年10月20日 上午10:21:53
	 */
	public static Map<String,Object> parseMsgData(String json) throws JSONException {
		JSONObject jsonObject = null;
		jsonObject = new JSONObject(json);
		System.out.println("json===="+json);
		Map<String,Object> map = new HashMap<String,Object>();
		if(json.contains("result")){
			if(jsonObject.get("result") != null){
				JSONObject resultObj = jsonObject.getJSONObject("result");
				map.put("res", resultObj.get("res"));
				map.put("error_code", jsonObject.get("error_code"));
			}
		}
		return map;

	}

	/**
	 * 
	 * @Title: parseCardInfoJson
	 * @Description:解析银行卡详细信息返回json数据
	 * @param json
	 * @return
	 * @throws JSONException
	 * @author : Jia xinlei
	 * @return Map<String,Object>
	 * @date 2016年10月21日 上午11:32:02
	 */
	public static Map<String,Object> parseCardInfoJson(String json) throws JSONException {
		JSONObject jsonObject = null;
		jsonObject = new JSONObject(json);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("error_code", jsonObject.get("error_code"));
		map.put("reason", jsonObject.get("reason"));
		System.out.println("json===="+json);
		// 得到指定json key对象的value对象
		if(json.contains("result")){
			if(jsonObject.get("result") != null){
				JSONObject resultObj = jsonObject.getJSONObject("result");
				map.put("logo", "http://images.juheapi.com/banklogo/"+resultObj.getString("logo"));
				map.put("bank", resultObj.getString("bank"));
				map.put("type", resultObj.getString("type"));
			}
		}
		return map;

	}

	/**
	 * 
	 * @Title: parseBankCardJson
	 * @Description:解析绑定银行卡返回json数据
	 * @param json
	 * @return
	 * @throws JSONException
	 * @author : Jia xinlei
	 * @return Map<String,Object>
	 * @date 2016年10月21日 下午2:33:21
	 */
	public static Map<String,Object> parseBankCardJson(String json) throws JSONException {
		JSONObject jsonObject = null;
		jsonObject = new JSONObject(json);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("error_code", jsonObject.get("error_code"));
		map.put("reason", jsonObject.get("reason"));
		System.out.println("json===="+json);
		// 得到指定json key对象的value对象
		if(json.contains("result")){
			if(jsonObject.get("result") != null){
				JSONObject resultObj = jsonObject.getJSONObject("result");
				map.put("res", resultObj.getString("res"));
				map.put("message", resultObj.getString("message"));
			}
		}
		return map;

	}
	
}
