package com.ineasy.maven.util.tool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {

	public static String getDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}

	public static String getTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
		return sdf.format(new Date());
	}

	public static String getMonth(){
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		return sdf.format(new Date());
	}

	/**
	 * 获取时间
	 * @return
	 */
	public static String getEndTime() {
		Date date = new Date();
		int[] weekDays = {7, 1, 2, 3, 4, 5, 6};
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd:12:00:00");
		cal.setTime(date);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		int num = weekDays[w];
		int nums = 0;
		if(num < 5 && num >= 1){
			nums = 5 - num;
		}else if (num == 6){
			nums = 6;
		}else if (num == 5){
			nums = 7;
		}else{
			nums = 5;
		}
		Calendar ca = Calendar.getInstance();
		ca.add(Calendar.DATE, nums);
		date = ca.getTime();
		String enddate = format.format(date);
		return enddate;
	}

	/**
	 * 获得之后的日期
	 * @param d
	 * @param day
	 * @return
	 */
	public static String getDateAfter(Date d, int day) {  
		Calendar now = Calendar.getInstance();  
		now.setTime(d);  
		now.set(Calendar.DATE, now.get(Calendar.DATE) + day);  
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
		return sdf.format(now.getTime());  
	}

	/**
	 * 获得之前的日期
	 * @param d
	 * @param day
	 * @return
	 */
	public static Date getDateBefore(Date d, int day) {  
		Calendar now = Calendar.getInstance();  
		now.setTime(d);  
		now.set(Calendar.DATE, now.get(Calendar.DATE) - day);  
		return now.getTime();  
	}  

	public static void main(String[] args) {
		String aa = TimeUtils.getDateAfter(new Date(),7);
		System.out.println(aa);
	}

	/**
	 * 比较日期
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public static boolean compareDate(String startTime,String endTime){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = sdf.parse(startTime);
			date2 = sdf.parse(endTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(date2.getTime() > date1.getTime()){
			return true;
		}else{
			return false;
		}
	}

}
