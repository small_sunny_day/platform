package com.ineasy.maven.mapper;

import java.util.List;

import com.ineasy.maven.po.Plan;
import com.ineasy.maven.po.Sign;
import com.ineasy.maven.po.Token;
import com.ineasy.maven.po.User;

public interface UserMapper {

	//注册
	int register(User user); 

	//查询用户
	User findUserByMobile(String mobile);

	//查询当日是否已签到
	String findSignByUserId(Sign vo);

	//签到
	int sign(Sign vo);

	//签到列表
	List<String> signNote(Sign vo);

	//签到列表
	List<Sign> signList(Sign vo);

	//保存token                                                                
	int saveToken(Token vo);

	//查询token是否已存在
	Token selectToken(String userId);

	//好友列表
	List<User> friendListById(String userId);
	
	//好友列表
	List<User> friendList();
	
	Plan findPlan();
	
	List<String> notice();

}
