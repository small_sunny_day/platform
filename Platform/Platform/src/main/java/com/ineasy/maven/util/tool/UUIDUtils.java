package com.ineasy.maven.util.tool;

import java.security.SecureRandom;
import java.util.UUID;

/**
 * 
 *@Title: UUIDUtils
 *@Desc: UUID工具类
 *@Author: Jia Xilei
 *@Version: 1.0.0
 *@Date: 2016年11月15日 上午9:38:07
 */
public class UUIDUtils {

	private static SecureRandom random = new SecureRandom();

	/**
	 * 封装JDK自带的UUID, 通过Random数字生成, 中间有-分割.
	 */
	public static String uuid() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 封装JDK自带的UUID, 通过Random数字生成, 中间无-分割.
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 使用SecureRandom随机生成Long.
	 */
	public static long randomLong() {
		return Math.abs(random.nextLong());
	}

}

