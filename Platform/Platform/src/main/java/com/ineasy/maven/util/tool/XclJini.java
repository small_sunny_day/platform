package com.ineasy.maven.util.tool;

public class XclJini {

	//声明为本地方法，生成为C/C++使用的.h 头文件中的函数声明。
	public native int GetVersion();     
	public native int GetStatus();
	public native String GetMsg();
	public native int SendMsg(String msg);

	static {
		//jvm变量
		//System.out.println(System.getProperty("java.library.path"));
		//C:\java\jdk\bin
		System.loadLibrary("XclJiniLib");           
		//System.load("C:\\java\\jdk\\bin\\XclJiniLib.dll");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args){
		System.out.println("__________________________"); 
		System.out.println("Java: jini 演示！");   

		XclJini _XclJini = new XclJini();       
		_XclJini.GetVersion();
		_XclJini.GetStatus();
		_XclJini.SendMsg("发个信息给C++.");
		String msg = _XclJini.GetMsg();
		System.out.println("java:"+msg);        
		System.out.println("__________________________"); 
	}
	
}