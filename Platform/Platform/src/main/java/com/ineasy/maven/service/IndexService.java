package com.ineasy.maven.service;

import javax.servlet.http.HttpServletRequest;

import com.ineasy.maven.po.Sign;
import com.ineasy.maven.po.User;

import net.sf.json.JSONObject;

public interface IndexService {
	
	//登录
	String login(HttpServletRequest request,User vo);
	
	//注册
	String register(User vo);
	
	//签到
	String sign(Sign vo);
	
	//签到记录
	String signNote(Sign vo);
	
	//签到列表
	String signList(Sign vo);
	
	//好友列表
	String friendList(JSONObject jsonObj);
	
	//计划
	String plan(HttpServletRequest request);
	
	String findPlan();
	
	String notice();
	
}
