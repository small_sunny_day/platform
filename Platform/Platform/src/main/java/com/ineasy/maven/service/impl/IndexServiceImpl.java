package com.ineasy.maven.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ineasy.maven.core.JsonApi;
import com.ineasy.maven.mapper.AdminMapper;
import com.ineasy.maven.mapper.UserMapper;
import com.ineasy.maven.po.Plan;
import com.ineasy.maven.po.Sign;
import com.ineasy.maven.po.User;
import com.ineasy.maven.service.IndexService;
import com.ineasy.maven.util.tool.TimeUtils;
import com.ineasy.maven.util.tool.UUIDUtils;
import com.mysql.jdbc.StringUtils;

import net.sf.json.JSONObject;

@Service
public class IndexServiceImpl implements IndexService{

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private AdminMapper adminMapper;

	private JsonApi jsonApi;

	//登录
	public String login(HttpServletRequest request,User vo) {
		String mobile = vo.getMobile();
		JSONObject json = (JSONObject)request.getSession().getAttribute("user");
		if(json != null){
			String usern = json.getString("mobile");
			if(usern.equals(vo.getMobile())){
				jsonApi = new JsonApi(10001, "用户已在线,不可重复登录");
			}else{
				if(!StringUtils.isNullOrEmpty(mobile)){
					User user = userMapper.findUserByMobile(mobile.trim());
					if(user != null){
						if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
							if(vo.getPassWord().equals(user.getPassWord())){
								user.setPassWord("");
								request.getSession().setAttribute("user", JSONObject.fromObject(user));
								jsonApi = new JsonApi(10000, "登录成功");
								jsonApi.SetObj("user", JSONObject.fromObject(user));
							}else{
								jsonApi = new JsonApi(10001, "用户名或密码错误");
							}
						}else{
							jsonApi = new JsonApi(10001, "密码不能为空");
						}

					}else{
						jsonApi = new JsonApi(10001, "用户不存在");
					}
				}else{
					jsonApi = new JsonApi(10001, "用户名不能为空");
				}
			}
		}else{
			if(!StringUtils.isNullOrEmpty(mobile)){
				User user = userMapper.findUserByMobile(mobile.trim());
				if(user != null){
					if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
						if(vo.getPassWord().equals(user.getPassWord())){
							user.setPassWord("");
							request.getSession().setAttribute("user", JSONObject.fromObject(user));
							jsonApi = new JsonApi(10000, "登录成功");
							jsonApi.SetObj("user", JSONObject.fromObject(user));
						}else{
							jsonApi = new JsonApi(10001, "用户名或密码错误");
						}
					}else{
						jsonApi = new JsonApi(10001, "密码不能为空");
					}

				}else{
					jsonApi = new JsonApi(10001, "用户不存在");
				}
			}else{
				jsonApi = new JsonApi(10001, "用户名不能为空");
			}
		}
		return jsonApi.ToJson();
	}

	//注册
	public String register(User vo) {
		String mobile = vo.getMobile();
		if(!StringUtils.isNullOrEmpty(mobile)){
			User user = userMapper.findUserByMobile(mobile);
			if(user == null){
				if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
					int res = userMapper.register(vo);
					if(res == 1){
						jsonApi = new JsonApi(10000, "注册成功");
					}else{
						jsonApi = new JsonApi(10001, "网络异常");
					}
				}else{
					jsonApi = new JsonApi(10001, "密码不能为空");
				}
			}else{
				jsonApi = new JsonApi(10001, "用户已存在");
			}
		}else{
			jsonApi = new JsonApi(10001, "用户名不能为空");
		}
		return jsonApi.ToJson();
	}

	//签到
	@Override
	public String sign(Sign vo) {
		String userId = vo.getUserId();
		if(!StringUtils.isNullOrEmpty(userId)){
			vo.setSignDate(TimeUtils.getDate());
			String result = userMapper.findSignByUserId(vo);
			if(result == null){
				vo.setId(UUIDUtils.getUUID());
				vo.setBranch(10);
				vo.setRemarks("签到奖励10积分");
				int res = userMapper.sign(vo);
				if(res == 1){
					jsonApi = new JsonApi(10000, "签到成功");
				}else{
					jsonApi = new JsonApi(10001, "网络异常");
				}
			}else{
				jsonApi = new JsonApi(10001, "今天已签到");
			}
		}else{
			jsonApi = new JsonApi(10001, "参数不能为空");
		}
		return jsonApi.ToJson();
	}

	//签到记录
	@Override
	public String signNote(Sign vo) {
		String userId = vo.getUserId();
		if(!StringUtils.isNullOrEmpty(userId)){
			vo.setSignDate(TimeUtils.getMonth());
			List<String> list = userMapper.signNote(vo);
			jsonApi = new JsonApi(10000, "查询成功");
			jsonApi.SetObj("list",list);
		}else{
			jsonApi = new JsonApi(10001, "参数不能为空");
		}
		return jsonApi.ToJson();
	}

	//签到列表
	@Override
	public String signList(Sign vo) {
		String userId = vo.getUserId();
		if(!StringUtils.isNullOrEmpty(userId)){
			vo.setSignDate(TimeUtils.getMonth());
			List<Sign> list = userMapper.signList(vo);
			jsonApi = new JsonApi(10000, "查询成功");
			jsonApi.SetObj("list",list);
		}else{
			jsonApi = new JsonApi(10001, "参数不能为空");
		}
		return jsonApi.ToJson();
	}

	/**
	 * 好友列表
	 */
	@Override
	public String friendList(JSONObject jsonObj) {
		try {
			List<User> list = null;
			if (jsonObj != null && !"".equals(jsonObj)) {
				if (jsonObj.containsKey("id")) {
					String userId = "";
					userId = jsonObj.getString("id");
					list = userMapper.friendListById(userId);
				}else{
					list = userMapper.friendList();
				}

			}else{
				list = userMapper.friendList();
			}
			jsonApi = new JsonApi(10000, "查询成功");
			jsonApi.SetObj("list",list);
		} catch (Exception e) {
			// TODO: handle exception
			jsonApi = new JsonApi(10001,e.getMessage());
		}
		return jsonApi.ToJson();
	}

	/**
	 * 
	 */
	@SuppressWarnings("static-access")
	@Override
	public String plan(HttpServletRequest request) {
		try {
			new Thread().sleep(60 * 1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String url = request.getSession().getServletContext().getRealPath("");
		url = url.substring(0,url.lastIndexOf("webapps"))+"/fileupload";
		File f = new File(url);
		if(!f.exists()){
			f.mkdirs();
		}
		String filePath = url+ "/1.txt";
		StringBuffer sbf = new StringBuffer();
		int result = 0;
		String titile = null,number = "";
		try {
			String encoding="GBK";
			File file=new File(filePath);
			if(file.isFile() && file.exists()){ //判断文件是否存在
				InputStreamReader read = new InputStreamReader(
						new FileInputStream(file),encoding);//考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				List<String> list = new ArrayList<>();
				String lineTxt = null;
				while((lineTxt = bufferedReader.readLine()) != null){
					sbf.append(lineTxt+"</br>");
					list.add(lineTxt+"");
				}
				if(list.size() > 0){
					titile = list.get(list.size() -1);
					number = titile.substring(titile.lastIndexOf("开奖号码") + 5,titile.length()-1);
					titile = titile.substring(0, titile.lastIndexOf("开奖号码") -1);
				}

				read.close();
				String res = adminMapper.selectPlan();
				if(res == null || "".equals(res)){
					result =  adminMapper.savePlan(UUIDUtils.getUUID(),titile,number, sbf.toString());
				}else{
					if(!res.equals(sbf.toString())){
						result =  adminMapper.savePlan(UUIDUtils.getUUID(),titile,number, sbf.toString());
					}
				}
			}else{
				jsonApi = new JsonApi(10001, "找不到指定文件");
			}
			if(result != 0){
				jsonApi = new JsonApi(10000, "成功");
				jsonApi.SetObj("obj",sbf.toString());
				jsonApi.SetObj("number",number);
				//jsonApi.SetObj("titile",titile.substring(titile.lastIndexOf("第"),titile.length()));
				jsonApi.SetObj("titile",titile);
			}else{
				jsonApi = new JsonApi(10001, "数据未发生改变");
			}
		} catch (Exception e) {
			jsonApi = new JsonApi(10001,e.getMessage());
		}
		return jsonApi.ToJson();
	}

	@Override
	public String findPlan() {
		Plan plan = null;
		try {
			plan = userMapper.findPlan();
			jsonApi = new JsonApi(10000, "成功");
			jsonApi.SetObj("plan",plan);
		} catch (Exception e) {
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

	//公告列表
	@Override
	public String notice() {
		List<String> list = null;
		try {
			list = userMapper.notice();
			jsonApi = new JsonApi(10000, "成功");
			jsonApi.SetObj("list",list);
		} catch (Exception e) {
			jsonApi = new JsonApi(10001, e.getMessage());
		}
		return jsonApi.ToJson();
	}

}
