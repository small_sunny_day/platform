package com.ineasy.maven.util.tool;


import java.util.HashMap;
import java.util.Map;

/**
 * 密码进行盐值加密
 * Created by Jia Xinlei on 2017/1/5.
 */
public class PassWordUtils {

    // 加密次数
    public static final int INTERATIONS = 1;

    // 盐size
    private static final int SALT_SIZE = 8;
    /**
     * 密码盐值加密
     * @param pass
     * @return
     */
    public static Map<String,String> encodePass(String pass){
        Map<String,String> map = new HashMap<String,String>();
        byte[] salt = Digests.generateSalt(SALT_SIZE);
       // byte[] salt = Encodes.decodeHex(resp.getSalt());
        byte[] hashPassword = Digests.sha1(pass.getBytes(), salt, INTERATIONS);
        map.put("pass",Encodes.encodeHex(hashPassword));
        map.put("salt",Encodes.encodeHex(salt));
        return map;
    }

    /**
     * 密码校验
     * @param salt
     * @param newPass
     * @param oldPass
     * @return
     */
    public static boolean checkPass(String salt,String newPass,String oldPass){
        byte[] sal = Encodes.decodeHex(salt);
        byte[] hashPassword = Digests.sha1(newPass.getBytes(), sal, INTERATIONS);
        if(Encodes.encodeHex(hashPassword).equals(oldPass)){
            return true;
        }else{
            return false;
        }
    }
}
