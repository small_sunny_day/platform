
function signFun(){
        var $dateBox = $("#js-qiandao-list"), //获取ul列表
            $currentDate = $(".current-date"),  //用于显示当前时间
            $qiandaoBnt = $("#js-just-qiandao"),  //获取右上角按钮
            _html = '',
            _handle = true,   // var dateArray = [] ;
            myDate = new Date();  //获取当前时间
        $currentDate.text(myDate.getFullYear() + '年' + parseInt(myDate.getMonth() + 1) + '月' + myDate.getDate() + '日');
// 因为getMonth是从0开始计算的，myDate.getMonth()输出的结果是当前月份-1，所以实际使用时需要+1

        var monthFirst = new Date(myDate.getFullYear(), parseInt(myDate.getMonth()), 1).getDay();

        var d = new Date(myDate.getFullYear(), parseInt(myDate.getMonth() + 1), 0);
        var totalDay = d.getDate(); //获取当前月的天数

        for (var i = 0; i < 42; i++) {
            _html += ' <li><div class="qiandao-icon"></div></li>'
            //用于动态创建li列表，并且在每个li中都要生成一个div用于显示签到的那个状态(就是那个绿色的圈圈)
            // 生成的li和div大小都是一样的，并且div的层级要比li高，但是初始状态是display:none;在签到之后显示block，这样这个绿色圈圈就会覆盖在li上面就是我们看到的情况。

        }
        $dateBox.html(_html) //生成日历网格	     
        var now = new Date();  
        var day = now.getDate();            //日  
		var dateArray = [] ;	
	       // 假设已经签到的   // 自定义一个数组，用于显示在列表上事先已经签到的日期
        $.ajax({
			"dataType" : 'json',
			"type" : "POST",
			"url" : basePath+'sign/note.htm',
			"cache" : false,
			"data":{'userId':user.id},
			"success" : function(data) {
				console.log(data)			    		
				if(data.flag == 10000){
					var ndata = data.list;
					 var ableqd = $(".date" + myDate.getDate()).hasClass('qiandao');
					 
					//生成当月的日历且含已签到
					 var $dateLi = $dateBox.find("li");
					 var ableqd = $(".date" + myDate.getDate()).hasClass('qiandao');
				        for (var i = 0; i < totalDay; i++) {
				            $dateLi.eq(i + monthFirst).addClass("date" + parseInt(i + 1));
				            for (var j = 0; j < ndata.length; j++) {				            
				                if (i == parseInt(ndata[j])) {
				                    $dateLi.eq(i + monthFirst-1).addClass("qiandao");
				                    if(day == ndata[j]){
				                    	 $qiandaoBnt.html('已签到');
				                    }
				                   
				                }
				            }
				        } 
				        dateArray.push(ndata)
				         if (!ableqd) {
				            $(".date" + myDate.getDate()).addClass('able-qiandao'); // 给当天添加可以签到的样式				           
				        }
					return;
				}else{
					layer.msg(data.msg);
				}
				
			}	
			
		})
 
        $(".date" + myDate.getDate()).addClass('able-qiandao');   //给当天添加可以签到的样式

        $dateBox.on("click", "li", function() {
                if ($(this).hasClass('able-qiandao') && _handle) {
                    $(this).addClass('qiandao');
                    qiandaoFun();
                }
            }) //签到

        $qiandaoBnt.on("click", function() {
            if (_handle) {
                qiandaoFun();
            }
        }); //签到
        function qiandaoFun() {  
        	$qiandaoBnt.html('已签到');
//            $qiandaoBnt.addClass('actived'); //立即签到改为已签到
            qianDao();
            _handle = false;  //设置当天不能签到

        } //签到函数，签到完成之后按钮显示已经签到，并且调用弹窗函数

        function qianDao() {
        	 $.ajax({
    				"dataType" : 'json',
    				"type" : "POST",
    				"url" : basePath+'sign.htm',
    				"cache" : false,
    				"data":{'userId':user.id},
    				"success" : function(data) {
    					console.log(data)
    					if(data.flag == 10000 ){
    						layer.msg(data.msg);
    						$(".date" + myDate.getDate()).addClass('qiandao');	
    					}else{
    						
    					}
    										
    					
    				}
    			})
            
        }  //在列表上显示已经签到的状态
     
        function openLayer(a, Fun) {
            $('.' + a).fadeIn(Fun)
        } //打开弹窗

        var closeLayer = function() {
            $("body").on("click", ".close-qiandao-layer", function() {
                $(this).parents(".qiandao-layer").fadeOut()
            })
        }() //关闭弹窗

            
           // 签到记录
        $("#js-qiandao-history").on("click", function() {
            openLayer("qiandao-history-layer", myFun);

            function myFun() {
            	$.ajax({
        			"dataType" : 'json',
        			"type" : "POST",
        			"url" : basePath+'sign/list.htm',
        			"cache" : false,
        			"data":{'userId':user.id},
        			"success" : function(data) {
        				console.log(data)
        				if(data.flag == 10000){
        					//成功的操作以及提示	
        					 $(".list").children().remove();
        					var result ='';
        					for( var i=0;i<data.list.length;i++){							
        						result +='<ul> <li>'+data.list[i].signDate+'</li> <li>'+data.list[i].branch+'</li><li>'+data.list[i].remarks+'</li> </ul>'					

        					}		
        					$(".list").append(result);
        				}else{
        					//失败的提示信息
        					layer.msg(data.msg); 
        				}   
        			}
        		})

                return
            } //打开弹窗返回函数   已经签到的天数，点击显示弹窗
        })
    };
    

