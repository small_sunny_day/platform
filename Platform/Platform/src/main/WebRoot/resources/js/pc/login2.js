//注册登陆一系列
	//登录
	var ghPublic={};
	var loginFn=function(loginData){
		var loginData=loginData?loginData:{};
		var mobile=loginData.mobile?loginData.mobile:'';
		var pass=loginData.pass?loginData.pass:'';
		var nextUrl=loginData.nextUrl?loginData.pass:'';
		//到后台验证手机号是否存在
		var checkMobileurl=loginData.checkMobileurl?loginData.checkMobileurl:'';//没传说明不需要认证
		var loginsuccess=loginData.loginsuccess?loginData.loginsuccess:'没有成功后的函数';
		var loginUrl=loginData.loginUrl?loginData.loginUrl:'';
		//注册系列参数
		var registerUrl=loginData.registerUrl?loginData.registerUrl:'';
		var mbyzmUrl=loginData.mbyzmUrl?loginData.mbyzmUrl:'';
		//放到公共对象上,用于注册中的登录使用
/*		ghPublic.loginUrl=loginUrl;
		ghPublic.nextUrl=nextUrl;
		ghPublic.checkMobileurl=checkMobileurl;
		ghPublic.loginsuccess=loginsuccess;*/
		var conn=ghPublic.conn;
		layer.open({
			type: 1,
			shade: false,
			title: false, //不显示标题
            area: ['370px', 'auto'], //宽高
            content: '<div class="layer_box">' +
            '<div class="login_bg login_top">登录</div>'+
			'<div class="layer_li">' +
			'<span class="fa fa-user"></span>' +
			'<input type="text" id="layer_user" placeholder="用户名" value="'+mobile+'">' +
			'</div>' +
			'<div class="layer_li">' +
			'<span class="fa fa-key"></span>' +
			'<input type="password" id="layer_pass" placeholder="密码" value="'+pass+'">' +
			'</div>' +
			'<div id="qr_login" class="qr_btn">确认登录</div>'+
			'<div class="login_bg login_bto"></div>'+
			'</div>',
//            type: 1,
//            closeBtn: 1,
//            area: ['300px', 'auto'], //宽高
//            title: '登陆',
//            content: '<div class="layer_box"><div class="layer_li"><span>用户名</span><input type="text" id="layer_user" placeholder="请输入手机号" value="'+mobile+'"></div><div class="layer_li"><span>密码</span><input type="password" id="layer_pass" value="'+pass+'"></div><div id="qr_login" class="qr_btn">确认登陆</div></div>',

            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('#layer_user').blur(function(){
                 	if(checkMobileurl==''){
                 		//console.log('没有验证手机号是否存在的接口,请确认是否需要');
                 		return;
					}
                	 if(tel_validate('layer_user')){
                		 $.ajax({
                      		url:checkMobileurl,
                      		type:'POST',
                      		data:{mobile:$(this).val().trim()},
                      		success:function(data){
                      			console.log(data);
                      			if(typeof data =='string'){
                     				var data=$.parseJSON(data);
                     			}
                      			if(data.flag==10000){
                      				if(data.msg=='0'){//后台没有
                      					//注册
                      					layer.msg('请先注册')
                      					setTimeout(function(){
                      						layer.closeAll;
                      						zhuceFn({registerUrl:ghPublic.registerUrl});
                      					},500)
                      				}
                      			}
                      		}
                      	})
                	 }
                 })
                 //登陆按钮
                 $('#qr_login').click(function(){
                	 if(loginUrl==''){
                		 console.log('loginUrl是登录接口,必须传入');
                		 return;
                	 }
                	 if(tel_validate('layer_user')&&isNumber('layer_pass')){
                		 //后台登录
                		 var loginTmp=function(token){
                			 var sendData={mobile:$('#layer_user').val().trim(),passWord:$('#layer_pass').val().trim()};
                			 /*if(typeof token !='undefined'){
                				 sendData.token=token;
                			 }*/
                			 $.ajax({
                           		url:loginUrl,
                           		type:'POST',
                           		data:sendData,
                           		success:function(data){
                           			console.log(data);
                           			if(typeof data =='string'){
                          				var data=$.parseJSON(data);
                          			}
                           			if(typeof loginsuccess=='function'){
                   						loginsuccess(data);
                   					}else{
                   						console.log('loginsuccess是登录后的操作,不传入执行默认操作');
                   						if(data.flag==10000){
                               				setTimeout(function(){
                               					layer.closeAll();
                               					if(nextUrl==''){
                               						location.reload();
                               					}else{
                               						location.href = nextUrl;
                               					}
                               					
                               				},500)
                               			}
                               			layer.msg(data.msg);
                   						return;
                   					}
                           		}
                           	})
                		 }
                	     loginTmp();
                	 }
                 })
            }
		})
	}
	var zhuceFn=function(ang){
		//手机短信验证
		var ang=ang?ang:{};
		var mbyzmUrl=ang.mbyzmUrl?ang.mbyzmUrl:'';
		var mbyzmDomStr='';
		var mbyzmData=ang.mbyzmData?ang.mbyzmData:{};//短信验证时传入后台的数据,默认手机号传入为mobile
		//前台随机验证
		var sjyzmDomStr='';
		if(mbyzmUrl!=''){
			mbyzmDomStr='<div class="layer_li"><span></span><span id="layer_getYzm" >获取验证码</span><input type="text" id="layer_setYzm" placeholder="请输入验证码"></div>';
		}else{
			sjyzmDomStr='<div class="layer_li"><span class="fa fa-pencil-square-o"></span><input type="text" id="layer_sjyzm" style="width:40%" placeholder="请输入右侧数字"><input type = "button" id="code" onClick="createCode()" style="width:35%;cursor:pointer;"></div>'
//			sjyzmDomStr='<div class="layer_li"><span></span><input type="text" id="layer_sjyzm" style="width:35%"><input type = "button" id="code" style="width:35%"></div>'

		}
		//注册接口
		var registerUrl=ang.registerUrl?ang.registerUrl:'';
		var registerSuccess=ang.registerSuccess?ang.registerSuccess:'';
		//
		var conn=ghPublic.conn;
		if(registerUrl==''){
			 console.log('请传入注册接口')
			 return;
		 }
		layer.open({

			type: 1,
			shade: false,
			title: false, //不显示标题
            area: ['370px', 'auto'], //宽高

//            type: 1,
//            closeBtn: 1,
//            area: ['300px', 'auto'], //宽高

//            type: 1,
//            shade: false,
//            area: ['370px', 'auto'], //宽高
//            title: '注册',
            content: '<div class="layer_box">'+
            '<div class="login_bg login_top">注册</div>'+
            '<div class="layer_li"><span class="fa fa-user"></span><input type="text" id="layer_user" placeholder="手机号"></div>'+
            '<div class="layer_li"><span class="fa fa-user"></span><input type="text" id="layer_nike" placeholder="请输入昵称"></div>'+
//            '<div class="layer_li"><span>用户名</span><input type="text" id="layer_user" placeholder="请输入手机号"></div>'+
            mbyzmDomStr+
            sjyzmDomStr
            +'<div class="layer_li"><span class="fa fa-key"></span><input type="password" id="layer_pass" placeholder="密码"></div><div id="qr_zhuce" class="qr_btn">确认注册</div>'+
        	'<div class="login_bg login_bto"></div>'+
            '</div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 createCode('code');
                 $('#code').click(function(){
                	 createCode('code');
                 })
                 $('#layer_user').blur(function(){
                	 tel_validate('layer_user')
                 })
                 if(mbyzmUrl!=''){
                	 $('#layer_getYzm').click(function(){
                    	 if(tel_validate('layer_user')){
                    		 if($('#layer_getYzm').text()!='获取验证码'){
                        		 return;
                        	 }
                        	 var count=60;
                        	 var setIntervalIndex=setInterval(function(){
                        		 $('#layer_getYzm').text(count+'秒后重新发送');
                        		 count=count-1;
                        	 },1000)
                        	 setTimeout(function(){
                        		 $('#layer_getYzm').text('获取验证码')
                        		 clearInterval(setIntervalIndex)
                        	 },60000)
                        	 mbyzmData.mobile=$('#layer_user').val().trim()
                    		 $.ajax({
                        		url:mbyzmUrl,
                        		type:'POST',
                        		data:mbyzmData,
                        		success:function(data){
                        			getYzm=true;
                        			if(typeof data =='string'){
                         				var data=$.parseJSON(data);
                         			}
                        			if(data.flag==10000){
                        				$('#layer_getYzm').attr('yzMobile',data.mobile);
                        				$('#layer_getYzm').attr('yzYzm',data.code)
                        			}else{
                        				layer.msg(data.msg);
                        			}
                        			
                        		}
                        	})
                    	 }
                     })
                 }  
                 var zcFlag=true;
                 $('#qr_zhuce').click(function(){
                	 
                	 if(!zcFlag){
                		 return;
                	 }
                	 zcFlag=false;
                	 if($('#layer_nike').val().trim()==''){
                		 layer.msg('请输入昵称');
                		 return;
                	 }
                	 if(tel_validate('layer_user')&&isNumber('layer_pass')){
                		 if(mbyzmUrl!=''){
	                		 if($('#layer_getYzm').attr('yzMobile')!=$('#layer_user').val().trim()){
	                			 layer.msg('手机号不一致,请重新获取验证码');
	                			 zcFlag=true;
	                			 return;
	                		 }
	                		 
	                		 if($('#layer_getYzm').attr('yzYzm')!=$('#layer_setYzm').val().trim()){
	                			 layer.msg('验证码有误');
	                			 zcFlag=true;
	                			 return;
	                		 }
                		 }else{
                			 if(!yzmvalidate({id:'layer_sjyzm'})){
                				 layer.msg('验证码有误');
                				 zcFlag=true;
	                			 return;
                			 }
                		 }
           				 var mobile=$('#layer_user').val().trim();
           				 var pass=$('#layer_pass').val().trim();
                		 var resTmp=function(){
                			 $.ajax({
                          		url:registerUrl,
                          		type:'POST',
                          		data:{mobile:$('#layer_user').val().trim(),passWord:$('#layer_pass').val().trim(),username:$('#layer_nike').val().trim()},//username:$('#layer_nike').val().trim()
                          		success:function(data){
                          			zcFlag=true;
                          			console.log(data);
                          			if(typeof data =='string'){
                          				var data=$.parseJSON(data);
                          			}
                          			if(data.flag==10000){
                          				layer.msg(data.msg);
                          				setTimeout(function(){
                          					layer.closeAll();
                              				/*loginFn({
                              					mobile:mobile,
                              					pass:pass,
                              					loginUrl:ghPublic.loginUrl,
                              					loginsuccess:ghPublic.loginsuccess,
                              					nextUrl:ghPublic.nextUrl,
                              					checkMobileurl:ghPublic.checkMobileurl?ghPublic.checkMobileurl:''
                              				})*/
                          				},500)
                          			}else{
                          				layer.msg(data.msg);
                          			}
                          			//注册完成后其他操作
                          			if(typeof registerSuccess =='function'){
                          				registerSuccess(data);
                          			}
                          		}
                          	})
                		 }
                		 resTmp();
                	 }else{
                		 zcFlag=true;
                	 }
                 })
            }
		})
	}