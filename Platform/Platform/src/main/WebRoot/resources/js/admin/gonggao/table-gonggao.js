$(function(){
	//按钮颜色--根据皮肤切换
	var v = localStorage.sccluiSkin;
	var bgcolor='#33AECC';
	if(v=='blue'){
		bgcolor='#1E9FFF';
	}else if(v=='molv'){
		bgcolor='#19a97b';
	}else if(v=='qinxin'){
		bgcolor='#33AECC';
	}
	$('.conb-add').css('background',bgcolor);
	//表格中所展示的字段的字段名
	var zdm=['content','status','saveTime','edit'];
	var successFn=function(ang){
		if(typeof ang=='string'){
			var ang=$.parseJSON(ang);
		}
		if(ang.flag==10000){
			$('#table tbody').children().remove();
			if(ang.list.length==0){
				$('#table table').hide();
				$('#table .zwsj').hide();
				$('<div class="zwsj">').css({'textAlign':'center','lineHeight':'300px','fontSize':'30px'}).text('暂无数据').prependTo('#table').show();
			}else{
				$('#table .zwsj').hide();
				$('#table table').show();
				ghtable.tbody({
					parent:'#table tbody',
					data:ang.list,
					page:ang.current,
					titlezi:zdm
				})
				ghtable.pages({
					parent:'#table',
					pages:ang.page,
					url:ctx+'/alluser/adminuser',
					callback:successFn,
					currPage:ang.current,
				});
				$('#table tbody td[name=edit]').each(function(index){//<span class="table_edit bianji">编辑</span> | 
	    			$(this).html('<span class="table_edit bianji">编辑</span> |<span class="table_del">删除</span> | <span class="table_edit ckxq">查看详情</span>')    			
	    		})
	    		$('#table tbody td[name=content]').each(function(index){//<span class="table_edit bianji">编辑</span> | 
	    			$(this).attr('content',$(this).text());
	    			if($(this).text().length>30){
	    				$(this).text($(this).text().substr(0, 30)+'...')
	    			}
	    		})
	    		$('#table tbody td[name=status]').each(function(index){//<span class="table_edit bianji">编辑</span> | 
	    			if($(this).text()=='1'){
	    				$(this).text('显示')
	    			}else{
	    				$(this).text('隐藏')
	    			}
	    		})
			}
		}
	}
	//table中的编辑
	$('#table tbody').delegate('.bianji','click',function(){
		var thisid=$(this).parents('tr').attr('name');
		var that=this;
		var content=$(this).parent().parent().find('td[name=content]').attr('content');
		if($(this).parent().parent().find('td[name=status]').text()=='显示'){
			var statusStr='<select id="layer_statue"><option value="1" selected="selected">显示</option><option value="2">不显示</option></select>'
		}else{
			var statusStr='<select id="layer_statue"><option value="1">显示</option><option value="2" selected="selected">不显示</option></select>'
		}
		//回显状态
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '编辑',
           /* offset: ['25%','35%'],*/
            offset: '25%',
            content: '<div class="layer_box">'+
            '<div class="layer_li"><span>状态</span>'+statusStr+'</div>'+
            '<div class="layer_li"><span style="display: block;float: left;height: 100px;line-height: 100px;">内容</span><textarea id="layer_content" style="width:182px;height:100px">'+content+'</textarea></div>'+
            '<div style="clear:both;"></div>'+
            '<div id="qr_bianji" class="qr_btn" istrue="false">确认修改</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('.layui-layer-content').css({'height': 'auto'});
                 $('#qr_bianji').css('background',bgcolor);
                 $('#qr_bianji').click(function(){
                		 var setData={};
                    	 setData.status=$('#layer_statue').find('option:selected').val().trim();
                    	 setData.content=$('#layer_content').val().trim();
                    	 setData.id=thisid;
                    	 setData.type='2';
                    	 if(setData.content==''){
                    		 layer.msg('请输入公告内容');
                    		 return;
                    	 }
                    	 console.log(setData);
                    	 $.ajax({
                 	    	url:ctx+'/admin/edit/votice.htm',
                 	    	type:'POST',
                 	    	cache: false,
                 	    	data:setData,
                 	    	success:function(res){
                 	    		if(typeof res=='string'){
	                 	   			var res=$.parseJSON(res);
	                 	   		}
                 	    		console.log(res.flag)
                 	    		if(res.flag==10000||res.flag==10000){
                 	    			layer.msg(res.msg);
        		 	    		    setTimeout(function(){
        		 	    		    	layer.closeAll();
        		 	    		    	getDataFn();
        		 	    		    },500)
                 	    		}else{
                 	    			layer.msg(res.msg);
                 	    		}
                 	    	}
                 	    })
                 })
                 
            }
		})
	})
	//新增
	$('#table').delegate('.conb-add','click',function(){
		var statusStr='<select id="layer_statue"><option value="1">显示</option><option value="2">不显示</option></select>'
		//回显状态
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '新增',
           /* offset: ['25%','35%'],*/
            offset: '25%',
            content: '<div class="layer_box">'+
            '<div class="layer_li"><span>状态</span>'+statusStr+'</div>'+
            '<div class="layer_li"><span style="display: block;float: left;height: 100px;line-height: 100px;">内容</span><textarea id="layer_content" style="width:182px;height:100px"></textarea></div>'+
            '<div style="clear:both;"></div>'+
            '<div id="qr_add" class="qr_btn" istrue="false">确认修改</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('.layui-layer-content').css({'height': 'auto'});
                 $('#qr_add').css('background',bgcolor);
                 $('#qr_add').click(function(){
	                	 var setData={};
	                	 setData.status=$('#layer_statue').find('option:selected').val().trim();
	                	 setData.content=$('#layer_content').val().trim();
	                	 setData.type='1';
	                	 if(setData.content==''){
	                		 layer.msg('请输入公告内容');
	                		 return;
	                	 }
	                	 console.log(setData);
	                	 $.ajax({
                 	    	url:ctx+'/admin/edit/votice.htm',
                 	    	type:'POST',
                 	    	cache: false,
                 	    	data:setData,
                 	    	success:function(res){
                 	    		if(typeof res=='string'){
	                 	   			var res=$.parseJSON(res);
	                 	   		}
                 	    		console.log(res.flag)
                 	    		if(res.flag==10000||res.flag=='10000'){
                 	    			layer.msg(res.msg);
        		 	    		    setTimeout(function(){
        		 	    		    	layer.closeAll();
        		 	    		    	getDataFn();
        		 	    		    },500)
                 	    		}else{
                 	    			layer.msg(res.msg);
                 	    		}
                 	    	}
                 	    })
                 })
                 
            }
		})
	})
	//删除
	$('#table tbody').delegate('.table_del','click',function(){
		var id=$(this).parents('tr').attr('name');
		layer.confirm('是否删除', {
			  btn: ['是','否'], //按钮
			  success:function(){
				  $('.layui-layer-title').hide();
	              $('.layui-layer-content').css({'text-align':'center'});
	              $('.layui-layer-btn').css({'text-align':'center'});
			  }
			}, function(){
				 $.ajax({
          	    	url:ctx+'/admin/edit/votice.htm',
          	    	type:'POST',
          	    	cache: false,
          	    	data:{id:id,type:'3'},
          	    	success:function(res){
          	    		if(typeof res=='string'){
              	   			var res=$.parseJSON(res);
              	   		}
          	    		console.log(res.flag)
          	    		if(res.flag==10000||res.flag=='10000'){
          	    			layer.msg(res.msg);
 		 	    		    setTimeout(function(){
 		 	    		    	layer.closeAll();
 		 	    		    	getDataFn();
 		 	    		    },500)
          	    		}else{
          	    			layer.msg(res.msg);
          	    		}
          	    	}
          	    })
			}, function(){
			  
			});
	})
	//查看详情
	$('#table tbody').delegate('.ckxq','click',function(){
		var content=$(this).parent().parent().find('td[name=content]').attr('content');
		//回显状态
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['500px', 'auto'], //宽高
            title: '详情',
           /* offset: ['25%','35%'],*/
            offset: '25%',
            content: '<div class="layer_box">'+content+'</div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)','text-align': 'center'});
                 $('.layui-layer-content').css({'height': 'auto'});
            }
		})
	})
	//加载头部
	ghtable.title({
		parent:'#table thead',
		courses:['内容','状态','时间','操作'],
		zdm:zdm
	})
    //获取数据
    var getDataFn=function(){
    	$.ajax({
        	url:ctx+'/admin/notice/list.htm',
        	type:'POST',
        	cache: false,
        	success:function(res){
        		console.log(res);
        		//res.search=[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }]
        		successFn(res)
        	}
        })
    }
    getDataFn();
    /*ghtable.sousuo({
		parent:".sousuo",
		value:"用户名查询",
		url:ctx+'/admin/user.htm',
		callback:successFn,
		data:[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }],
		success:function(selector){
			$(selector).css('background',bgcolor);
		},
		searchkey:'account'
	})*/
})