$(function(){
	//按钮颜色--根据皮肤切换
	var v = localStorage.sccluiSkin;
	var bgcolor='#33AECC';
	if(v=='blue'){
		bgcolor='#1E9FFF';
	}else if(v=='molv'){
		bgcolor='#19a97b';
	}else if(v=='qinxin'){
		bgcolor='#33AECC';
	}
	
	//表格中所展示的字段的字段名
	var zdm=['mobile','username','userType','saveTime','edit'];
	var successFn=function(ang){
		if(ang.flag=='10000'){
			$('#table tbody').children().remove();
			if(ang.list.length==0){
				$('#table table').hide();
				$('#table .zwsj').hide();
				$('<div class="zwsj">').css({'textAlign':'center','lineHeight':'300px','fontSize':'30px'}).text('暂无数据').prependTo('#table').show();
			}else{
				$('#table .zwsj').hide();
				$('#table table').show();
				ghtable.tbody({
					parent:'#table tbody',
					data:ang.list,
					page:1,
					titlezi:zdm
				})
				ghtable.pages({
					parent:'#table',
					pages:1,
					url:ctx+'/alluser/adminuser',
					callback:successFn,
					currPage:1,
				});
	    		$('#table tbody td[name=userType]').each(function(index){
	    			switch ($(this).text()) {
					case '1':
						$(this).html('计划员');
						break;
					case '2':
						$(this).html('至尊');
						break;
					case '3':
						$(this).html('皇冠');
						break;
					case '4':
						$(this).html('铂金');
						break;
					case '5':
						$(this).html('黄金');
						break;
					case '6':
						$(this).html('vip');
						break;
					case '7':
						$(this).html('普通会员');
						break;
					default:
						//$(this).text("未知");
						$(this).text("禁用");	
						break;
					}
	    		})
	    		$('#table tbody td[name=edit]').each(function(index){
	    			$(this).html('<span class="table_edit bianji">编辑</span> | <span class="table_del">删除</span> | <span class="table_edit updatapass">修改密码</span>')    			
	    		})
			}
		}
	}
	//table中的编辑
	$('#table tbody').delegate('.bianji','click',function(){
		var thisid=$(this).parents('tr').attr('name');
		var that=this;
		var hyType='';
		console.log($(this).parent().parent().find('td[name=userType]').text());
		if($(this).parent().parent().find('td[name=userType]').text()=='计划员'){
			layer.msg('计划员不可编辑');
			return;
		}else if($(this).parent().parent().find('td[name=userType]').text()=='至尊'){
			hyType+='<select id="layer_usertype"><option value="2" selected="selected">至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='皇冠'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3" selected="selected">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='铂金'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4" selected="selected">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='黄金'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5" selected="selected">黄金</option><option value="6">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='vip'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6" selected="selected">vip</option><option value="7">会员</option></select>';
		}else if($(this).parent().parent().find('td[name=userType]').text()=='普通会员'){
			hyType+='<select id="layer_usertype"><option value="2" >至尊</option><option value="3">皇冠</option><option value="4">铂金</option><option value="5">黄金</option><option value="6">vip</option><option value="7" selected="selected">会员</option></select>';
		}
		console.log(hyType);
		//回显状态
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '编辑',
           /* offset: ['25%','35%'],*/
            offset: '25%',
            content: '<div class="layer_box">'+
            /*'<div class="layer_li"><span>姓名</span><input type="text" id="layer_name" value="'+relaname+'"></div>'+
            '<div class="layer_li"><span>机构</span>'+deptStr+'</div>'+*/
            '<div class="layer_li"><span>会员类型</span>'+hyType+'</div>'+
            '<div style="clear:both;"></div>'+
            '<div id="qr_bianji" class="qr_btn" istrue="false">确认修改</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('.layui-layer-content').css({'height': 'auto'});
                 $('#qr_bianji').css('background',bgcolor);
                 $('#qr_bianji').click(function(){
                		 var setData={};
                    	 setData.userType=$('#layer_usertype').find('option:selected').val();
                    	 var userrole='';
                    	 console.log(setData);
                    	 $.ajax({
                 	    	url:ctx+'/alluser/adminuser/edit',
                 	    	type:'POST',
                 	    	cache: false,
                 	    	data:setData,
                 	    	success:function(res){
                 	    		console.log(res);
                 	    		if(res.ret=='0'){
                 	    			layer.msg('编辑成功');
        		 	    		    setTimeout(function(){
        		 	    		    	layer.closeAll();
        		 	    		    	getDataFn();
        		 	    		    },500)
                 	    		}else{
                 	    			layer.msg('编辑失败');
                 	    		}
                 	    	}
                 	    })
                 })
                 
            }
		})
	})
	//新增
	$('#table').delegate('.conb-add','click',function(){
		var statusStr='';
		statusStr='<select id="layer_status"><option value ="DISABLED">禁用</option><option value = "ENABLED" selected="selected">启用</option></select>';
		var deptStr='<select id="layer_org">';
		for(var di=0;di<dept.length;di++){
			if(di==0){
				deptStr+='<option value="'+dept[di].id+'" selected="selected">'+dept[di].name+'</option>';
			}else{
				deptStr+='<option value="'+dept[di].id+'">'+dept[di].name+'</option>';
			}
		}
		deptStr+='</select>';
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '新增',
            offset: '25%',
            content: '<div class="layer_box"><div class="layer_li"><span>用户名</span><input type="text" id="layer_account" value=""></div>'+
            '<div class="layer_li"><span>姓名</span><input type="text" id="layer_name" value=""></div>'+
            '<div class="layer_li"><span>机构</span>'+deptStr+'</div>'+
            '<div class="layer_li"><span>手机</span><input type="text" id="layer_mobile"></div>'+
            '<div class="layer_li"><span>邮箱</span><input type="text" id="layer_email"></div>'+
            '<div class="layer_li"><span>密码</span><input type="text" id="layer_pass"></div>'+
            '<div class="layer_li" id="layer_role" style="overflow:hidden;width:100%;"><span>角色</span></div>'+
            '<div class="layer_li" style="margin-top: -20px;"><span></span>'+roleSel+'</div>'+
            //'<div class="layer_li"><span>状态</span>'+statusStr+'</div>'+
            '<div id="qr_bianji" class="qr_btn" istrue="false">确认新增</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('.layui-layer-content').css({'height': 'auto'});
                 $('#qr_bianji').css('background',bgcolor);
                 //角色删除
                 $('.layui-layer-content').delegate('.role-move','click',function(){
                	 if($('.role-move').length<=1){
                		 layer.msg('必须绑定至少一个角色');
                	 }else{
                		 $(this).parent().remove();
                	 }
                 })
                 $('.layui-layer-content').delegate('#layer_role_select','change',function(){
                	 var xzid=$(this).children('option:selected').val().trim();
                	 var xzname=$(this).children('option:selected').text().trim();
                	 var ylids=[];
                	 for(var bi=0;bi<$('#layer_role b').length;bi++){
                		 ylids.push($($('#layer_role b')[bi]).attr('roleid').trim());
                	 }
                	 if(!contains(ylids,xzid)){
            			 $('#layer_role').append('<b roleid="'+ xzid+'">'+ xzname+'<i class="glyphicon glyphicon-remove font-red role-move"></i></b>');
                	 }else{
                		 layer.msg('已添加角色')
                	 }
                 })
                 $('#qr_bianji').click(function(){
                	 //验证
                	 if(tel_validate('layer_mobile')){
                		 var account=$('#layer_account').val().trim();
                		 var realname=$('#layer_name').val().trim();
                		 var email=$('#layer_email').val().trim();
                		 var pass=$('#layer_pass').val().trim();
                		 if(account==''){
                    		 layer.msg('请输入用户名');
                    		 return;
                    	 }
//                		 if(email==''){
//                    		 layer.msg('请邮箱');
//                    		 return;
//                    	 }
//                		 if(!checkEmail(email)){
//                			 layer.msg('邮箱不正确');
//                    		 return;
//                		 }
                		 if(realname==''){
                    		 layer.msg('请输入真是姓名');
                    		 return;
                    	 }
                		 if(pass==''){
                    		 layer.msg('请输入密码');
                    		 return;
                    	 }
                		 if(pass.length<6){
                    		 layer.msg('密码不能小于6位');
                    		 return;
                    	 }
                		 if($('.role-move').length<1){
                    		 layer.msg('必须绑定至少一个角色');
                    		 return;
                    	 }
                		 var setData={};
                    	 setData.realname=realname;
                    	 setData.mobile=$('#layer_mobile').val().trim();
                    	 setData.email=email;
                    	 setData.org=$('#layer_org').find('option:selected').val();
                    	 var userrole='';
                    	 for(var ul=0;ul<$('#layer_role b').length;ul++){
                    		 userrole+=$($('#layer_role b')[ul]).attr('roleid')+',';
                    	 }
                    	 console.log(userrole);
                    	 setData.allroleIds=userrole.slice(0,-1);
                    	 //setData.status=$('#layer_status').find('option:selected').val();
                    	 setData.account=account;
                    	 setData.password=pass;
                    	 console.log(setData);
                    	 $.ajax({
                 	    	url:ctx+'/alluser/adminuser/create',
                 	    	type:'POST',
                 	    	cache: false,
                 	    	data:setData,
                 	    	success:function(res){
                 	    		console.log(res);
                 	    		if(res.ret=='0'){
                 	    			layer.msg('新增成功');
        		 	    		    setTimeout(function(){
        		 	    		    	layer.closeAll();
        		 	    		    	getDataFn();
        		 	    		    },500)
                 	    		}else{
                 	    			layer.msg(res.msg);
                 	    		}
                 	    	}
                 	    })
                	 }
                 })
                 
            }
		})
	})
	//删除
	$('#table tbody').delegate('.table_del','click',function(){
		var username=$(this).parents('tr').find('td[name=account]').text();
		layer.confirm('是否删除', {
			  btn: ['是','否'], //按钮
			  success:function(){
				  $('.layui-layer-title').hide();
	              $('.layui-layer-content').css({'text-align':'center'});
	              $('.layui-layer-btn').css({'text-align':'center'});
			  }
			}, function(){
				$.ajax({
			    	url:ctx+'/alluser/adminuser/delete',
			    	type:'POST',
			    	cache: false,
			    	data:{username:username},
			    	success:function(res){
			    		console.log(res);
			    		if(res.ret=='0'){
			    			layer.msg('删除成功');
		 	    		    setTimeout(function(){
		 	    		    	layer.closeAll();
		 	    		    	getDataFn();
		 	    		    	
		 	    		    },500)
		 	    		}else{
		 	    			layer.msg('删除失败');
		 	    		}
			    	}
			    })
			}, function(){
			  
			});
	})
	//修改密码
	$('#table tbody').delegate('.updatapass','click',function(){
		var username=$(this).parents('tr').find('td[name=account]').text();
		layer.open({
            type: 1,
            closeBtn: 1,
            area: ['300px', 'auto'], //宽高
            title: '修改密码',
            offset: '25%',
            content: '<div class="layer_box"><div class="layer_li"><span>新密码</span><input type="text" id="layer_pass"></div>'+
            '<div id="qr_bianji" class="qr_btn" istrue="false">确认修改</div></div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)'});
                 $('.layui-layer-content').css({'height': 'auto'});
                 $('#qr_bianji').css('background',bgcolor);
                 $('#qr_bianji').click(function(){
                	 if($('#layer_pass').val().trim()==''){
                		 layer.msg('新密码不能为空');
                		 $('#layer_pass').focus();
                		 return;
                	 }
                	 if($('#layer_pass').val().trim().length<6){
                		 layer.msg('密码不能小于6位');
                		 $('#layer_pass').focus();
                		 return;
                	 }
                	 $.ajax({
     			    	url:ctx+'/alluser/adminuser/editpwd',
     			    	type:'POST',
     			    	cache: false,
     			    	data:{username:username,password:$('#layer_pass').val().trim()},
     			    	success:function(res){
     			    		console.log(res);
     			    		if(res.ret=='0'){
     			    			layer.msg('密码修改成功');
     			    			setTimeout(function(){
    		 	    		    	layer.closeAll();
    		 	    		    },500)
     		 	    		}else{
     		 	    			layer.msg('密码修改失败');
     		 	    		}
     			    	}
     			    })
                 })
            }
		})
	})
	//加载头部
	ghtable.title({
		parent:'#table thead',
		courses:['用户名','昵称','会员类型','时间','操作'],
		zdm:zdm
	})
	var aoData = [];
    aoData.push( { "name": "pageNumber", "value": 0 });
    aoData.push( { "name": "pageSize", "value": 12 });
/*    aoData.push( { "name": "search_lft", "value": lft});
    aoData.push( { "name": "search_rgt", "value": rgt });*/
    //aoData.id=$('body').attr('thisid');
    aoData.push( { "name": "search_EQ@a.id", "value": $('body').attr('thisid') });
    //获取数据
    var getDataFn=function(){
    	$.ajax({
        	url:ctx+'/admin/user.htm',
        	"dataType": 'json',
        	"contentType": "application/json",
        	type:'POST',
        	cache: false,
        	data:JSON.stringify(aoData),
        	success:function(res){
        		console.log(res);
        		//res.search=[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }]
        		successFn(res)
        	}
        })
    }
    getDataFn();
    ghtable.sousuo({
		parent:".sousuo",
		value:"用户名查询",
		url:ctx+'/admin/user.htm',
		callback:successFn,
		data:[{ "name": "pageNumber", "value": 0 },{ "name": "pageSize", "value": 12 }],
		success:function(selector){
			$(selector).css('background',bgcolor);
		},
		searchkey:'account'
	})
})