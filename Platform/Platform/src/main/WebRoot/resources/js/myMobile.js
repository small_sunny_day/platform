/**
 * Created by gh on 2017/8/21.
 */
$(function(){
	$('#publicChat').height(document.documentElement.clientHeight-50-55-40);
	//公告
    var datas=$.parseJSON($('#msg_tip_show').attr('datas')).list;
    var ggstr='';
    for(var i=0;i<datas.length;i++){
    	ggstr+='<span style="color:white;width:100%;display:inline-block;overflow: hidden;text-overflow:ellipsis;white-space: nowrap;margin-left:20px">'+datas[i]+' </span> '
    }
    $('#msg_tip_show').append(ggstr);
	//share
	function ShareTip(){};
	//分享到腾讯微博  
	ShareTip.prototype.sharetoqq=function(content,url,picurl)  {  
	 var shareqqstring='http://v.t.qq.com/share/share.php?title='+content+'&url='+url+'&pic='+picurl;  
	 window.open(shareqqstring,'newwindow','height=100,width=100,top=100,left=100');  
	}  
	//分享到新浪微博  
	ShareTip.prototype.sharetosina=function(title,url,picurl)  {  
	 var sharesinastring='http://v.t.sina.com.cn/share/share.php?title='+title+'&url='+url+'&content=utf-8&sourceUrl='+url+'&pic='+picurl;  
	 window.open(sharesinastring,'newwindow','height=400,width=400,top=100,left=100');  
	}  
	//分享到QQ空间  
	ShareTip.prototype.sharetoqqzone=function(title,url,picurl)  {  
	 var shareqqzonestring='http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?summary='+title+'&url='+url+'&pics='+picurl;  
	 window.open(shareqqzonestring,'newwindow','height=400,width=400,top=100,left=100');  
	}
	
	//获取开奖吗
	/*var thisPlan=$.parseJSON($('#myTab0_Content0').attr('thisplan'));
	$('#bjpk10-expect').text(thisPlan.plan.title);
	var planArr=thisPlan.plan.number.split(',');
	var plans='';
	var numsss='';
	for(var i=0;i<planArr.length;i++){
		if(planArr[i][0]=='0'){
			numsss=planArr[i].substr(1,2)
		}else{
			numsss=planArr[i]
		}
		plans+='<span class="bjpk10_no'+numsss+'">'+numsss+'</span>'
	}
	$('#expectbjpk10').append(plans);*/
	
	//获取当前时间
	function getNowFormatDate() {
	    var date = new Date();
	    var seperator1 = "-";
	    var seperator2 = ":";
	    var month = date.getMonth() + 1;
	    var strDate = date.getDate();
	    if (month >= 1 && month <= 9) {
	        month = "0" + month;
	    }
	    if (strDate >= 0 && strDate <= 9) {
	        strDate = "0" + strDate;
	    }
	    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
	            + " " + date.getHours() + seperator2 + date.getMinutes()
	            + seperator2 + date.getSeconds();
	    /*var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " ;*/
	    return currentdate;
	}
	//获取计划信息
	var getPlanFn=function(){
		$.ajax({
	  		url:basePath+'/plan.htm',
	  		type:'POST',
	  		success:function(data){
	  			var data=$.parseJSON(data);
	  			var chatStr=''
	  			if(data.flag=='10000'||data.flag==10000){
	  				chatStr+='<div class="msg manage" id="00d127cc">'+
	  	            '<div class="msg_head"><img src="'+path+'/resources/image/3501.png" class="msg_group_ico"></div>'+
	  	            '<div class="msg_content"><div><font class="u">计划员</font>&nbsp;&nbsp;<font class="date">'+
	  	            getNowFormatDate()+
	  	            '</font></div>'+
	  	            '<div class="layim_chatsay">'+
	  	            data.obj+
	  	            '</div>'+
	  	            '</div></div><div style="clear:both;"></div>';
	  				$('#publicChat').append(chatStr);
	  		    	$('#publicChat').scrollTop( $('#publicChat')[0].scrollHeight);
	  		    	//修改头部
	  		    	$('#expectbjpk10').children().remove();
	  		    	$('#bjpk10-expect').text(data.titile);
		  		  	var planArr=data.number.split(',');
		  		  	var plans='';
			  		var numsss='';
			  		for(var i=0;i<planArr.length;i++){
			  			if(planArr[i][0]=='0'){
			  				numsss=planArr[i].substr(1,2)
			  			}else{
			  				numsss=planArr[i]
			  			}
			  			plans+='<span class="bjpk10_no'+numsss+'">'+numsss+'</span>'
			  		}
		  		  	$('#expectbjpk10').append(plans);
	  			}
	  			getPlanFn();
	  		}
		})
	}
	getPlanFn();
	//底部信息操作
	//加载表情
    $('.smile').qqFace({
        id : 'facebox',
        assign:'messageEditor',
        path:path+'/resources/arclist/'	//表情存放的路径
    });
    //更多
    var boxHeight=$('#moreFunctionbox').height();
    $('#moreFunctionbox').height(0);
    $('#moreFunction').click(function(){
        var that=this;
        $('#facebox').hide();
        if($('#moreFunctionbox').height()==0){
            $('#moreFunctionbox').show();
            $('#moreFunctionbox').animate({'height':boxHeight},500);
            $('#footer').animate({'bottom':'105px'},500);
            $(that).addClass('xuanzhuan');
        }else{
            $('#moreFunctionbox').animate({'height':0},500,function(){
                $('#moreFunctionbox').hide();
            });
            $('#footer').animate({'bottom':'0px'},500);
            $(that).removeClass('xuanzhuan');
        } 
    })
  //登录
	var wsServer = null;
    var ws = null;
    ghPublic.loginUrl=basePath+'login.htm';
	ghPublic.nextUrl='';
	ghPublic.loginsuccess=function(data){
		if(data.flag==10000){
				setTimeout(function(){
					layer.closeAll();
//					location.reload();	
					console.log(data);
					user=data.user;
			        ws = new WebSocket(wsServer); //创建WebSocket对象
			        ws.onopen = function (evt) {
			        	console.log("已经建立连接", { offset: 0});
			        };
			        ws.onmessage = function (evt) {
			        	console.log(evt);
			            analysisMessage(evt.data);  //解析后台传回的消息,并予以展示
			        };
			        ws.onerror = function (evt) {
//				        layer.msg("产生异常", { offset: 0});
			        	contFn();
			        };
			        ws.onclose = function (evt) {
			            //layer.msg("已经关闭连接", { offset: 0});
			        };
				},500)
			}else if(data.flag==10001){
				layer.msg('请先注册')
				setTimeout(function(){
					layer.closeAll;
					zhuceFn({registerUrl:basePath+'register.htm'});
				},500)
			}else{
				layer.msg(data.msg);
			}
	};
	ghPublic.registerUrl=basePath+'register.htm';
    $('#login').click(function(){
    	if(user!=''){
    		layer.msg('已登录');
    		return;
    	}
    	loginFn({
        	loginUrl:ghPublic.loginUrl,
        	nextUrl:ghPublic.nextUrl,
        	loginsuccess:ghPublic.loginsuccess
        });
    })
     $('#res').click(function(){
    	 zhuceFn({
    		 registerUrl:ghPublic.registerUrl
    		 });
    })
    //分享
    var ShareTip=new ShareTip();
    $('#sharedBtn').click(function(){
    	layer.open({
            type: 1,
            closeBtn: 1,
            area: ['70%', 'auto'], //宽高
            title: '分享',
            content: '<div class="layer_box">'+
//            	'<div class="layer_li"><div class="layer_share">分享到腾讯微博</div></div>'+
            	'<div class="layer_li"><div class="layer_share">分享到新浪微博</div></div>'+
            	'<div class="layer_li"><div class="layer_share">分享到qq空间</div></div>'+
            	'</div>',
            success:function(){
            	 $('.layui-layer-title').css({'padding': 0, 'text-align': 'center', 'background': 'white'});
                 $('.layui-layer-content').css({'background': 'rgb(243, 243, 243)','height':'120px'});//170px
                 $('.layer_share').css({
            	     'text-align': 'center',
	                 'background': 'green',
	                 'color': 'white',
	                 'height': '30px',
	                 'line-height': '30px',
	                 'border-radius': '3px',
                 })
                 $('.layer_share').click(function(){
                	 var content,url,picurl;
                	 content='53彩票是.......';
                	 url=basePath+'index.htm';
                	 picurl=path+'/resources/images/logo.png';
                	 console.log(picurl)
                	 if($(this).text()=='分享到腾讯微博'){
                		 ShareTip.sharetoqq(content,url,picurl)
                	 }
                	 if($(this).text()=='分享到新浪微博'){
                		 ShareTip.sharetosina(content,url,picurl)
                	 }
                	 if($(this).text()=='分享到qq空间'){
                		 ShareTip.sharetoqqzone(content,url,picurl)
                	 }
                 })
            }
    	})
    })
    //签到
    $('.signin-container').click(function(){
    	if(user==''){
    		layer.msg('请先登录');
    		return;
    	}
		$.ajax({
			"dataType" : 'json',
			"type" : "POST",
			"url" : basePath+'sign.htm',
			"cache" : false,
			"data":{'userId':user.id},
			"success" : function(data) {
				console.log(data)			    		
				if(data.flag == 10000){
					layer.msg(data.msg);
				}else{
					layer.msg(data.msg);
				}
				
			}	
			
		})
     
    })
    //聊天
    var wsServer = null;
    var ws = null;
    wsServer = "ws://" + location.host+"/chatServer";
    if(user!=''){
        ws = new WebSocket(wsServer); //创建WebSocket对象 Platform/
        ws.onopen = function (evt) {
        	console.log("已经建立连接", { offset: 0});
        };
        ws.onmessage = function (evt) {
        	console.log(evt);
            analysisMessage(evt.data);  //解析后台传回的消息,并予以展示
        };
        ws.onerror = function (evt) {
//        	        layer.msg("产生异常", { offset: 0});
        	contFn();
        };
        ws.onclose = function (evt) {
            //layer.msg("已经关闭连接", { offset: 0});
        };
    }
   
    var contFn=function(){
	    ws = new WebSocket(wsServer); //创建WebSocket对象
    }
    //解析消息
    function analysisMessage(message){
        message = JSON.parse(message);
        if(message.type == "message"){      //会话消息
            showChat(message.message);
        }
        if(message.type == "notice"){       //提示消息
            showNotice(message.message);
        }
        if(message.list != null && message.list != undefined){      //在线列表
            showOnline(message.list);
        }
    }
    function showChat(message){
    	console.log(message);
    	if(message.username!='计划员'){
    	 	var chatStr='<div class="msg notmine" id="00d127cc">'+
            '<div class="msg_head"><img src="'+path+'/resources/images/null.jpg" class="msg_group_ico"></div>'+
            '<div class="msg_content"><div><font class="u">'+
            message.username+
            '</font>&nbsp;&nbsp;<font class="date">'+
            message.time+
            '</font></div>'+
            '<div class="layim_chatsay">'+
            message.content+
            '</div>'+
            '</div></div><div style="clear:both;"></div>';
    	}else{
    		var chatStr='<div class="msg manage" id="55826dec"><div class="msg_head"><img src="'+path+'resources/image/3501.png" class="msg_group_ico"></div>'+
                '<div class="msg_content"><div><font class="u">'+
	            message.username+
	            '</font>&nbsp;&nbsp;<font class="date">'+
	            message.time+
	            '</font></div>'+
	            '<div class="layim_chatsay">'+
	            message.content+
	            '</div>'+
	            '</div></div><div style="clear:both;"></div>';
    	}
    	$('#publicChat').append(chatStr);
    	$('#publicChat').scrollTop( $('#publicChat')[0].scrollHeight);
    }
    function showNotice(message){
    	if(message.indexOf('计划员')>0){
    		return;
    	}
    	var chatStr='<li class="history-hr-wrap"><div class="history-hr"></div><div class="history-hr-text" style="color:white!important;width:100%;">'+message+'</div></li>'
    	$('#publicChat').append(chatStr);
    	$('#publicChat').scrollTop( $('#publicChat')[0].scrollHeight);
    }
    function showOnline(){
    	
    }
    //时间
    function appendZero(s){return ("00"+ s).substr((s+"").length);}  //补0函数
    function getDateFull(){
        var date = new Date();
        var currentdate = date.getFullYear() + "-" + appendZero(date.getMonth() + 1) + "-" + appendZero(date.getDate()) + " " + appendZero(date.getHours()) + ":" + appendZero(date.getMinutes()) + ":" + appendZero(date.getSeconds());
        return currentdate;
    }
    //发送消息
    function sendMessage(ang){
    	var userid=ang.userid?ang.userid:'';
    	var massage=ang.massage?ang.massage:'';
        if(ws == null){
            layer.msg("连接未开启!", { offset: 0, shift: 6 });
            return;
        }
        //var to = $("#sendto").text() == "全体成员"? "": $("#sendto").text();
        var to = '';
        //$("#tuling").text() == "已上线"? tuling(message):console.log("图灵机器人未开启");  //检测是否加入图灵机器人
        ws.send(JSON.stringify({
            message : {
                content : massage,
                from : userid,
                to : to,      //接收人,如果没有则置空,如果有多个接收人则用,分隔
                time : getDateFull(),
                username:user.username
            },
            type : "message"
        }));
    }
    $('#sendBtn').click(function(){
    	if(user==''){
    		layer.msg('请先登录');
    		return;
    	}
    	var str=$('#messageEditor').html().trim();
    	if(str==''){
    		layer.msg('说点什么呗');
    		return;
    	}
    	sendMessage({
    		userid:user.id,
    		massage:str,
    	})
    	$('#messageEditor').html('')
    })
});