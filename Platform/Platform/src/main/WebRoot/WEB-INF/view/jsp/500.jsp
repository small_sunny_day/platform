<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>500</title>
<link rel="stylesheet" href="<%=path %>/resources/css/error.css">
</head>
<body>
 <div class="box">
        <div class="left-box">
            <img src="<%=path %>/resources/img/500.png" alt="">
        </div>
        <div class="right-box">
            <ul>
                <li><i>抱歉!</i>您所访问的页面不存在.</li>
                <li>您要查看的网址可能已被删除或者暂时不可用.</li>
                <li>点击以下链接继续浏览网站</li>
                <li>&gt;&gt;<a href="../">返回网站首页</a></li>
            </ul>
        </div>
    </div>
</body>
</body>
</html>