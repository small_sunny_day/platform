<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%  
String base = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+base+"/";  
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>404</title>
<link rel="stylesheet"
	href="<%=base%>/resources/css/error.css">
</head>
<body>
	<div class="box">
		<div class="left-box">
			<img src="<%=base%>/resources/img/404.png"
				alt="">
		</div>
		<div class="right-box">
			<ul>
				<li><i>抱歉!</i>您所访问的页面不存在.</li>
				<li>您要查看的网址可能已被删除或者暂时不可用.</li>
				<li>点击以下链接继续浏览网站</li>
				<li>&gt;&gt;<a href="../">返回网站首页</a></li>
			</ul>
		</div>
	</div>
</body>
</html>