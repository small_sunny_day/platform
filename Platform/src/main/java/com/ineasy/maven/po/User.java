package com.ineasy.maven.po;

import com.ineasy.maven.util.tool.UUIDUtils;

public class User {
	
	@SuppressWarnings("unused")
	private String id = UUIDUtils.getUUID();
	
	private String mobile;
	
	private String passWord;
	
	private String userType;
	
	private String saveTime;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getSaveTime() {
		return saveTime;
	}

	public void setSaveTime(String saveTime) {
		this.saveTime = saveTime;
	}

}
