package com.ineasy.maven.easemob.util;

import java.util.HashMap;
import java.util.Map;

import com.ineasy.maven.util.tool.HttpUtils;

import net.sf.json.JSONObject;

public class EasemobUtils {

	public static Map<String,Object> index(){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("org_name",EasemobConfig.org_name);
		map.put("app_name",EasemobConfig.app_name);
		return map;
	}


	//返回Json字符串
	public static String toJson(Map<String, Object> jsonMap) {
		StringBuffer buffer = new StringBuffer();
		if (jsonMap.size() > 0) {
			buffer.append("{");
			buffer.append(",");
			for (String key : jsonMap.keySet()) {
				if (!key.equals("class"))
					//"\"name\"";
					buffer.append("\""+key+"\"" + ":\"" + jsonMap.get(key) + "\",");
				  //buffer.append(key+ ":" + jsonMap.get(key) + ",");
			}
			// 去掉最后一个','
			buffer.deleteCharAt(buffer.length() - 1);
			buffer.deleteCharAt(1);
		}
		buffer.append("}");
		return buffer.toString();
	}
	
	 //获取token
	public static String getAccessToken() {
		String url = EasemobConfig.token_url;
		Map<String,Object> jsonMap = new HashMap<String, Object>(); 
		jsonMap.put("grant_type", EasemobConfig.grant_type);
		jsonMap.put("client_id", EasemobConfig.client_id);
		jsonMap.put("client_secret", EasemobConfig.client_secret);
		String result = HttpUtils.sendPost(url, jsonMap);
		String access_token = "";
		if(result.contains("access_token")){
			JSONObject json = JSONObject.fromObject(result);
			access_token = json.getString("access_token");
			System.out.println(access_token);
		}
		return access_token;
	}
	
}
