package com.ineasy.maven.controller;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ineasy.maven.po.User;
import com.ineasy.maven.service.IndexService;
import com.navict.jrobot.core.annotations.ApiLog;

/**
 * 新闻管理控制层
 * @author Jia Xinlei
 *
 */
@Controller
public class IndexController {
	
	@Resource
	private IndexService indexService;

	@RequestMapping(value = "/index",method=RequestMethod.GET)
	@ApiLog(name="首页")
	public ModelAndView index(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/chatMb");
		return mav;
	}
	
	@RequestMapping(value = "/register",method=RequestMethod.POST)
	@ApiLog(name="注册")
	@ResponseBody
	public String register(HttpServletRequest request,HttpServletResponse response,@Valid User vo){
		String result = indexService.register(vo);
		return result;
	}
	
	@RequestMapping(value = "/login",method=RequestMethod.POST)
	@ApiLog(name="登录")
	@ResponseBody
	public String login(HttpServletRequest request,HttpServletResponse response,@Valid User vo){
		String result = indexService.login(request,vo);
		return result;
	}
}
