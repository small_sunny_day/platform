package com.ineasy.maven.service;

import javax.servlet.http.HttpServletRequest;

import com.ineasy.maven.po.User;

public interface IndexService {
	
	//登录
	String login(HttpServletRequest request,User vo);
	
	//注册
	String register(User vo);

}
