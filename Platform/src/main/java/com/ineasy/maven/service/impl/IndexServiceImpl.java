package com.ineasy.maven.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ineasy.maven.core.JsonApi;
import com.ineasy.maven.mapper.UserMapper;
import com.ineasy.maven.po.User;
import com.ineasy.maven.service.IndexService;
import com.mysql.jdbc.StringUtils;

import net.sf.json.JSONObject;

@Service
public class IndexServiceImpl implements IndexService{

	@Autowired
	private UserMapper userMapper;

	private JsonApi jsonApi;

	//登录
	public String login(HttpServletRequest request,User vo) {
		String mobile = vo.getMobile();
		if(!StringUtils.isNullOrEmpty(mobile)){
			User user = userMapper.findUserByMobile(mobile.trim());
			if(user != null){
				if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
					if(vo.getPassWord().equals(user.getPassWord())){
						request.getSession().setAttribute("user", user);
						jsonApi = new JsonApi(10000, "登录成功");
						jsonApi.SetObj("user", JSONObject.fromObject(user));
					}else{
						jsonApi = new JsonApi(10001, "用户名或密码错误");
					}
				}else{
					jsonApi = new JsonApi(10001, "密码不能为空");
				}

			}else{
				jsonApi = new JsonApi(10001, "用户不存在");
			}
		}else{
			jsonApi = new JsonApi(10001, "用户名不能为空");
		}
		return jsonApi.ToJson();
	}

	//注册
	public String register(User vo) {
		String mobile = vo.getMobile();
		if(!StringUtils.isNullOrEmpty(mobile)){
			User user = userMapper.findUserByMobile(mobile);
			if(user == null){
				if(!StringUtils.isNullOrEmpty(vo.getPassWord())){
					int result = userMapper.register(vo);
					if(result == 1){
						jsonApi = new JsonApi(10000, "注册成功");
					}else{
						jsonApi = new JsonApi(10001, "网络异常");
					}
				}else{
					jsonApi = new JsonApi(10001, "密码不能为空");
				}
			}else{
				jsonApi = new JsonApi(10001, "用户已存在");
			}
		}else{
			jsonApi = new JsonApi(10001, "用户名不能为空");
		}
		return jsonApi.ToJson();
	}

}
