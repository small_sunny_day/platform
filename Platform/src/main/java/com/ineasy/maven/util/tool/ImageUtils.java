package com.ineasy.maven.util.tool;
import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import com.google.zxing.BarcodeFormat;  
import com.google.zxing.EncodeHintType;  
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

/**
 * 
 *@Title: ImageUtils
 *@Desc: 生成二维码util类
 *@Author: Jia Xilei
 *@Version: 1.0.0
 *@Date: 2016年11月14日 上午11:47:38
 */
public class ImageUtils {

	/**
	 * 
	 *@Title: createCode
	 *@Desc: 生成二维码图片
	 *@Author: Jia Xilei
	 *@ReturnType String
	 *@Version: 1.0.0
	 *@Date: 2016年11月14日 下午2:43:16
	 */
	@SuppressWarnings({"unchecked", "rawtypes" })
	public static String createCode(HttpServletRequest request){
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//获取Apache安实际安装目录
		String projectName = request.getContextPath();
		//去掉项目名中的/
		projectName = projectName.substring(1, projectName.length());
		String path = request.getSession().getServletContext().getRealPath("/");
		path = path.substring(0, path.indexOf(projectName))+"resources/";
		File file  = new File(path);  
		//如果指定文件夹不存在，则新建文件夹	
		if (!file.exists()) {
			file.mkdir();
		}
		//地址栏访问地址
		String paths =  request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/";
				//二维码中的链接
		String text = paths+projectName;  
		//获取32位随机数（字母和数字组成）
		String random = UUIDUtils.getUUID();
		int width = 300; 	     //二维码的宽带 
		int height = 300;        //二维码的高度 
		String format = "png";   //二维码的格式(png,jpg)
		Hashtable hints = new Hashtable();  
		//内容所使用编码  
		try {
			hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");  
			BitMatrix bitMatrix = new MultiFormatWriter().encode(text,  
					BarcodeFormat.QR_CODE, width, height, hints);  
			//生成二维码 ，图片保存到Tomcat安装目录下webaaps下resources文件夹下
			File outputFile = new File(path+File.separator+random+".png");  
			//向文件夹中写入二维码图片
			MatrixToImageWriter.writeToFile(bitMatrix, format, outputFile);
			//返回二维码直接路径
			return paths+"resources"+File.separator+random+".png";
		} catch (WriterException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}  
	}

}
