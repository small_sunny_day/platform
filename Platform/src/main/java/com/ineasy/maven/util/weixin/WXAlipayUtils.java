package com.ineasy.maven.util.weixin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ineasy.maven.util.alipay.AlipayCore;


/**
 * 微信退款util类
 * @author Administrator
 *
 */
public class WXAlipayUtils{

	private static String key = "54OYyxkGwX70YwFSPtgx27r5avUxEYCN";						     //key值
	//智信：ineasytechineasytechineasytech52
	//云易：54OYyxkGwX70YwFSPtgx27r5avUxEYCN

	private static String tkurl = "https://api.mch.weixin.qq.com/secapi/pay/refund";		//退款地址

	private static String xdUrl = "https://api.mch.weixin.qq.com/pay/unifiedorder";		    //统一下单地址

	private static String appid = "wx106b94b1e2aba367";									    //appid
	//智信：wx7938133d7ca17bee
	//云易：wx106b94b1e2aba367

	private static String mch_id = "1366177502";										    //商户号
	//智信：1359778202
	//云易：1366177502

	/**
	 * 微信统一下单接口
	 * @return
	 */
	public static WeixinXmlEntity wxOrder(String orderNo,String price,String url){
		String nonce_str = getMd5Info(getRandom());
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("appid", WXAlipayUtils.appid);										//应用ID
		sParaTemp.put("mch_id", WXAlipayUtils.mch_id);										//商户号
		sParaTemp.put("body", "易租车");														//商品描述
		sParaTemp.put("nonce_str", nonce_str); 												//随机字符串
		sParaTemp.put("out_trade_no",orderNo);												//订单号
		sParaTemp.put("total_fee",price);													//总价格
		sParaTemp.put("spbill_create_ip", "192.168.1.1");									//终端IP
		sParaTemp.put("trade_type","APP");													//交易类型
		sParaTemp.put("notify_url", url);													//通知地址
		String strInfo = AlipayCore.createLinkString(sParaTemp);
		String sign = WXAlipayUtils.getMd5Info(strInfo+"&key="+WXAlipayUtils.key);
		String result= strInfo+"&sign="+sign.toUpperCase();
		String xml = null;
		try {
			xml = sendXMLInfo(getXMLInfo(result),"1");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		WeixinXmlEntity entity =getjxXMLInfo(xml);
		return entity;
	}
	
	/**
	 * 申请微信退款
	 * @param map
	 * @return 
	 */
	public static String apliyRefund(Map<String,String> map){
		String nonce_str = getMd5Info(getRandom());
		long out_refund_no = System.currentTimeMillis();
		map.put("appid", WXAlipayUtils.appid);
		map.put("mch_id", WXAlipayUtils.mch_id);
		map.put("nonce_str",nonce_str);
		map.put("op_user_id",WXAlipayUtils.mch_id);
		map.put("out_refund_no", String.valueOf(out_refund_no));
		String str = AlipayCore.createLinkString(map);
		String sign = WXAlipayUtils.getMd5Info(str+"&key="+WXAlipayUtils.key);
		String result = str+"&sign="+sign.toUpperCase();
		String xml = WXAlipayUtils.getXMLInfo(result);
		String response = "";
		try {
			response = new HttpsRequest().sendPost(WXAlipayUtils.tkurl, xml, "/data/zhengshu/10016228.p12", WXAlipayUtils.mch_id);
			///data/zhengshu/10016228.p12
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(response);
		WeixinXmlEntity entiy =getjxXMLInfo(response);
		return entiy.getResult_code();
	}

	/**
	 *生成六位随机数
	 * @return
	 */
	public static String getRandom(){
		Random rd = new Random();
		String str = "";
		for (int i = 0; i < 6; i++) {
			str += rd.nextInt(9);
		}
		return str;
	}

	/*** 
	 * MD5加码 生成32位md5码
	 */  
	public static String getMd5Info(String inStr){  
		MessageDigest md5 = null;  
		try{  
			md5 = MessageDigest.getInstance("MD5");  
		}catch (Exception e){  
			e.printStackTrace();  
			return "";  
		}  
		byte[] charArray = null;
		try {
			charArray = inStr.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		byte[] byteArray = new byte[charArray.length];  

		for (int i = 0; i < charArray.length; i++)  
			byteArray[i] = (byte) charArray[i];  
		byte[] md5Bytes = md5.digest(byteArray);  
		StringBuffer hexValue = new StringBuffer();  
		for (int i = 0; i < md5Bytes.length; i++){  
			int val = ((int) md5Bytes[i]) & 0xff;  
			if (val < 16)  
				hexValue.append("0");  
			hexValue.append(Integer.toHexString(val));  
		}  
		return hexValue.toString();  
	}

	/**
	 * 将字符串转换为xml文件
	 * @param str
	 * @return
	 */
	public static String getXMLInfo(String str){
		String xml = "";
		if(str.contains("&")){
			xml ="<xml>\n";
			String arr[] = str.split("&");
			for (int i = 0; i < arr.length; i++) {
				if(arr[i].contains("=")){
					String arr2[] = arr[i].split("=");
					xml += "<"+arr2[0]+">";
					xml += arr2[1];
					xml += "</"+arr2[0]+">\n";
				}
			}
			xml += "</xml>";
		}
		return xml;
	}

	/**
	 * 发送xml文件
	 * @param xmlInfo
	 * @throws IOException 
	 */
	public static String sendXMLInfo(String xmlInfo,String sta) throws IOException{
		URLConnection con = null;
		String str = "";
		String line = null;
		try {  
			URL url = null;
			if(sta.equals("0")){						//申请退款
				url = new URL(WXAlipayUtils.tkurl);  
			}else if(sta.equals("1")){ 				   //统一下单
				url = new URL(WXAlipayUtils.xdUrl);  
			}
			con = url.openConnection();  
			con.setDoOutput(true);  

			OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());      
			out.write(new String(xmlInfo.getBytes("UTF-8")));  
			out.flush();  
			out.close();  
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream())); 
			for (line = br.readLine(); line != null; line = br.readLine()) {  
				str += line;
			} 
		} catch (MalformedURLException e) {  
			e.printStackTrace();  
		} catch (IOException e) {  
			e.printStackTrace();  
		} 
		return str;
	}

	/**
	 * 解析微信xml消息
	 * @param strXml
	 * @return
	 */
	public static WeixinXmlEntity getjxXMLInfo(String strXml){
		WeixinXmlEntity msg = null;
		try {
			if (strXml.length() <= 0 || strXml == null){
				return null;
			}
			// 将字符串转化为XML文档对象
			Document document = DocumentHelper.parseText(strXml);
			// 获得文档的根节点
			Element root = document.getRootElement();
			// 遍历根节点下所有子节点
			Iterator<?> iter = root.elementIterator();

			// 遍历所有结点
			msg = new WeixinXmlEntity();
			//利用反射机制，调用set方法
			//获取该实体的元类型
			Class<?> c = Class.forName("com.navict.refund.util.WeixinXmlEntity");
			msg = (WeixinXmlEntity)c.newInstance();//创建这个实体的对象
			while(iter.hasNext()){
				Element ele = (Element)iter.next();
				//获取set方法中的参数字段（实体类的属性）
				Field field = c.getDeclaredField(ele.getName());
				//获取set方法，field.getType())获取它的参数数据类型
				Method method = c.getDeclaredMethod("set"+ele.getName().substring(0, 1).toUpperCase()+ele.getName().substring(1, ele.getName().length()), field.getType());
				//调用set方法
				method.invoke(msg, ele.getText());
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("xml 格式异常: "+ strXml);
			e.printStackTrace();
		}
		return msg;
	}

}
