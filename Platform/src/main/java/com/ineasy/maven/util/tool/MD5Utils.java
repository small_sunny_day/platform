package com.ineasy.maven.util.tool;

import java.util.HashMap;
import java.util.Map;

import com.navict.jrobot.core.utils.Digests;
import com.navict.jrobot.core.utils.Encodes;

/**
 * 
 *@Title: MD5Utils
 *@Desc: 加密及解密相关util类
 *@Author: Jia Xilei
 *@Version: 1.0.0
 *@Date: 2016年11月14日 下午2:54:54
 */
public class MD5Utils {

	//加密次数
	public static final int INTERATIONS = 2;

	// 盐值size
	private static final int SALT_SIZE = 8;
	
	/**
	 * 
	 *@Title: dataEncrypt
	 *@Desc: 密码加密
	 *@param password  初始密码
	 *@return
	 *@Author: Jia Xilei
	 *@ReturnType Map<String,String>
	 *@Version: 1.0.0
	 *@Date: 2016年11月15日 上午9:38:49
	 */
	public static Map<String,String> dataEncrypt(String password){
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		Map<String,String> map = new HashMap<String,String>();
		byte[] hashPassword = Digests.sha1(password.getBytes(), salt, INTERATIONS);
		map.put("password", Encodes.encodeHex(hashPassword));
		map.put("salt",Encodes.encodeHex(salt));
		return map;
	}
	
	/**
	 * 
	 *@Title: dataDecrypt
	 *@Desc: 密码解密
	 *@param password  密码
	 *@param salt      盐值
	 *@param dbPass    数据库用户保存密码
	 *@return
	 *@Author: Jia Xilei
	 *@ReturnType String
	 *@Version: 1.0.0
	 *@Date: 2016年11月14日 下午3:06:02
	 */
	public static boolean dataDecrypt(String password,String salt,String dbPass){
		byte[] data =Encodes.decodeHex(salt);
		byte[] hashPassword = Digests.sha1(password.getBytes(), data, INTERATIONS);
		if(Encodes.encodeHex(hashPassword).equals(dbPass)){
			return true;
		}else{
			return false;
		}
	}

}
