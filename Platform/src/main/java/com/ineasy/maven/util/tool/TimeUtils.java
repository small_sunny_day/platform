package com.ineasy.maven.util.tool;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {

	public static String getDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
	
	public static String getTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
		return sdf.format(new Date());
	}
	public static void main(String[] args) {
		System.out.println(TimeUtils.getTime());
	}
}
