package com.ineasy.maven.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.ineasy.maven.util.tool.TimeUtils;

/**
 * 
 * @Title:LogInterceptor
 * @Description:日志拦截器
 * @author: Jia xinlei
 * @date：2016年11月2日 下午6:19:43
 * @version: 1.0.0
 */
public class LogInterceptor implements HandlerInterceptor {

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj, Exception arg3)
			throws Exception {

		@SuppressWarnings("unchecked")
		Map<String,String[]> map = request.getParameterMap();
		if(map.size() > 0)
			for (String key : map.keySet()) {
				System.out.println("请求参数名为： "+ key + ",对应的值为:" + map.get(key)[0]);
			}else{
				System.out.println("请求参数为空");
			}
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView arg3)
			throws Exception {
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HandlerMethod handlerMethod = (HandlerMethod) handler;  
		System.out.println(TimeUtils.getTime()+"请求的类："+handlerMethod.getBeanType().getSimpleName()+"==请求的方法："+handlerMethod.getMethod().getName());
	//	String sessionId = request.getSession().getId();
		/*	Object m = request.getSession().getAttribute(sessionId);
		String time = null;
		if(m != null){
			time = String.valueOf(m.toString());
		}
		if(m == null){
			request.getSession().setAttribute(sessionId,System.currentTimeMillis());
			return true;
		}else if(System.currentTimeMillis() - Long.parseLong(time) < 60 * 10000){
			System.out.println(System.currentTimeMillis() - Long.parseLong(time));
			return false;
		}else{
			System.out.println(System.currentTimeMillis() - Long.parseLong(time));
			return true;
		}*/
		//Thread.sleep(1000);   //休息一下, 单位毫秒
		return true;
	}

}
