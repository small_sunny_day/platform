/**
 * Created by gh on 2017/8/21.
 */
//表情:查看结果
function replace_em(str){
    str = str.replace(/\</g,'&lt;');
    str = str.replace(/\>/g,'&gt;');
    str = str.replace(/\n/g,'<br/>');
    str = str.replace(/\[em_([0-9]*)\]/g,'<img src="arclist/$1.gif" border="0" />');
    return str;
}
$(function(){
    $('.smile').qqFace({
        id : 'facebox',
        assign:'messageEditor',
        path:'arclist/'	//表情存放的路径
    });
    $("#sendBtn").click(function(){
        var str = $("#messageEditor").text();
        // $("#show").html(replace_em(str));
        alert(str);
    });
    var boxHeight=$('#moreFunctionbox').height();
    $('#moreFunctionbox').height(0);
    $('#moreFunction').click(function(){
        var that=this;
        $('#facebox').hide();
        if($('#moreFunctionbox').height()==0){
            $('#moreFunctionbox').show();
            $('#moreFunctionbox').animate({'height':boxHeight},500);
            $('#footer').animate({'bottom':'105px'},500);
            $(that).addClass('xuanzhuan');
        }else{
            $('#moreFunctionbox').animate({'height':0},500,function(){
                $('#moreFunctionbox').hide();
            });
            $('#footer').animate({'bottom':'0px'},500);
            $(that).removeClass('xuanzhuan');
        }
    })
});