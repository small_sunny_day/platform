<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%> 
<!DOCTYPE html>
<!-- saved from url=(0047)http://www.803876.com/room/m/index.php?rid=6001 -->
<html class="ios iphone mobile portrait">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>信彩计划聊天室</title>
    <meta content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="telephone=no" name="format-detection">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="white" name="apple-mobile-web-app-status-bar-style">
    <link rel="stylesheet" href="<%=path %>/resources/css/index.min.css">
    <link rel="stylesheet" href="<%=path %>/resources/css/layer.css">
    <link rel="stylesheet" href="<%=path %>/resources/css/weui.css">
    <link rel="stylesheet" href="<%=path %>/resources/css/mb.css">
</head>
<body>
<article>
    <!--头部-->
    <section id="#head_1">
        <section class="video-container">
            <!--头部-->
            <div class="tophdpp">
                <div id="logo">
                    <img src="<%=path %>/resources/images/logo.png">
                </div>
                <div class="tophdppy">
                    <div id="loginBtn"><span id="res">注册</span>/<span id="login">登陆</span></div>
                </div>
            </div>
            <!--彩票信息公告-->
            <script type="text/javascript">
                //彩种切换
                function nTabs(thisObj,Num){
                    if(thisObj.className == "active")return;
                    var tabObj = thisObj.parentNode.id;
                    var tabList = document.getElementById(tabObj).getElementsByTagName("li");
                    for(i=0; i <tabList.length; i++)
                    {
                        if (i == Num)
                        {
                            thisObj.className = "active";
                            document.getElementById(tabObj+"_Content"+i).style.display = "block";
                        }else{
                            tabList[i].className = "normal";
                            document.getElementById(tabObj+"_Content"+i).style.display = "none";
                        }
                    }
                }
            </script>
            <div class="mobaoma">
                <div class="mobaomat">
                    <div id="myTab0_Content0">
                        <div class="ppbaomad">
                            <!-- bjpk10 -->
                            <div class="ppbaomadz">
                                <img src="<%=path %>/resources/images/1a.png">
                            </div>
                            <div class="ppbaomady">
                                <div class="ppbaomadyt">
                                    <span id="bjpk10-expect">635447</span>
                                    期
                                    <span class="y bjpk10-countdown">
                                                    距下期还有:<span id="kjbjpk10-next-h" class="none">00时</span>
                                                    <span id="kjbjpk10-next-m">2</span>
                                                    分<span id="kjbjpk10-next-s">57</span>
                                                    秒
                                                </span>
                                    <span class="y none bjpk10-lottery-time" style="display: none;"></span>
                                </div>
                                <div class="ppbaomadyb">
                                                <span id="expectbjpk10">
                                                    <span class="bjpk10_no10">10</span>
                                                    <span class="bjpk10_no4">4</span>
                                                    <span class="bjpk10_no7">7</span>
                                                    <span class="bjpk10_no6">6</span>
                                                    <span class="bjpk10_no9">9</span>
                                                    <span class="bjpk10_no1">1</span>
                                                    <span class="bjpk10_no8">8</span>
                                                    <span class="bjpk10_no2">2</span>
                                                    <span class="bjpk10_no3">3</span>
                                                    <span class="bjpk10_no5">5</span>
                                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="myTab0_Content1" class="none">
                        <div class="ppbaomad">
                            <!-- cqssc -->
                            <div class="ppbaomadz">
                                <img src="<%=path %>/resources/images/2a.png">
                            </div>
                            <div class="ppbaomady">
                                <div class="ppbaomadyt">
                                    <span id="cqssc-expect">20170821033</span>
                                    期
                                    <span class="y cqssc-countdown">
                                                    距下期还有:<span id="kjcqssc-next-h" class="none">00时</span>
                                                    <span id="kjcqssc-next-m">0</span>
                                                    分<span id="kjcqssc-next-s">57</span>
                                                    秒
                                                </span>
                                    <span class="y none cqssc-lottery-time" style="display: none;"></span>
                                </div>
                                <div class="ppbaomadyb">
                                                <span id="expectcqssc">
                                                    <span class="cqssc_no1">1</span>
                                                    <span class="cqssc_no4">4</span>
                                                    <span class="cqssc_no9">9</span>
                                                    <span class="cqssc_no9">9</span>
                                                    <span class="cqssc_no6">6</span>
                                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="myTab0_Content2" class="none">
                        <div class="ppbaomad">
                            <!-- hk6 -->
                            <div class="ppbaomadz">
                                <img src="<%=path %>/resources/images/3a.png">
                            </div>
                            <div class="ppbaomady">
                                <div class="ppbaomadyt">
                                    <span id="hk6-expect">2017097</span>
                                    期
                                    <span class="y none hk6-countdown">
                                                    距下期还有:<span id="kjhk6-next-m">00</span>
                                                    分<span id="kjhk6-next-s">00</span>
                                                    秒
                                                </span>
                                    <span class="y hk6-lottery-time">晚上9:34:40</span>
                                </div>
                                <div class="ppbaomadyb">
                                                <span id="expecthk6">
                                                    <span class="hk6_no34">34</span>
                                                    <span class="hk6_no2">2</span>
                                                    <span class="hk6_no32">32</span>
                                                    <span class="hk6_no43">43</span>
                                                    <span class="hk6_no45">45</span>
                                                    <span class="hk6_no23">23</span>
                                                    <span class="hk6_no+">+</span>
                                                    <span class="hk6_no23">22</span>
                                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="myTab0_Content3" class="none">
                        <div class="ppbaomad">
                            <!-- jsk3 -->
                            <div class="ppbaomadz">
                                <img src="<%=path %>/resources/images/8a.png">
                            </div>
                            <div class="ppbaomady">
                                <div class="ppbaomadyt">
                                    <span id="jsk3-expect">20170821018</span>
                                    期
                                    <span class="y jsk3-countdown">
                                                    距下期还有:<span id="kjjsk3-next-h" class="none">00时</span>
                                                    <span id="kjjsk3-next-m">0</span>
                                                    分<span id="kjjsk3-next-s">57</span>
                                                    秒
                                                </span>
                                    <span class="y none jsk3-lottery-time" style="display: none;"></span>
                                </div>
                                <div class="ppbaomadyb">
                                                <span id="expectjsk3">
                                                    <span class="jsk3_no3">3</span>
                                                    <span class="jsk3_no3">3</span>
                                                    <span class="jsk3_no5">5</span>
                                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="myTab0_Content4" class="none">
                        <div class="ppbaomad">
                            <!-- gdklsf -->
                            <div class="ppbaomadz">
                                <img src="<%=path %>/resources/images/08.png">
                            </div>
                            <div class="ppbaomady">
                                <div class="ppbaomadyt">
                                    <span id="gdklsf-expect">20170821015</span>
                                    期
                                    <span class="y gdklsf-countdown">
                                                    距下期还有:<span id="kjgdklsf-next-h" class="none">00时</span>
                                                    <span id="kjgdklsf-next-m">0</span>
                                                    分<span id="kjgdklsf-next-s">57</span>
                                                    秒
                                                </span>
                                    <span class="y none gdklsf-lottery-time" style="display: none;"></span>
                                </div>
                                <div class="ppbaomadyb">
                                                <span id="expectgdklsf">
                                                    <span class="gdklsf_no1">1</span>
                                                    <span class="gdklsf_no12">12</span>
                                                    <span class="gdklsf_no9">9</span>
                                                    <span class="gdklsf_no8">8</span>
                                                    <span class="gdklsf_no3">3</span>
                                                    <span class="gdklsf_no19">19</span>
                                                    <span class="gdklsf_no11">11</span>
                                                    <span class="gdklsf_no10">10</span>
                                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobaomab">
                    <ul id="myTab0">
                        <li class="active" onclick="nTabs(this,0);">北京PK10</li>
                        <li class="normal" onclick="nTabs(this,1);">重庆时时彩</li>
                        <li class="normal" onclick="nTabs(this,2);">香港彩</li>
                        <li class="normal" onclick="nTabs(this,3);">江苏快3</li>
                        <li class="normal" onclick="nTabs(this,4);">广东快乐十分</li>
                    </ul>
                </div>
            </div>
            <div id="MsgSound" class="none"></div>
            <!--ajax到后台获取各彩票开奖情况-->
        </section>
        <section class="video-show-wrap" style="display: none;">
            <a id="video-show" class="videoShow" href="javascript:void(0);"></a>
        </section>
    </section>
    <!--主体-->
    <section class="tab-content">
        <!--公告-->
        <div class="top" id="announcement">
            <marquee scrollamount="3" id="msg_tip_show">
                <span style="color:white">欢迎来到信彩聊天室 信誉铸就九年品牌 巨资打造网上购彩第一品牌！ 最新存款优惠：充10元送18元 / 充100送38 / 充1000送188 /详情请您加客服QQ1700170032 聊天室不定时发送红包 加入信彩赢取豪礼</span>
            </marquee>
            <!--按钮:控制mobaoma和announcement显示隐藏-->
            <div id="video-hide" class="videoHide">
                <img src="<%=path %>/resources/images/btn-eye.png" width="20px" height="20px">
            </div>
        </div>
        <div class="weui-gallery" id="gallery">
            <span class="weui-gallery__img" id="galleryImg"></span>
        </div>
        <!--聊天区-->
        <div id="publicChat" class="publicChat" style="overflow-y: auto; display: block; height: 492px;">
            <div style="clear:both;"></div>
            <!--notmine:会员说的-->
            <div class="msg notmine " id="00d127cc">
                <div class="msg_head">
                    <img src="<%=path %>/resources/images/null.jpg" class="msg_group_ico">
                </div>
                <div class="msg_content">
                    <div>
                        <font class="u">laowang889</font>
                        &nbsp;&nbsp;<font class="date">11:27:46</font>
                    </div>
                    <div class="layim_chatsay">
                        <font>
                            这把 追上<br>
                        </font>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
            <!--manage:管理员(计划员)-->
            <div class="msg manage" id="55826dec">
                <div class="msg_head">
                    <img src="<%=path %>/resources/images/3501.png" class="msg_group_ico">
                </div>
                <div class="msg_content">
                    <div>
                        <font class="u">计划员</font>
                        &nbsp;&nbsp;<font class="date">11:32:51</font>
                    </div>
                    <div class="layim_chatsay">
                        <font>
                            <div>信彩冠军⑤码三期</div>
                            <div>------------------------------</div>
                            <div>438-440期 PK10冠军【01 02 06 08 09】440期 开【05】 挂</div>
                            <div>441-443期 PK10冠军【01 05 06 07 10】441期 开【01】 中</div>
                            <div>442-444期 PK10冠军【01 03 05 06 07】444期 开【09】 挂</div>
                            <div>445-447期 PK10冠军【01 03 04 05 09】445期 开【05】 中</div>
                            <div>446-448期 PK10冠军【02 03 04 08 09】447期 等开</div>
                            <div>------------------------------</div>
                            <div>440-442期 PK10亚军【02 03 04 06 09】440期 开【04】 中</div>
                            <div>441-443期 PK10亚军【01 03 08 09 10】441期 开【03】 中</div>
                            <div>442-444期 PK10亚军【01 03 06 09 10】442期 开【03】 中</div>
                            <div>443-445期 PK10亚军【02 03 04 05 10】445期 开【09】 挂</div>
                            <div>446-448期 PK10亚军【03 04 08 09 10】447期 等开</div>
                            <div>------------------------------</div>
                            <div>温馨提示：开户请认准信彩彩票，私聊都是骗子,</div>
                            <div>提款不到账，请大家谨防上当！</div>
                            <div>
                                <br>
                            </div>
                            <div>电脑/手机开户网址：www.xincaiD.com</div>
                            <div>
                                <br>
                            </div>
                            <div>信彩彩票开奖网：www.908511.com</div>
                            <div>
                                <br>
                            </div>
                        </font>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
            <div style="clear:both;"></div>
            <li class="history-hr-wrap">
                <div class="history-hr"></div>
                <div class="history-hr-text">以上是历史消息</div>
            </li>
            <div style="clear:both;"></div>
            <div class="msg system-msg">
                <div class="msg_head">
                    <img src="<%=path %>/resources/images/sys.png">
                </div>
                <div class="msg_content">
                    <div>
                        <font class="u">系统消息</font>
                        <font class="date">&nbsp;&nbsp;11:39:12</font>
                    </div>
                    <div class="layim_chatsay">
                        <font style="color:#fff">您与大奖的距离只差一个会员账号！信彩开户官网：《www.xincaiE.com》，存款还有更多豪礼详情请添加客服QQ：1700771234</font>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="signin-container">
            <img src="<%=path %>/resources/images/signin-icon.png">
        </div>
        <div class="pound-gold-egg" style="display:none">
            <img src="<%=path %>/resources/images/egg.png">
        </div>
    </section>
    <!--悬浮的四个按钮-->
    <div class="moppmenu">
        <li>
            <a href="https://www.xincaid.com/?Intr=28107" target="_blank">官网</a>
        </li>
        <li>
            <a href="https://www.908522.com/" target="_blank">开奖</a>
        </li>
        <li>
            <a href="https://www.xc00.com/" target="_blank">导航</a>
        </li>
        <li>
            <a href="https://chat56.live800.com/live800/chatClient/chatbox.jsp?companyID=841169&amp;configID=127013&amp;jid=9217061320&amp;subject=%E5%92%A8%E8%AF%A2&amp;prechatinfoexist=1&amp;s=1" target="_blank">客服</a>
        </li>
    </div>
</article>
<div id="footer" style="display: block; bottom: 0px;">
    <!--更多-->
    <div class="more-function" id="moreFunction">
        <img src="<%=path %>/resources/images/add.png">
    </div>
    <div class="sendBtn fr" id="sendBtn">发送</div>
    <!-- <div id="sharedBtn"> <span>分享</span> </div> -->
    <div class="smile" id="smile">
        <img src="<%=path %>/resources/images/smile.png" alt="表情" width="26px" height="26px">
    </div>
    <div id="editor">
        <div class="messageEditor" id="messageEditor" contenteditable="true"></div>
    </div>
    <!--更多-->
    <div class="more-function-container1" id="moreFunctionbox" style="display: none">
        <dl>
            <dd>
                <div class="img-container" id="sharedBtn">
                    <img src="<%=path %>/resources/images/Share.png">
                </div>
                <div class="text-container">
                    <span>分享</span>
                </div>
            </dd>
            <dd>
                <div class="img-container" id="barrageBtn">
                    <img src="<%=path %>/resources/images/barrage.png">
                </div>
                <div class="text-container">
                    <span id="barrageInfo">开启</span>
                </div>
            </dd>
            <div style="clear:both;"></div>
        </dl>
        <div style="clear:both;"></div>
        <input id="filedata" type="file" size="20" name="filedata" class="input" style="display:none" onchange="uploadAvatar(&#39;filedata&#39;, &#39;#messageEditor&#39;)">
    </div>
    <!--表情-->
    <div id="facebox" style="width: 100%; display: none;background: white;padding: 10px 10px;overflow-x: scroll;height: 105px;"></div>
</div>

<div class="loginWrap"></div>
<div class="tipMesWrap"></div>
<!-- <div class="qqchat"> <span></span><img src="./images/mobilefloat.png"></div> -->
<div class="setting-expression-layer" style="display: none;">
    <div class="expression" id="expressions" style="bottom: 0px;">
        <table class="expr-tab expr-tab1"></table>
    </div>
</div>
</live-room-m>
<input id="MyMoney" type="hidden" value="0.00">
<div style="display:none"></div>
<div class="egg-code-container">
    <div class="inputInfo">
        <!-- <img class="colseLogin" src="/images/messageBtn.png" alt="关闭"> -->
        <div class="colseLogin"></div>
        <p class="inputInfoTitle">输入抽奖</p>
        <div class="codeDiv">
            <label for="codeInput">抽奖码:</label>
            <input class="codeInput" id="codeInput" type="text" placeholder="请输入抽奖码">
        </div>
        <input class="submitTijiao" type="button" value="提交" id="submitCode">
    </div>
</div>
<!--我的-->
<script src="<%=path %>/resources/js/jquery-2.1.4.js"></script>
<!--表情-->
<script src="<%=path %>/resources/js/jquery.qqFace.js"></script>

<script src="<%=path %>/resources/js/myMobile.js"></script>
</body>
</html>